/**
 * @file test_dio.c
 * @author ya058764
 * @date 2019.07.23
 * @brief 測試dio函式運作
 */

#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/dio.h"

#include <avr/io.h>

int main() {
    C4M_DEVICE_set();
    char a, b;
    DIO_fpt(200, 0xff, 0, 0x0f);
    a = DIO_fpt(0, 0xff, 0, 0xff);
    printf("fpt_output_warnning=%d\n", a);
    b = 0xff;
    a = DIO_put(0, 1, (void*)&b);
    printf("put_output_warnning=%d\n", a);
    a = DIO_fgt(100, 0xff, 0, (void*)&b);
    printf("fgt_input_warnning=%d\n", a);
    a = DIO_fgt(100, 0xff, 0, (void*)&b);
    printf("get_input_warnning=%d\n",a);
    
    
    DIO_fpt(200,0xff,0,0xff);
    a = DIO_fpt(0, 0x0f, 0, 0x0f);
    printf("fpt_output_normal=%d\n", a);
    b = 0xff;
    a = DIO_put(0, 1, (void*)&b);
    printf("put_output_normal=%d\n", a);
    
    return 0;
}
