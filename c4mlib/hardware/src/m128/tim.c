/**
 * @file tim.c
 * @author LiYu87
 * @date 2019.04.02
 * @brief tim 暫存器操作相關函式實作。
 */

#include "c4mlib/hardware/src/tim.h"

#include "c4mlib/macro/src/bits_op.h"
#include "c4mlib/macro/src/std_res.h"

#include <avr/io.h>

char TIM_fpt(char LSByte, char Mask, char Shift, char Data) {
    if (Shift > 7) {
        return RES_ERROR_SHIFT;
    }
    switch (LSByte) {
        case 200:
            REGFPT(DDRA, Mask, Shift, Data);
            break;
        case 201:
            REGFPT(DDRB, Mask, Shift, Data);
            break;
        case 202:
            REGFPT(DDRC, Mask, Shift, Data);
            break;
        case 203:
            REGFPT(DDRD, Mask, Shift, Data);
            break;
        case 204:
            REGFPT(DDRE, Mask, Shift, Data);
            break;
        case 205:
            REGFPT(DDRF, Mask, Shift, Data);
            break;
        case 207:
            REGFPT(TIMSK, Mask, Shift, Data);
            break;
        case 208:
            REGFPT(TIFR, Mask, Shift, Data);
            break;
        case 209:
            REGFPT(ETIMSK, Mask, Shift, Data);
            break;
        case 210:
            REGFPT(ETIFR, Mask, Shift, Data);
            break;
        case 211:
            REGFPT(SFIOR, Mask, Shift, Data);
            break;
        case 214:
            REGFPT(ASSR, Mask, Shift, Data);
            break;
        case 215:
            REGFPT(TCCR0, Mask, Shift, Data);
            break;
        case 217:
            REGFPT(TCCR1A, Mask, Shift, Data);
            break;
        case 218:
            REGFPT(TCCR1B, Mask, Shift, Data);
            break;
        case 219:
            REGFPT(TCCR1C, Mask, Shift, Data);
            break;
        case 220:
            REGFPT(ACSR, Mask, Shift, Data);
            break;
        case 222:
            REGFPT(TCCR2, Mask, Shift, Data);
            break;
        case 224:
            REGFPT(TCCR3A, Mask, Shift, Data);
            break;
        case 225:
            REGFPT(TCCR3B, Mask, Shift, Data);
            break;
        case 226:
            REGFPT(TCCR3C, Mask, Shift, Data);
            break;
        default:
            return RES_ERROR_LSBYTE;
    }
    return RES_OK;
}

char TIM_fgt(char LSByte, char Mask, char Shift, void* Data_p) {
    return RES_OK;
}

char TIM_get(char LSByte, char Bytes, void* Data_p) {
    if (Bytes != 1 && Bytes != 2) {
        return RES_ERROR_BYTES;
    }

    uint8_t data8 = 0;
    uint16_t data16 = 0;

    switch (LSByte) {
        case 20:
            data8 = TCNT0;
            break;
        case 100:
            data16 = ICR1;
            break;
        case 26:
            data8 = TCNT2;
            break;
        case 104:
            data16 = ICR3;
            break;
        default:
            return RES_ERROR_LSBYTE;
    }

    if (Bytes == 1) {
        *((uint8_t*)Data_p) = data8;
    }
    else {
        *((uint16_t*)Data_p) = data16;
    }
    return RES_OK;
}

char TIM_put(char LSByte, char Bytes, void* Data_p) {
    uint8_t data8 = 0;
    uint16_t data16 = 0;

    if (Bytes == 1) {
        data8 = *((uint8_t*)Data_p);
    }
    else if (Bytes == 2) {
        data16 = *((uint16_t*)Data_p);
    }
    else {
        return RES_ERROR_BYTES;
    }

    switch (LSByte) {
        // timer0
        case 0:
            OCR0 = data8;
            break;
        case 20:
            TCNT0 = data8;
            break;

        // timer1
        case 2:
            OCR1A = data16;
            break;
        case 22:
            TCNT1 = data16;
            break;

        // timer2
        case 10:
            OCR2 = data8;
            break;
        case 26:
            TCNT2 = data8;
            break;

        // timer3
        case 12:
            OCR3A = data16;
            break;
        case 28:
            TCNT3 = data16;
            break;
        default:
            return RES_ERROR_LSBYTE;
    }
    return RES_OK;
}

char PWM_fpt(char LSByte, char Mask, char Shift, char Data)
    __attribute__((alias("TIM_fpt")));

char PWM_fgt(char LSByte, char Mask, char Shift, void *Data_p)
    __attribute__((alias("TIM_fgt")));

char PWM_put(char LSByte, char Bytes, void* Data_p)
    __attribute__((alias("TIM_put")));

char PWM_get(char LSByte, char Bytes, void* Data_p)
    __attribute__((alias("TIM_put")));
