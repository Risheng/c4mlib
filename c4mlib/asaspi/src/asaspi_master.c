/**
 * @file asaspi_master.c
 * @author Deng Xiang-Guan
 * @date 2019.10.02
 * @brief 實現ASA SPI Master(主)的驅動函式，提供使用者呼叫已設定ASA單板電腦SPI的工作模式，
 * 並提供多種不同模式通訊協定配合ASA介面卡協定，達成資料送收之目的。
 * 
 * NOTE: _delay_ms 在debug的時候要加，因printf會造成時間的延遲，如果不加，會造成通訊失敗。
 */

#include "asaspi_master.h"

#include "c4mlib/asabus/src/exfunc.h"
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/hardware/src/hal_spi.h"
#include "c4mlib/macro/src/bits_op.h"

#ifdef USE_C4MLIB_DEBUG
#    define SPI_DELAY(tick) \
        { _delay_ms(20); }
#else
#    define SPI_DELAY(tick)              \
        for (int i = 0; i < tick; i++) { \
            __asm__ __volatile__("nop"); \
        }
#endif

char ASA_SPIM_trm(char mode, char ASAID, char RegAdd, char Bytes,
                  void *Data_p,uint16_t WaitTick) {
    char echo = 0, err = 0, chk = 0;
    SPIM_Inst.enable_cs(ASAID);
    if (mode >= 100) {
        chk = ExFunc_trm(EXFUNC_SPI, mode, ASAID, RegAdd, Bytes,
                         (uint8_t *)Data_p);
        return chk;
    }
    switch (mode) {
        case 0:
            chk = SPIM_Inst.spi_swap((RegAdd | 0x80));  // add+bit7H
            DEBUG_INFO("get=%d\n", chk);
            SPI_DELAY(WaitTick);
            SPIM_Inst.spi_swap(*((char *)Data_p));  // send frist bytes
            DEBUG_INFO("get=%d\n", chk);
            SPI_DELAY(WaitTick);
            for (int i = 1; i < Bytes; i++) {
                echo = SPIM_Inst.spi_swap(
                    *((char *)Data_p + i));  //送出下一筆&拿回前一筆
                DEBUG_INFO("get=%d\n", echo);
                DEBUG_INFO("old=%d\n", (*((char *)Data_p + i - 1)));
                if (echo != (*((char *)Data_p + i - 1)))
                    err = 1;  //檢查前一筆
                SPI_DELAY(WaitTick);
            }
            echo = SPIM_Inst.spi_swap(0);  //喚回最後一筆
            DEBUG_INFO("get=%d\n", echo);
            DEBUG_INFO("old=%d\n", (*((char *)Data_p + Bytes - 1)));
            SPI_DELAY(WaitTick);
            if (echo != (*((char *)Data_p + Bytes - 1)))
                err = 1;                    //檢查是否吻合
            chk = SPIM_Inst.spi_swap(err);  //送出是否吻合
            DEBUG_INFO("chk=%d\n", chk);

            return chk;
            break;
        
        case 1:
            for (int i = 0; i < Bytes; i++) {
                if (i == 0) {
                    SPIM_Inst.spi_swap(RegAdd | (*((char *)Data_p)));
                    DEBUG_INFO("swap:%d\n", RegAdd | (*((char *)Data_p)));
                    SPI_DELAY(WaitTick);
                }
                else {
                    SPIM_Inst.spi_swap(*((char *)Data_p + i));
                    DEBUG_INFO("swap:%d\n", *((char *)Data_p + i));
                    SPI_DELAY(WaitTick);
                }
            }
            break;

        case 2:
            for (int i = Bytes - 1; i >= 0; i--) {
                if (i == Bytes - 1) {
                    SPIM_Inst.spi_swap(RegAdd |
                                       (*((char *)Data_p + Bytes - 1)));
                    DEBUG_INFO("swap:%d\n", RegAdd |
                                       (*((char *)Data_p + Bytes - 1)));
                    SPI_DELAY(WaitTick);
                }
                else {
                    SPIM_Inst.spi_swap(*((char *)Data_p + i));
                    DEBUG_INFO("swap:%d\n", *((char *)Data_p + i));
                    SPI_DELAY(WaitTick);
                }
            }
            break;

        case 3:
            for (int i = 0; i < Bytes; i++) {
                SPIM_Inst.spi_swap(*((char *)Data_p + i));
                DEBUG_INFO("swap:%d\n", *((char *)Data_p + i));
                SPI_DELAY(WaitTick);
            }
            break;

        case 4:
            for (int i = Bytes - 1; i >= 0; i--) {
                SPIM_Inst.spi_swap(*((char *)Data_p + i));
                SPI_DELAY(WaitTick);
            }
            break;

        case 5:
            SPIM_Inst.spi_swap(RegAdd);
            SPI_DELAY(WaitTick);
            for (int i = 0; i < Bytes; i++) {
                SPIM_Inst.spi_swap(*((char *)Data_p + i));
                SPI_DELAY(WaitTick);
            }
            break;

        case 6:
            SPIM_Inst.spi_swap(RegAdd);
            SPI_DELAY(WaitTick);
            for (int i = Bytes - 1; i >= 0; i--) {
                SPIM_Inst.spi_swap(*((char *)Data_p + i));
                SPI_DELAY(WaitTick);
            }
            break;

        case 7:
            SPIM_Inst.spi_swap(RegAdd | 0x80);
            SPI_DELAY(WaitTick);
            for (int i = 0; i < Bytes; i++) {
                SPIM_Inst.spi_swap(*((char *)Data_p + i));
                SPI_DELAY(WaitTick);
            }
            break;

        case 8:
            SPIM_Inst.spi_swap(RegAdd | 0x80);
            SPI_DELAY(WaitTick);
            for (int i = Bytes - 1; i >= 0; i--) {
                SPIM_Inst.spi_swap(*((char *)Data_p + i));
                SPI_DELAY(WaitTick);
            }
            break;

        case 9:
            SPIM_Inst.spi_swap((RegAdd << 1) | 1);
            SPI_DELAY(WaitTick);
            for (int i = 0; i < Bytes; i++) {
                SPIM_Inst.spi_swap(*((char *)Data_p + i));
                SPI_DELAY(WaitTick);
            }
            break;

        case 10:
            SPIM_Inst.spi_swap((RegAdd << 1) | 1);
            SPI_DELAY(WaitTick);
            for (int i = Bytes - 1; i >= 0; i--) {
                SPIM_Inst.spi_swap(*((char *)Data_p + i));
                SPI_DELAY(WaitTick);
            }
            break;
        default:
            SPIM_Inst.disable_cs(ASAID);
            return HAL_ERROR_MODE_SELECT;
            break;
    }
    SPIM_Inst.disable_cs(ASAID);
    return 0;
}

char ASA_SPIM_rec(char mode, char ASAID, char RegAdd, char Bytes, void *Data_p,
                  uint16_t WaitTick) {
    char chk = 0, a = 0;
    SPIM_Inst.enable_cs(ASAID);
    if (mode >= 100) {
        chk = ExFunc_rec(EXFUNC_SPI, mode, ASAID, RegAdd, Bytes,
                         (uint8_t *)Data_p);
        return chk;
    }
    switch (mode) {
        case 0:
            SPIM_Inst.spi_swap(RegAdd);
            // slave會檢查所以比較慢，不delay會出錯
            SPI_DELAY(WaitTick);
            *((char *)Data_p) = SPIM_Inst.spi_swap(0);  //送0喚回第一筆
            a = *((char *)Data_p);  //把對面給的資料丟回去
            SPI_DELAY(WaitTick);
            for (int i = 1; i < Bytes; i++) {
                *((char *)Data_p + i) = SPIM_Inst.spi_swap(a);
                a = *((char *)Data_p + i);
                SPI_DELAY(WaitTick);
            }
            SPIM_Inst.spi_swap(a);  //丟回最後一筆
            SPI_DELAY(WaitTick);
            chk = SPIM_Inst.spi_swap(0);  //接收對面的檢查結果
            return chk;
            break;

        case 1:
            return HAL_ERROR_MODE_SELECT;
            break;

        case 2:
            return HAL_ERROR_MODE_SELECT;
            break;

        case 3:
            SPIM_Inst.spi_swap(0);
            SPI_DELAY(WaitTick);
            for (int i = 0; i < Bytes; i++) {
                *((char *)Data_p + i) = SPIM_Inst.spi_swap(0);
                DEBUG_INFO("rec %d\n", *((char *)Data_p + i));
                SPI_DELAY(WaitTick);
            }
            break;

        case 4:
            SPIM_Inst.spi_swap(0);
            SPI_DELAY(WaitTick);
            for (int i = Bytes - 1; i >= 0; i--) {
                *((char *)Data_p + i) = SPIM_Inst.spi_swap(0);
                SPI_DELAY(WaitTick);
            }
            break;

        case 5:
            *((char *)Data_p + Bytes - 1) = SPIM_Inst.spi_swap(RegAdd);
            SPI_DELAY(WaitTick);
            for (int i = 0; i < Bytes; i++) {
                *((char *)Data_p + i) = SPIM_Inst.spi_swap(0);
                SPI_DELAY(WaitTick);
            }
            break;

        case 6:
            *((char *)Data_p + Bytes - 1) = SPIM_Inst.spi_swap(RegAdd);
            SPI_DELAY(WaitTick);
            for (int i = Bytes - 1; i >= 0; i--) {
                *((char *)Data_p + i) = SPIM_Inst.spi_swap(0);
                SPI_DELAY(WaitTick);
            }

            break;

        case 7:
            SPIM_Inst.spi_swap(RegAdd);
            SPI_DELAY(WaitTick);
            for (int i = 0; i < Bytes; i++) {
                *((char *)Data_p + i) = SPIM_Inst.spi_swap(0);
                SPI_DELAY(WaitTick);
            }
            break;

        case 8:
            *((char *)Data_p + Bytes - 1) = SPIM_Inst.spi_swap(RegAdd);
            SPI_DELAY(WaitTick);
            for (int i = Bytes - 1; i >= 0; i--) {
                *((char *)Data_p + i) = SPIM_Inst.spi_swap(0);
                SPI_DELAY(WaitTick);
            }
            break;

        case 9:
            *((char *)Data_p + Bytes - 1) = SPIM_Inst.spi_swap((RegAdd << 1));
            SPI_DELAY(WaitTick);
            for (int i = 0; i < Bytes; i++) {
                *((char *)Data_p + i) = SPIM_Inst.spi_swap(0);
                SPI_DELAY(WaitTick);
            }
            break;

        case 10:
            *((char *)Data_p + Bytes - 1) = SPIM_Inst.spi_swap((RegAdd << 1));
            SPI_DELAY(WaitTick);
            for (int i = Bytes - 1; i >= 0; i--) {
                *((char *)Data_p + i) = SPIM_Inst.spi_swap(0);
                SPI_DELAY(WaitTick);
            }

            break;
        default:
            SPIM_Inst.disable_cs(ASAID);
            return HAL_ERROR_MODE_SELECT;
            break;
    }
    SPIM_Inst.disable_cs(ASAID);
    return 0;
}

char ASA_SPIM_frc(char mode, char ASAID, char RegAdd, char Mask, char Shift,
                  char *Data_p, uint16_t WaitTick) {
    char chk = 0;
    if (mode >= 100) {
        chk = ExFunc_frc(EXFUNC_SPI, mode, ASAID, RegAdd, Mask, Shift,
                         (uint8_t *)Data_p);
        return chk;
    }
    if ((mode == 1) || (mode == 2) || (mode == 4) ||  (mode == 6) || (mode == 8) ||
        (mode == 10))
        return HAL_ERROR_MODE_SELECT;
    char temp = 0;
    SPIM_Inst.enable_cs(ASAID);
    ASA_SPIM_rec(mode, ASAID, RegAdd, 1, &temp, WaitTick);
    *Data_p = (temp << Shift) & Mask;
    SPIM_Inst.disable_cs(ASAID);
    return 0;
}

char ASA_SPIM_ftm(char mode, char ASAID, char RegAdd, char Mask, char Shift,
                  char *Data_p, uint16_t WaitTick) {
    char chk = 0;
    if (mode >= 100) {
        chk = ExFunc_ftm(EXFUNC_SPI, mode, ASAID, RegAdd, Mask, Shift,
                      (uint8_t *)Data_p);
        return chk;
    }
    if ((mode == 1) || (mode == 2) || (mode == 3) || (mode == 4) || (mode == 6) ||
        (mode == 8) || (mode == 10))
        return HAL_ERROR_MODE_SELECT;
    char temp = 0;
    SPIM_Inst.enable_cs(ASAID);
    ASA_SPIM_rec(mode, ASAID, RegAdd, 1, &temp, WaitTick);
    temp = (temp & ~Mask) + ((*Data_p << Shift) & Mask);
    ASA_SPIM_trm(mode, ASAID, RegAdd, 1, &temp,WaitTick);
    SPIM_Inst.disable_cs(ASAID);
    return 0;
}
