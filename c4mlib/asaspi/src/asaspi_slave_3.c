/**
 * @file asaspi_slave_3.c
 * @author Deng Xiang-Guan
 * @date 2019.10.02
 * @brief 提供Remote register SPI mode 3 slave端的狀態機，
 *        詳細操作請參照ASA_SPI M_S設計書。
 */

#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asaspi/src/asaspi_slave.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/hardware/src/hal_spi.h"

void ASA_SPIS3_step(void) {
    uint8_t tempData = 0;
    switch (ASASPISerialIsrStr->rw_mode) {
        // write mode, slave read data from master
        case REMOTE_REG_WRITE:
            tempData = SPIS_Inst.read_byte();
            ASASPISerialIsrStr->remo_reg[2]
                .data_p[ASASPISerialIsrStr->byte_counter] = tempData;
            DEBUG_INFO("tempData:%d, byte_counter:%d\n", tempData,
                       ASASPISerialIsrStr->byte_counter);
            ASASPISerialIsrStr->byte_counter++;
            if (ASASPISerialIsrStr->byte_counter ==
                ASASPISerialIsrStr->remo_reg[2].sz_reg) {
                ASASPISerialIsrStr->byte_counter = 0;
            }
            break;
        // read mode, master recieve data from master
        case REMOTE_REG_READ:
            if (ASASPISerialIsrStr->byte_counter ==
                ASASPISerialIsrStr->remo_reg[2].sz_reg) {
                SPIS_Inst.write_byte(0);
            }
            else {
                SPIS_Inst.write_byte(
                    ASASPISerialIsrStr->remo_reg[2]
                        .data_p[ASASPISerialIsrStr->byte_counter]);
                DEBUG_INFO("send:%d, byte_counter:%d\n",
                           ASASPISerialIsrStr->remo_reg[2]
                               .data_p[ASASPISerialIsrStr->byte_counter],
                           ASASPISerialIsrStr->byte_counter);
            }
            ASASPISerialIsrStr->byte_counter++;
            if (ASASPISerialIsrStr->byte_counter ==
                ASASPISerialIsrStr->remo_reg[2].sz_reg + 1) {
                ASASPISerialIsrStr->byte_counter = 0;
            }
            break;
        default:
            break;
    }
}
