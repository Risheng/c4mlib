/**
 * @file asaspi_slave.h
 * @author Deng Xiang-Guan
 * @date 2019.10.02
 * @brief 實現remote-register(遠端讀寫暫存器)，SPI Slave(僕)的狀態機函式。
 */

#ifndef C4MLIB_ASASPI_ASASPI_SLAVE_H
#define C4MLIB_ASASPI_ASASPI_SLAVE_H

#include "c4mlib/device/src/device.h"

/* Mode state descroption */
#define READ_MODE \
    0  ///< ASA SPI Slave(僕)接收到來自 ASA SPI Master(主) 的命令，唯讀模式。
       ///< @ingroup asaspi_macro
#define SPIS_STATE_IDLE \
    0  ///< ASA SPI Slave(僕)狀態機，為閒置狀態。 @ingroup asaspi_macro
#define SPIS_STATE_WRITE \
    1  ///< ASA SPI Slave(僕)狀態機，為傳輸狀態，接收來自Master(主)的資料。
       ///< @ingroup asaspi_macro
#define SPIS_STATE_READ \
    2  ///< ASA SPI Slave(僕)狀態機，唯讀取狀態，傳輸資料至Master(主)。 @ingroup
       ///< asaspi_macro
#define SPIS_STATE_CNT_MORE_THAN_BYTES \
    3  ///< ASA SPI Slave(僕)狀態機內的計數器超過使用者設定的Byte數。 @ingroup
       ///< asaspi_macro
#define SPIS_STATE_CNT_EQUAL_BYTES \
    4  ///< ASA SPI Slave(僕)狀態機內的計數器等於使用者設定的Byte數。 @ingroup
       ///< asaspi_macro
#define SPIS_STATE_CHECK_OK \
    5  ///< ASA SPI Slave(僕)狀態機，為確認資料正確狀態。 @ingroup asaspi_macro
#define SPIS_STATE_NO_CONDITION \
    6  ///< ASA SPI Slave(僕)狀態機，為沒有條件符合的狀態。 @ingroup
       ///< asaspi_macro
#define SPIS_STATE_CHECK_FAIL \
    7  ///< ASA SPI Slave(僕)狀態機，為確認資料錯誤狀態。 @ingroup asaspi_macro
#define SPIS_STATE_TIMEOUT \
    8  ///< ASA SPI Slave(僕)狀態機，為timeout(超過時間)狀態。 @ingroup
       ///< asaspi_macro
#define SPIS_STATE_DATA \
    9  ///< ASA SPI Slave(僕)狀態機，為資料處理的狀態。 @ingroup asaspi_macro

/* Public Section Start */
/**
 * @brief SPI Slave(僕)中斷狀態機處理函式，支援SPI Master(主) mode 0。
 *
 * @ingroup asaspi_func
 *
 * SPI Slave(僕)狀態機處理的函式，Master使用Mode 0傳輸給Slave裝置，具檢
 * 查機制，當Master cs(chip select)拉低觸發中斷，會執行此函式，第一筆會
 * 由Master發送命令[R/W + RegAdd]，決定是要Read或Write Slave端的哪個暫
 * 存器，之後根據Master端傳送的資料，接收或傳送給Master。
 */
void ASA_SPIS0_step(void);

/**
 * @brief SPI Slave(僕)中斷狀態機處理函式，支援SPI Master(主) mode 1。
 *
 * @ingroup asaspi_func
 *
 * SPI Slave(僕)狀態機處理的函式，Master使用Mode 1傳輸給Slave裝置，當
 * Master cs(chip select)拉低觸發中斷，會執行此函式，第一筆會由Master
 * 發送命令[3bits 控制旗標 + RegAdd]，之後對Slave端註冊的RegAdd寫入資
 * 料。
 */
void ASA_SPIS1_step(void);

/**
 * @brief SPI Slave(僕)中斷狀態機處理函式，支援SPI Master(主) mode 2。
 *
 * @ingroup asaspi_func
 *
 * SPI Slave(僕)狀態機處理的函式，Master使用Mode 2傳輸給Slave裝置，當
 * Master cs(chip select)拉低觸發中斷，會執行此函式，第一筆會由Master
 * 發送命令[RegAdd + 3bits 控制旗標]，之後對Slave端註冊的RegAdd寫入資
 * 料。
 */
void ASA_SPIS2_step(void);

/**
 * @brief SPI Slave(僕)中斷狀態機處理函式，支援SPI Master(主) mode 3。
 *
 * @ingroup asaspi_func
 *
 * SPI Slave(僕)狀態機處理的函式，使用者須先設定是要Read還是Write，之後
 * 使用Mode 3傳輸或接收Slave裝置，當Master cs(chip select)拉低觸發中斷
 * ，會執行此函式，之後由低到高傳輸或接收Slave資料。
 */
void ASA_SPIS3_step(void);

/**
 * @brief SPI Slave(僕)中斷狀態機處理函式，支援SPI Master(主) mode 4。
 *
 * @ingroup asaspi_func
 *
 * SPI Slave(僕)狀態機處理的函式，使用者須先設定是要Read還是Write，之後
 * 使用Mode 4傳輸或接收Slave裝置，當Master cs(chip select)拉低觸發中斷
 * ，會執行此函式，之後由高到低傳輸或接收Slave資料。
 */
void ASA_SPIS4_step(void);

/**
 * @brief SPI Slave(僕)中斷狀態機處理函式，支援SPI Master(主) mode 7。
 *
 * @ingroup asaspi_func
 *
 * SPI Slave(僕)狀態機處理的函式，Master使用Mode 7傳輸給Slave裝置，當
 * Master cs(chip select)拉低觸發中斷，會執行此函式，第一筆會由Master
 * 發送命令[R/W + RegAdd]，之後對Slave端註冊的RegAdd由低到高寫入或讀取
 * 資料。
 */
void ASA_SPIS7_step(void);

/**
 * @brief SPI Slave(僕)中斷狀態機處理函式，支援SPI Master(主) mode 8。
 *
 * @ingroup asaspi_func
 *
 * SPI Slave(僕)狀態機處理的函式，Master使用Mode 7傳輸給Slave裝置，當
 * Master cs(chip select)拉低觸發中斷，會執行此函式，第一筆會由Master
 * 發送命令[R/W + RegAdd]，之後對Slave端註冊的RegAdd由高到低寫入或讀取
 * 資料。
 */
void ASA_SPIS8_step(void);

/**
 * @brief SPI Slave(僕)中斷狀態機處理函式，支援SPI Master(主) mode 9。
 *
 * @ingroup asaspi_func
 *
 * SPI Slave(僕)狀態機處理的函式，Master使用Mode 7傳輸給Slave裝置，當
 * Master cs(chip select)拉低觸發中斷，會執行此函式，第一筆會由Master
 * 發送命令[RegAdd + R/W]，之後對Slave端註冊的RegAdd由低到高寫入或讀取
 * 資料。
 */
void ASA_SPIS9_step(void);

/**
 * @brief SPI Slave(僕)中斷狀態機處理函式，支援SPI Master(主) mode 10。
 *
 * @ingroup asaspi_func
 *
 * SPI Slave(僕)狀態機處理的函式，Master使用Mode 7傳輸給Slave裝置，當
 * Master cs(chip select)拉低觸發中斷，會執行此函式，第一筆會由Master
 * 發送命令[RegAdd + R/W]，之後對Slave端註冊的RegAdd由高到低寫入或讀取
 * 資料。
 */
void ASA_SPIS10_step(void);
/* Public Section End */

#endif  // C4MLIB_ASASPI_ASASPI_SLAVE_H
