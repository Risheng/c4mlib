/**
 * @file asaspi_master.h
 * @author Deng Xiang-Guan
 * @date 2019.10.02
 * @brief 宣告ASA SPI Master(主)的驅動函式，提供SPI不同通訊的方式，
 * 提供使用者呼叫已設定ASA單板電腦SPI的工作模式，並提供多種不同模式通
 * 訊協定配合ASA介面卡協定，達成資料送收之目的。
 */

#ifndef C4MLIB_ASASPI_ASASPI_MASTER_H
#define C4MLIB_ASASPI_ASASPI_MASTER_H

#include "c4mlib/device/src/device.h"

/**
 * @defgroup asaspi_func asaspi functions
 */

/* Public Section Start */
/**
 * @brief ASA SPI Master 多位元組傳送函式。
 *
 * @ingroup asaspi_func
 * @param mode    SPI通訊模式，目前支援：0~10。
 * @param ASAID   ASA介面卡的ID編號。
 * @param RegAdd  遠端讀寫暫存器(Register)的位址或控制旗標(control flag)。
 * @param Bytes   資料筆數。
 * @param WaitTick  位元組間延遲時間，單位為 1us。
 * @param *Data_p 待送資料指標。
 * @return char   錯誤代碼：
 *                 - 0：成功無誤。
 *                 - 5：模式選擇錯誤。
 *                 - 其他：錯誤。
 *
 * SPI Master傳輸給Slave裝置，依照功用分為10種mode，傳送方式如下：
 *  - mode 0 ：
 *      具check的SPI通訊方式，第一筆傳送給Slave端為[W | RegAdd]，
 *      之後開始傳送資料Data_p[0]直到Data[Bytes-1]，每次傳送給Slave端，
 *      Slave端會回傳上一筆資料給Master端，Master端會進行檢查，如果不一樣，
 *      再傳送最後一筆資料給Slave的時候，Slave會回傳非0的錯誤資訊。
 *
 *  - mode 1 ：
 *      SPI通訊的第一筆為[RegAdd | Data_p[Bytes-1]]，
 *      剩餘的傳輸資料由高到低傳輸。
 *      * 此 mode 中 RegAdd 為控制旗標(control flag)。
 *
 *  - mode 2 ：
 *      SPI通訊的第一筆為[RegAdd | Data_p[0]]，
 *      剩餘的傳輸資料由低到高傳輸。
 *      * 此 mode 中 RegAdd 為控制旗標(control flag)。
 *
 *  - mode 3 ：SPI通訊的傳輸資料由低到高傳輸，純送資料，未送RegAdd。
 *  - mode 4 ：SPI通訊的傳輸資料由高到低傳輸，純送資料，未送RegAdd。
 *  - mode 5 ：SPI通訊的第一筆為[RegAdd]，傳輸資料由低到高傳輸。
 *  - mode 6 ：SPI通訊的第一筆為[RegAdd]，傳輸資料由高到低傳輸。
 *  - mode 7 ：SPI通訊的第一筆為[W | RegAdd]，傳輸資料由低到高傳輸。
 *  - mode 8 ：SPI通訊的第一筆為[W | RegAdd]，傳輸資料由高到低傳輸。
 *  - mode 9 ：SPI通訊的第一筆為[(RegAdd<<1) | W]，傳輸資料由低到高傳輸。
 *  - mode 10：SPI通訊的第一筆為[(RegAdd<<1) | W]，傳輸資料由高到低傳輸。
 *  - mode 100以上，為外掛式SPI Master(主)傳輸資料。
 */
char ASA_SPIM_trm(char mode, char ASAID, char RegAdd, char Bytes, void *Data_p,
                  uint16_t WaitTick);

/**
 * @brief ASA SPI Master 多位元組接收函式。
 *
 * @ingroup asaspi_func
 * @param mode    SPI通訊模式，目前支援：0、3~10。
 * @param ASAID   ASA介面卡的ID編號。
 * @param RegAdd  控制旗標(control flag)或遠端讀寫暫存器的ID.
 * @param Bytes   資料筆數。
 * @param WaitTick 位元組間延遲時間，單位為 1us。
 * @param *Data_p 待收資料指標。
 * @return char   錯誤代碼：
 *                 - 0：成功無誤。
 *                 - 5：模式選擇錯誤。
 *                 - 其他：錯誤。
 *
 * SPI Master從Slave裝置接收資料，依照功用分為10種mode，接收方式如下：
 *  - mode 0 ：
 *      具check的SPI通訊方式，第一筆傳送給Slave端為[RegAdd]，
 *      之後開始由低到高接收資料依序儲存到 *Data_p，
 *      接收完成後會傳送最後一筆接收的資料給Slave端驗證，
 *      Slave會回傳正確或錯誤資訊。
 *
 *  - mode 1 ：回傳5，表示模式選擇錯誤。
 *  - mode 2 ：回傳5，表示模式選擇錯誤。
 *  - mode 2 ：回傳5，表示模式選擇錯誤。
 *  - mode 3 ：SPI通訊的第一筆為[0x00]]，純接資料，由低到高依序存入*Data_p。
 *  - mode 4 ：SPI通訊的第一筆為[0x00]]，純接資料，由高到低依序存入*Data_p。
 *  - mode 5 ：SPI通訊的，和Slave交換資料由低到高傳輸。
 *  - mode 6 ：SPI通訊的，和Slave交換資料由高到低傳輸。
 *  - mode 7 ：SPI通訊的第一筆為[R | RegAdd]，之後和Slave交換資料由低到高傳輸。
 *  - mode 8 ：SPI通訊的第一筆為[R | RegAdd]，之後和Slave交換資料由高到低傳輸。
 *  - mode 9 ：
 *      SPI通訊和Slave交換資料第一筆為[(RegAdd<<1) | R]，
 *      之後交換資料由低到高傳輸。
 *  - mode 10：
 *      SPI通訊和Slave交換資料第一筆為[(RegAdd<<1) | R]，
 *      之後交換資料由高到低傳輸。
 *  - mode 100以上，為外掛式SPI Master(主)接收資料。
 */
char ASA_SPIM_rec(char mode, char ASAID, char RegAdd, char Bytes, void *Data_p,
                  uint16_t WaitTick);

/**
 * @brief ASA SPI Master 旗標式接收函式。
 *
 * @ingroup asaspi_func
 * @param mode     SPI通訊模式，目前支援：0、3~10。
 * @param ASAID    ASA介面卡的ID編號。
 * @param RegAdd   遠端讀寫暫存器(Register)的位址或控制旗標(control flag)。
 * @param Mask     位元組遮罩。
 * @param Shift    接收旗標向右位移。
 * @param WaitTick 位元組間延遲時間，單位為 1us。
 * @param *Data_p  待收資料指標。
 * @return char    錯誤代碼：
 *                  - 0：成功無誤。
 *                  - 5：模式選擇錯誤。
 *                  - 其他：錯誤。
 *
 * SPI Master從Slave裝置接收資料，依照功用分為10種mode，接收方式如下：
 *  - mode 0 ：
 *    具check的SPI通訊方式，先使用ASA_SPIM_rec mode 0 讀取資料，
 *    將資料左移後，用遮罩取資料，最後放到Data_p裡面。
 *
 *  - mode 1 ：回傳5，表示模式選擇錯誤。
 *  - mode 2 ：回傳5，表示模式選擇錯誤。
 *  - mode 3 ：
 *      使用ASA_SPIM_rec mode 3 讀取資料，將資料左移後，用遮罩取資料，
 *      最後放到Data_p裡面。
 *  - mode 4 ：回傳5，表示模式選擇錯誤。
 *  - mode 5 ：
 *      使用ASA_SPIM_rec mode 5 讀取資料，將資料左移後，用遮罩取資料，
 *      最後放到Data_p裡面。
 *  - mode 6 ：
 *      使用ASA_SPIM_rec mode 6 讀取資料，將資料左移後，用遮罩取資料，
 *      最後放到Data_p裡面。
 *  - mode 7 ：
 *      使用ASA_SPIM_rec mode 7 讀取資料，將資料左移後，用遮罩取資料，
 *      最後放到Data_p裡面。
 *  - mode 8 ：回傳5，表示模式選擇錯誤。
 *  - mode 9 ：
 *      使用ASA_SPIM_rec mode 9 讀取資料，將資料左移後，用遮罩取資料，
 *      最後放到Data_p裡面。
 *  - mode 10：回傳5，表示模式選擇錯誤。
 *  - mode 100以上，為外掛式SPI Master(主)旗標式資料接收。
 */
char ASA_SPIM_frc(char mode, char ASAID, char RegAdd, char Mask, char Shift,
                  char *Data_p, uint16_t WaitTick);

/**
 * @brief SPI Master(主)旗標式資料傳輸。
 *
 * @ingroup asaspi_func
 * @param mode     SPI通訊模式，目前支援：0、3~10。
 * @param ASAID    ASA介面卡的ID編號。
 * @param RegAdd   控制旗標(control flag)或遠端讀寫暫存器的ID.
 * @param Mask     位元組遮罩。
 * @param Shift    發送資料向左位移。
 * @param WaitTick 位元組間延遲時間，單位為 1us。
 * @param *Data_p  待送資料指標。
 * @return char    錯誤代碼。
 *                  - 0：成功無誤。
 *                  - 5：模式選擇錯誤。
 *                  - 其他：錯誤。
 *
 * SPI Master從Slave裝置接收資料，依照功用分為10種mode，接收方式如下：
 *  - mode 0 ：
 *      具check的SPI通訊方式，先使用ASA_SPIM_rec mode 0讀取資料，
 *      將資料左移後，用遮罩取資料，最後使用ASA_SPIM_trm mode 0傳輸資料。
 *  - mode 1 ：回傳5，表示模式選擇錯誤。
 *  - mode 2 ：回傳5，表示模式選擇錯誤。
 *  - mode 3 ：
 *      使用ASA_SPIM_rec mode 3讀取資料，將資料左移後，用遮罩取資料，
 *      最後使用ASA_SPIM_trm mode 3傳輸資料。
 *  - mode 4 ：回傳5，表示模式選擇錯誤。
 *  - mode 5 ：
 *      使用ASA_SPIM_rec mode 5讀取資料，將資料左移後，用遮罩取資料，
 *      最後使用ASA_SPIM_trm mode 5傳輸資料。
 *  - mode 6 ：
 *      使用ASA_SPIM_rec mode 6讀取資料，將資料左移後，用遮罩取資料，
 *      最後使用ASA_SPIM_trm mode 6傳輸資料。
 *  - mode 7 ：
 *      使用ASA_SPIM_rec mode 7讀取資料，將資料左移後，用遮罩取資料，
 *      最後使用ASA_SPIM_trm mode 7傳輸資料。
 *  - mode 8 ：回傳5，表示模式選擇錯誤。
 *  - mode 9 ：
 *      使用ASA_SPIM_rec mode 9讀取資料，將資料左移後，用遮罩取資料，
 *      最後使用ASA_SPIM_trm mode 9傳輸資料。
 *  - mode 10：回傳5，表示模式選擇錯誤。
 *  - mode 100以上，為外掛式SPI Master(主)旗標式資料傳輸。
 */
char ASA_SPIM_ftm(char mode, char ASAID, char RegAdd, char Mask, char Shift,
                  char *Data_p, uint16_t WaitTick);

/**
 * @brief SPI Master(主)向記憶體(flash)傳輸資料。
 *
 * @ingroup asaspi_func
 * @param mode      SPI通訊模式。
 * @param ASAID     ASA介面卡的ID編號。
 * @param RegAdd    命令或指令.
 * @param AddBytes  記憶體位置的資料筆數。
 * @param MemAdd_p  指標指向記憶體位置。
 * @param DataBytes 資料筆數。
 * @param *Data_p   指標指向資料。
 * @return char     錯誤代碼：
 *                   - 0：成功無誤。
 *                   - 5：模式選擇錯誤。
 *
 * SPI Master(主)向記憶體(flash)傳輸資料，傳輸模式如下：
 *  - mode 5 ：
 *      SPI通訊的第一筆為[RegAdd]，之後由低到高傳送 AddBytes 筆
 *      MemAdd_p (記憶體位置)資料，最後由低到高傳送DataBytes筆Data_p的資料。
 * 
 *  - mode 6 ：
 *      SPI通訊的第一筆為[RegAdd]，之後由高到低傳送 AddBytes 筆
 *      MemAdd_p (記憶體位置)資料，最後由高到低傳送DataBytes筆Data_p的資料。
 * 
 *  - mode x ：回傳5，表示模式選擇錯誤。
 */
char SPIM_Mem_trm(char mode, char ASAID, char RegAdd, char AddBytes,
                  void *MemAdd_p, char DataBytes, void *Data_p);

/**
 * @brief SPI Master(主)向記憶體(flash)接收資料。
 *
 * @ingroup asaspi_func
 * @param mode      SPI通訊模式。
 * @param ASAID     ASA介面卡的ID編號。
 * @param RegAdd    命令或指令.
 * @param AddBytes  記憶體位置的資料筆數。
 * @param MemAdd_p  指標指向記憶體位置。
 * @param DataBytes 資料筆數。
 * @param *Data_p   指標指向資料。
 * @return char     錯誤代碼：
 *                   - 0：成功無誤。
 *                   - 5：模式選擇錯誤。
 *
 * SPI Master(主)向記憶體(flash)傳輸資料，傳輸模式如下：
 *  - mode 5 ：
 *      SPI通訊的第一筆為[RegAdd]，之後由低到高傳送 AddBytes 筆
 *      MemAdd_p (記憶體位置)資料，最後由低到高接收DataBytes筆資料至Data_p。
 * 
 *  - mode 6 ：
 *      SPI通訊的第一筆為[RegAdd]，之後由高到低傳送 AddBytes 筆
 *      MemAdd_p (記憶體位置)資料，最後由高到低接收DataBytes筆資料至Data_p。
 * 
 *  - mode x ：回傳5，表示模式選擇錯誤。
 */
char SPIM_Mem_rec(char mode, char ASAID, char RegAdd, char AddBytes,
                  void *MemAdd_p, char DataBytes, void *Data_p);
/* Public Section End */

#endif  // C4MLIB_ASASPI_ASASPI_MASTER_H
