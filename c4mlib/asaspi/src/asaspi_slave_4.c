/**
 * @file asaspi_slave_4.c
 * @author Deng Xiang-Guan
 * @date 2019.10.02
 * @brief 提供Remote register SPI mode 4 slave端的狀態機，
 *        詳細操作請參照ASA_SPI M_S設計書。
 */

#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asaspi/src/asaspi_slave.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/hardware/src/hal_spi.h"

void ASA_SPIS4_step(void) {
    static uint8_t onetime_flag = true;
    uint8_t tempData = 0;
    switch (ASASPISerialIsrStr->rw_mode) {
        // write mode, slave read data from master
        case REMOTE_REG_WRITE:
            if (onetime_flag) {
                ASASPISerialIsrStr->byte_counter =
                    ASASPISerialIsrStr->remo_reg[2].sz_reg - 1;
                onetime_flag = false;
            }
            tempData = SPIS_Inst.read_byte();
            ASASPISerialIsrStr->remo_reg[2]
                .data_p[ASASPISerialIsrStr->byte_counter] = tempData;
            DEBUG_INFO("tempData:%d, byte_counter:%d\n", tempData,
                       ASASPISerialIsrStr->byte_counter);
            if (ASASPISerialIsrStr->byte_counter > 0) {
                ASASPISerialIsrStr->byte_counter--;
            }
            else {
                onetime_flag = true;
            }
            break;
        // read mode, master recieve data from master
        case REMOTE_REG_READ:
            if (onetime_flag) {
                ASASPISerialIsrStr->byte_counter =
                    ASASPISerialIsrStr->remo_reg[2].sz_reg - 1;
                onetime_flag = false;
            }
            if (ASASPISerialIsrStr->byte_counter == 255) {
                SPIS_Inst.write_byte(0);
                onetime_flag = true;
            }
            else {
                SPIS_Inst.write_byte(
                    ASASPISerialIsrStr->remo_reg[2]
                        .data_p[ASASPISerialIsrStr->byte_counter]);
                DEBUG_INFO("send:%d, byte_counter:%d\n",
                           ASASPISerialIsrStr->remo_reg[2]
                               .data_p[ASASPISerialIsrStr->byte_counter],
                           ASASPISerialIsrStr->byte_counter);
            }
            if (ASASPISerialIsrStr->byte_counter > 0) {
                ASASPISerialIsrStr->byte_counter--;
            }
            else if (ASASPISerialIsrStr->byte_counter == 0) {
                ASASPISerialIsrStr->byte_counter = 255;
            }
            break;
        default:
            break;
    }
}
