/**
 * @file test_master_spi.c
 * @author Deng Xiang-Guan
 * @date 2019.10.04
 * @brief 測試SPI Master IO腳與通訊正常與否。
 * 
 * 需搭配test_slave_spi.c，測試SPI Master(主)接收與傳輸資料，與GPIO正常與否，
 * 測試方式如下條列表示：
 *   1. 初始化IO，並打出High和Low訊號給Slave端，如果測試都正常Slave會回覆，程
 *      式繼續往下測試。
 *   2. 測試SPI暫存器設定是否正確，呼叫硬體SPI控制暫存器，檢查bit是否有成功寫入
 *      ，成功程式繼續往下測試。
 *   3. 測試SPI通訊，會和SPI Slave端通訊，如果測試資料傳輸過去與接收回來，都成
 *      功，程式繼續往下測試。
 *   4. 測試ASA_SPIM_trm和ASA_SPIM_rec mode 0，將資料傳輸到Slave再接收回來，
 *      成功此測試程式結束。
 */

#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asaspi/src/asaspi_master.h"
#include "c4mlib/asaspi/src/asaspi_slave.h"
#include "c4mlib/time/src/hal_time.h"

#include <stdlib.h>
#define SPI_TEST_OK 0
#define SPI_TEST_FAIL 1
#define ASAID 4
#define DELAY 10

uint32_t last_time = 0;
ISR(TIMER3_COMPA_vect) {
    HAL_tick();
}

uint8_t test_transmit[8] = {87, 11, 22, 33, 55, 66, 0, 0};
uint8_t test_rec[7] = {87, 11, 22, 33, 55, 66, 0};
// Test function
char test_Master_SPI_Reg(void);
char test_Master_SPI_IO(void);
char test_Master_SPI_transmission(void);
char test_Master_SPI_reciept(void);
void init_timer(void);
// Use DIO3(PF7)、DIO2(PF6) to keep slave wait master.
// When master let PF7 LOW, then start slave,
// it can let each other almost run together.
void wait_S_init(void);
bool wait_S_OK(void);

int main() {
    // Setup
    C4M_STDIO_init();
    HAL_time_init();
    init_timer();
    sei();
    uint16_t er_cnt = 0;
    bool er_flag = false;
    last_time = HAL_get_time();
    wait_S_init();
    printf("Start master ~\n");

    // Main code
    while (true) {
        /**
         * Test1
         * @brief:test spi io.
         */
        printf("\n\n\n====================================\n");
        printf("Start test master SPI IO ...\n");
        ASABUS_ID_init();
        ASABUS_ID_set(ASAID);
        while (test_Master_SPI_IO() != SPI_TEST_OK) {
            printf("PINB3 : %d, %d, %d\n", PINB & 0x08, DDRB, PORTB);
            if (HAL_get_time() - last_time > 500) {
                printf("Test master SPI IO error\n");
                er_cnt++;
                if (er_cnt > 10) {
                    printf("Error break.\n");
                    er_cnt = 0;
                    er_flag = true;
                    break;
                }
                last_time = HAL_get_time();
            }
            HAL_delay(3);
        }
        if (er_flag) {
            printf("test_Master_SPI_IO Fail !!! \n\n");
            er_flag = false;
            while (true)
                ;
        }
        else {
            printf("test_Master_SPI_IO Success !!! \n");
        }
        printf("====================================\n");

        /**
         * Test2
         * @brief:test spi register setting.
         */
        printf("\n\n\n====================================\n");
        printf("Start test master SPI register setting ...\n");
        SPIM_Inst.init();
        while (test_Master_SPI_Reg() != SPI_TEST_OK) {
            printf("Test master SPI register error, %d\n",
                   test_Master_SPI_Reg());
            printf("DDRB : %d\n", DDRB);
            printf("SPCR : %d\n", SPCR);
            printf("DDRF : %d\n", DDRF);
            if (HAL_get_time() - last_time > 500) {
                printf("Test master SPI IO error\n");
                er_cnt++;
                if (er_cnt > 10) {
                    printf("Error break.\n");
                    er_cnt = 0;
                    er_flag = true;
                    break;
                }
                last_time = HAL_get_time();
            }
        }
        if (er_flag) {
            printf("Test_Master_SPI_Reg Fail !!! \n\n");
            er_flag = false;
            while (true)
                ;
        }
        else {
            printf("Test_Master_SPI_Reg Success !!! \n\n");
        }
        printf("====================================\n");

        /**
         * Test3
         * @brief:test Master SPI transmission
         */
        printf("\n\n\n====================================\n");
        printf("Start test master SPI transmission & recept ...\n");
        while (test_Master_SPI_transmission() != SPI_TEST_OK) {
            if (HAL_get_time() - last_time > 500) {
                printf("Test master SPI transmission & recept error\n");
                er_cnt++;
                if (er_cnt > 10) {
                    printf("Error break.\n");
                    er_cnt = 0;
                    er_flag = true;
                    break;
                }
                last_time = HAL_get_time();
            }
        }
        if (er_flag) {
            printf("test_Master_SPI_transmission Fail !!! \n\n");
            er_flag = false;
            while (true)
                ;
        }
        else {
            printf("test_Master_SPI_transmission Success !!! \n\n");
        }
        printf("\n\n\n====================================\n");
        printf("Test Mode Start.\n");
        // Test mode 0
        wait_S_init();
        while (!wait_S_OK())
            ;  // Keep unitl slave ok
        wait_S_init();
        char data[4] = {10, 11, 12, 13};
        char *rec_data = (char *)malloc(4);
        char rec_ans[4] = {135, 15, 240, 255};
        char ok_cnt = 0;
        while (true) {
            if (HAL_get_time() - last_time > 1000) {
                int chk_trm, chk_rec;
                chk_trm = ASA_SPIM_trm(0, ASAID, 0, 4, &data[0], DELAY);
                HAL_delay(500);
                chk_rec = ASA_SPIM_rec(0, ASAID, 0, 4, rec_data, DELAY);
                printf("chk_trm=%d, chk_rec=%d\n", chk_trm, chk_rec);
                for (uint8_t j = 0; j < 4; j++) {
                    printf("data[%d]=%d, rec[%d]=%d\n", j, data[j], j,
                           rec_data[j]);
                    if (rec_data[j] != rec_ans[j]) {
                        chk_rec = 2;
                    }
                }
                last_time = HAL_get_time();
                printf("\n\n");
                for (uint8_t i = 0; i < sizeof(data); i++) {
                    data[i] = data[i] + 4;
                }

                if (chk_trm != 0 || chk_rec != 0) {
                    printf("Master test mode 0 fail!!!\n");
                    while (true)
                        ;
                }
                else {
                    printf("Master test mode 0 succeed!!! ->%d\n", ok_cnt);
                    ok_cnt++;
                }

                // over 5 times ok
                if (ok_cnt > 5) {
                    break;
                }
            }
        }

        printf("Master Test OK.\n");
        printf("====================================\n");
        while (true)
            ;
    }
    return 0;
}

char test_Master_SPI_Reg(void) {
    // 0 is input pin
    // 1 is output pin
    if (((BUS_SPI_DDR & (1 << BUS_SPI_MOSI)) >> BUS_SPI_MOSI) == 0)
        return 2;
    if (((BUS_SPI_DDR & (1 << BUS_SPI_SCK)) >> BUS_SPI_SCK) == 0)
        return 3;
    if (((BUS_SPI_DDR & (1 << BUS_SPI_SS)) >> BUS_SPI_SS) == 0)
        return 4;
    if (((BUS_SPI_DDR & (1 << BUS_SPI_MISO)) >> BUS_SPI_MISO) == 1)
        return 5;
    if (((ASA_CS_DDR & (1 << ASA_CS)) >> ASA_CS) == 0)
        return SPI_TEST_FAIL;
    return SPI_TEST_OK;
}

char test_Master_SPI_IO(void) {
    static char toggle_flag = 1;
    static char io_cnt = 0;
    static uint16_t tmp_cnt;
    static bool ok_f = false;
    // Setup SPI pins
    DDRB |= (1 << BUS_SPI_MOSI) | (1 << BUS_SPI_SCK) | (1 << BUS_SPI_SS);
    DDRB &= ~(1 << BUS_SPI_MISO);
    // Master CS pin is PF4
    ASA_CS_DDR |= (1 << ASA_CS);

    if (toggle_flag) {
        BUS_SPI_PORT |= (1 << BUS_SPI_MOSI);
        BUS_SPI_PORT |= (1 << BUS_SPI_SCK);
        BUS_SPI_PORT |= (1 << BUS_SPI_SS);
        ASA_CS_PORT &= ~(1 << ASA_CS);
    }
    else {
        BUS_SPI_PORT &= ~(1 << BUS_SPI_MOSI);
        BUS_SPI_PORT &= ~(1 << BUS_SPI_SCK);
        BUS_SPI_PORT &= ~(1 << BUS_SPI_SS);
        ASA_CS_PORT |= (1 << ASA_CS);
    }
    toggle_flag ^= 1;

    if (((PINB & (1 << BUS_SPI_MISO)) >> BUS_SPI_MISO) == 1) {
        io_cnt++;
    }
    else if (((PINB & (1 << BUS_SPI_MISO)) >> BUS_SPI_MISO) == 0 &&
             io_cnt == 1) {
        ok_f = true;
    }
    else {
        io_cnt = 0;
    }

    tmp_cnt++;
    if (ok_f == true && tmp_cnt > 100) {
        return SPI_TEST_OK;
    }
    return SPI_TEST_FAIL;
}

char test_Master_SPI_transmission(void) {
    uint16_t c = 0;
    uint8_t data[sizeof(test_rec)];
    uint8_t data_recieve[sizeof(test_transmit)];
    for (uint8_t i = 0; i < sizeof(test_transmit); i++) {
        SPIM_Inst.enable_cs(ASAID);
        data_recieve[i] = SPIM_Inst.spi_swap(test_transmit[i]);
        printf("Send[%d]: %d, Revcieve[%d]: %d\n", i, test_transmit[i], i,
               data_recieve[i]);
        SPIM_Inst.disable_cs(ASAID);
        if (i > 0) {
            data[i - 1] = data_recieve[i];
        }
        _delay_ms(100);
    }
    for (uint8_t i = 0; i < sizeof(test_rec); i++) {
        if (data[i] == test_rec[i]) {
            c++;
        }
    }
    if (c == 7) {
        return SPI_TEST_OK;
    }
    else {
        return SPI_TEST_FAIL;
    }
}

char test_Master_SPI_reciept(void) {
    return SPI_TEST_OK;
}

void init_timer(void) {
    ICR3 = 11058 * 2;
    TCCR3A = 0b00000000;
    TCCR3B = 0b00011001;
    TCNT3 = 0x00;
    ETIMSK |= 1 << OCIE3A;
}

void wait_S_init(void) {
    DDRF |= (1 << PF7);
    DDRF &= ~(1 << PF6);
    PORTF = 0;
}
bool wait_S_OK(void) {
    PORTF |= (1 << PF7);
    if (PINF & (1 << PF6)) {
        printf("Start testing mode\n");
        return true;
    }
    return false;
};
