/**
 * @file test_slave_remorw_mode9.c
 * @author Deng Xiang-Guan
 * @date 2019.10.03
 * @brief 測試ASA_SPIM_trm和ASA_SPIM_rec mode 9函式。
 * 
 * 需搭配test_master_remorw_mode9.c，測試SPI Master(主)接收與傳輸資料，
 * 具檢查機制，測試方式如下條列表示：
 *   1. Remote Register的結構宣告，並執行初始化結構的鏈結。
 *   2. 註冊需要的暫存器，需給予空間，本次測試註冊三個暫存器，大小分別為1、2、4。
 *   3. 註冊後回傳的每個暫存器的編號，此編號為Master端讀寫暫存器的位置。
 *   4. 接收Master端的測試資料。
 *   5. 檢查傳輸過去的資料和接收回來是否一致。
 *   6. 正確的話將成功計數器增加並於終端機顯示。
 *   7. 更新測試程式，回到流程4。
 */

#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asaspi/src/asaspi_slave.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/eeprom.h"
#include "c4mlib/hardware/src/hal_spi.h"
#include "c4mlib/time/src/hal_time.h"

void init_timer(void);

uint8_t test_ans1[1] = {87};
uint8_t test_ans2[2] = {1, 2};
uint8_t test_ans3[4] = {15, 16, 17, 18};

uint8_t rec[4];
char c = 0;
uint8_t uidBuf[10];

ISR(TIMER3_COMPA_vect) {
    HAL_tick();
}
int main() {
    // Setup
    C4M_STDIO_init();
    HAL_time_init();
    init_timer();
    printf("Start slave mode 9\n");
    for (uint8_t i = 0; i < sizeof(uidBuf); i++)
        uidBuf[i] = 0;

    // Initailize Serial SPI remote register type
    SerialIsr_t SPIIsrStr = SERIAL_ISR_STR_SPI_INI;
    SerialIsr_net(&SPIIsrStr, ASA_SPIS9_step);

    uint8_t reg_1[1] = {0};
    uint8_t reg_2[2] = {0, 0};
    uint8_t reg_3[4] = {0, 0, 0, 0};

    uint8_t reg_1_Id = RemoRW_reg(&SPIIsrStr, reg_1, 1);
    printf("Create RemoRWreg [%u] with %u bytes \n", reg_1_Id,
           SPIIsrStr.remo_reg[reg_1_Id].sz_reg);

    uint8_t reg_2_Id = RemoRW_reg(&SPIIsrStr, reg_2, 2);
    printf("Create RemoRWreg [%u] with %u bytes \n", reg_2_Id,
           SPIIsrStr.remo_reg[reg_2_Id].sz_reg);

    uint8_t reg_3_Id = RemoRW_reg(&SPIIsrStr, reg_3, 4);
    printf("Create RemoRWreg [%u] with %u bytes \n", reg_3_Id,
           SPIIsrStr.remo_reg[reg_3_Id].sz_reg);

    char er_flag = false;
    char ok_flag = 0;
    printf("Start test slave mode 7 remote read write register!!!\n");
    HAL_delay(3000UL);
    while (true) {
        // Show the register data

        for (uint8_t i = 0; i < sizeof(reg_1); i++) {
            printf("reg_1[%d]=%d, ", i, reg_1[i]);
            if (reg_1[i] == test_ans1[i]) {
                ok_flag++;
            }
            else {
                er_flag = true;
            }
            test_ans1[i] += 1;
        }
        printf("\n");
        for (uint8_t i = 0; i < sizeof(reg_2); i++) {
            printf("reg_2[%d]=%d, ", i, reg_2[i]);
            if (reg_2[i] == test_ans2[i]) {
                ok_flag++;
            }
            else {
                er_flag = true;
            }
            test_ans2[i] += 2;
        }
        printf("\n");
        for (uint8_t i = 0; i < sizeof(reg_3); i++) {
            printf("reg_3[%d]=%d, ", i, reg_3[i]);
            if (reg_3[i] == test_ans3[i]) {
                ok_flag++;
            }
            else {
                er_flag = true;
            }
            test_ans3[i] += 4;
        }
        printf("\n");
        printf("\n=================\n");
        if (er_flag) {
            printf("Test slave mode 9 remo-reg fail !!!\n");
            // while(1);
        }
        else {
            printf("Succeed --> %d\n", ok_flag);
        }
        if (ok_flag >= 35) {
            printf("Test slave mode 9 remo-reg succeed !!!\n");
            break;
        }
        HAL_delay(3000UL);
    }
}

void init_timer(void) {
    ICR3 = 11058 * 2;
    TCCR3A = 0b00000000;
    TCCR3B = 0b00011001;
    TCNT3 = 0x00;
    ETIMSK |= 1 << OCIE3A;
}
