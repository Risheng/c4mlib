/**
 * @file test_master_remorw_mode4_w.c
 * @author Deng Xiang-Guan
 * @date 2019.10.04
 * @brief 測試ASA_SPIM_trm mode 4函式。
 * 
 * 需搭配test_slave_remorw_mode4_r.c，測試SPI Master(主)mode 4純傳輸資料，
 * 由高到低，和Slave端交換資料，測試方式如下條列表示：
 *   1. 純送測試資料。
 *   2. 更新測試資料。
 */

#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asaspi/src/asaspi_master.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/hal_spi.h"
#include "c4mlib/time/src/hal_time.h"

#define SPI_MODE 4
#define ASAID 4
#define DELAY 10

void init_timer(void);

uint32_t last_time = 0;
ISR(TIMER3_COMPA_vect) {
    HAL_tick();
}

uint8_t test_trm1[4] = {15, 16, 17, 18};

int main() {
    // Setup
    C4M_STDIO_init();
    HAL_time_init();
    init_timer();
    printf("Start master write, mode 4\n");
    SPIM_Inst.init();
    sei();
    last_time = HAL_get_time();
    while (true) {
        char chk;

        /* Test write remote register SPI mode 2 */
        chk = ASA_SPIM_trm(SPI_MODE, ASAID, 0, sizeof(test_trm1), &test_trm1[0],
                           DELAY);
        printf("chk===%d\n", chk);
        _delay_ms(3000);

        for (uint8_t i = 0; i < sizeof(test_trm1); i++) {
            test_trm1[i] += 4;
        }
    }
}

void init_timer(void) {
    ICR3 = 11058 * 2;
    TCCR3A = 0b00000000;
    TCCR3B = 0b00011001;
    TCNT3 = 0x00;
    ETIMSK |= 1 << OCIE3A;
}
