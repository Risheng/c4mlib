/**
 * @file test_master_remorw_ftm_frc_mode7.c
 * @author Deng Xiang-Guan
 * @date 2019.10.03
 * @brief 測試ASA_SPIM_ftm和ASA_SPIM_frc mode 7函式。
 * 
 * 需搭配test_slave_remorw_ftm_frc_mode7.c，測試SPI Master(主)
 * mode 0旗標接收與旗標傳輸資料，具檢查機制，和Slave端交換資料，
 * 測試方式如下條列表示：
 *   1. 使用ASA_SPIM_trm函式，對Slave暫存器編號2寫入1 byte測試資料。
 *   2. 使用ASA_SPIM_trm函式，對Slave暫存器編號3寫入1 byte測試資料。
 *   3. 使用ASA_SPIM_ftm函式，旗標式寫入暫存器編號2的測試資料。
 *   4. 使用ASA_SPIM_ftm函式，旗標式寫入暫存器編號3的測試資料。
 *   5. 使用ASA_SPIM_frc函式，旗標式讀取暫存器編號2的測試資料。
 *   6. 使用ASA_SPIM_frc函式，旗標式讀取暫存器編號3的測試資料。
 *   7. 檢查傳輸過去的資料和接收回來，是否只更改設定的遮罩、位移。
 *   8. 正確的話將成功計數器增加並於終端機顯示。
 *   9. 更新測試程式，回到流程3。
 */

#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asaspi/src/asaspi_master.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/hal_spi.h"
#include "c4mlib/time/src/hal_time.h"

#include <stdlib.h>

#define SPI_MODE 7
#define ASAID 4
#define DELAY 10

void init_timer(void);

ISR(TIMER3_COMPA_vect) {
    HAL_tick();
}

uint8_t test_trm1[1] = {(0x0f << 4)};
uint8_t test_trm2[1] = {(0x0f)};

int main() {
    // Setup
    char chk;
    C4M_STDIO_init();
    HAL_time_init();
    init_timer();
    printf("Start master mode 7\n");
    SPIM_Inst.init();
    sei();
    chk = ASA_SPIM_trm(SPI_MODE, ASAID, 2, 1, test_trm1, DELAY);
    printf("trm1 chk===%d\n", chk);
    chk = ASA_SPIM_trm(SPI_MODE, ASAID, 3, 1, test_trm2, DELAY);
    printf("trm2 chk===%d\n", chk);
    while (true) {
        /* Test write remote register SPI mode 0 */
        char *temp_data = (char *)malloc(sizeof(temp_data));
        printf("==== remo one ====\n");
        temp_data[0] = 0x0f;
        chk = ASA_SPIM_ftm(SPI_MODE, ASAID, 2, 0x0f, 0, temp_data, DELAY);
        printf("chk===%d\n", chk);
        printf("==== remo two ====\n");
        temp_data[0] = 0xf0;
        chk = ASA_SPIM_ftm(SPI_MODE, ASAID, 3, 0xf0, 0, temp_data, DELAY);
        printf("chk===%d\n", chk);

        char ans1, ans2;
        ASA_SPIM_frc(SPI_MODE, ASAID, 2, 0x0f, 0, &ans1, DELAY);
        printf("->%d\n", ans1);
        ASA_SPIM_frc(SPI_MODE, ASAID, 2, 0xff, 0, &ans1, DELAY);
        printf("->%d\n", ans1);
        ASA_SPIM_frc(SPI_MODE, ASAID, 3, 0xf0, 0, &ans2, DELAY);
        printf("->%d\n", ans2);
        ASA_SPIM_frc(SPI_MODE, ASAID, 3, 0xff, 0, &ans2, DELAY);
        printf("->%d\n", ans2);

        printf("==================\n");
        if ((ans1 != 0xff) || (ans2 != 0xff)) {
            printf("Test error !!!\n");
            while (true)
                ;
        }
        else {
            printf("Test Success !!!\n");
        }

        _delay_ms(3000);
    }
}

void init_timer(void) {
    ICR3 = 11058 * 2;
    TCCR3A = 0b00000000;
    TCCR3B = 0b00011001;
    TCNT3 = 0x00;
    ETIMSK |= 1 << OCIE3A;
}
