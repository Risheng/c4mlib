/**
 * @file test_master_mem_trm_mode6.c
 * @author Deng Xiang-Guan
 * @date 2019.10.03
 * @brief 測試SPIM_Mem_trm mode 6函式。
 * 
 * 需搭配test_slave_mem_trm_mode6.c，測試SPI Master(主)記憶接收資料，
 * 由高到低，接收來自Slave端的資料，測試方式如下條列表示：
 *   1. 使用SPIM_Mem_rec的函式，和Slave端搭配command、memory address，之後開始接收Slave端的資料。
 *   2. 檢查接收的資料和測試資料是否正確。
 *   3. 正確的話將成功計數器增加。
 *   4. 更新測試資料，回到流程1。
 */

#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asaspi/src/asaspi_master.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/hal_spi.h"

#define SPI_MODE 6
#define ASAID 4

#define test_command 0x02

uint32_t mem_addr = 12341487;
uint8_t test_data[5] = {0, 1, 2, 3, 4};

int main() {
    // Setup
    C4M_STDIO_init();
    SPIM_Inst.init();
    printf("Start test SPIM_Mem_trm mode 5\n");
    while(true) {
        SPIM_Mem_trm(SPI_MODE, ASAID, test_command, 4, &mem_addr, sizeof(test_data), test_data);
        for(int i=0;i<sizeof(test_data);i++) {
            test_data[i] += sizeof(test_data);
        }
        _delay_ms(3000);
    }

}
