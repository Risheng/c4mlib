/**
 * @file test_master_mem_trm.c
 * @author Deng Xiang-Guan
 * @date 2019.08.26
 * @brief 測試SPIM_Mem_trm mode 5函式。
 * 
 * 需搭配test_master_mem_trm_mode5.c，測試SPI Master(主)記憶傳輸資料，
 * 由低到高，接收來自Master端的資料，測試方式如下條列表示：
 *   1. 是否發生SPI中斷，條件為SPI Master端 chip select為Low觸發。
 *   2. 發生中斷後，檢查Master傳送的command、memery address，如果都正確
 *      ，接收Master端的資料。
 *   3. 檢查Master端的資料是否和測試資料一致。
 *   4. 如果資料正確，成功計數器會增加，且於終端機顯示出成功次數。  
 *   5. 回流程1。
 */

#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asaspi/src/asaspi_master.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/hal_spi.h"

#define test_command 0x02

uint32_t mem_addr = 12341487;
uint8_t test_data[5] = {0, 1, 2, 3, 4};

typedef struct SPI_Mem_trm_test {
    volatile uint8_t index;
    volatile uint8_t command;
    volatile uint32_t reg_addr;
    volatile uint8_t data[5];
}test_spi_mem_t;

test_spi_mem_t mem_str = {0, 0, 0, {0, 0, 0, 0, 0}};

ISR(SPI_STC_vect) {
    uint8_t data = SPIS_Inst.read_byte();
    // printf("%d, %d\n", data, mem_str.index);
    if(mem_str.index == 0) {
        mem_str.command = data;
    }
    else if(mem_str.index < 5) {
        if(mem_str.index == 1) {
            mem_str.reg_addr = data;
        }
        else {
            mem_str.reg_addr += (uint32_t)data << (8*(mem_str.index-1));
        }
    }
    else if(mem_str.index < 10) {
        mem_str.data[mem_str.index-5] = data;
    }
    mem_str.index++;
    if(mem_str.index == 10) {
        mem_str.index = 0;
    }
}

int main() {
    // Setup
    uint16_t success_cnt = 0;
    C4M_STDIO_init();
    printf("Start test SPIS_Mem_trm mode 5\n");
    SPIS_Inst.init();
    sei();
    _delay_ms(3000);
    while(true) {
        uint8_t temp = 0;
        cli();
        printf("cmd:%x, regAdd:%lu, data[0]:%d, data[1]:%d, data[2]:%d, data[3]:%d, data[4]:%d\n", 
                mem_str.command, mem_str.reg_addr, mem_str.data[0], mem_str.data[1], mem_str.data[2], mem_str.data[3], mem_str.data[4]);
        if(mem_str.command == test_command) {
            if(mem_str.reg_addr == mem_addr) {
                for(uint8_t i=0;i<sizeof(test_data);i++) {
                    if(mem_str.data[i] == test_data[i]) {
                        temp++;
                        test_data[i] += sizeof(test_data);
                    }
                }
                if(temp == 5) {
                    success_cnt++;
                }
            }
        }
        sei();
        if(success_cnt != 0) {
            printf("Success !!! count:%d\n", success_cnt);
        }
        _delay_ms(3000);

    }

}
