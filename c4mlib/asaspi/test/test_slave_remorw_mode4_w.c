/**
 * @file test_slave_remorw_mode4_w.c
 * @author Deng Xiang-Guan
 * @date 2019.10.04
 * @brief 測試ASA_SPIM_rec mode 4函式。
 * 
 * 需搭配test_master_remorw_mode4_r.c，測試SPI Master(主)mode 2純傳輸資料，
 * 由低到高，和Slave端交換資料，測試方式如下條列表示：
 *   1. Remote Register的結構宣告，並執行初始化結構的鏈結。
 *   2. 註冊需要的暫存器，需給予空間，本次測試註冊一個暫存器，大小為4。
 *   3. 註冊後回傳的每個暫存器的編號，此編號為Master端讀暫存器的位置。
 *   4. 是否發生SPI中斷，條件為SPI Master端 chip select為Low觸發。
 *   5. 中斷時將發送Master端欲讀取的資料。
 *   6. 更新Slave端暫存器的資料，回到流程4。
 */

#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asaspi/src/asaspi_slave.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/eeprom.h"
#include "c4mlib/hardware/src/hal_spi.h"
#include "c4mlib/time/src/hal_time.h"

void init_timer(void);
uint8_t reg_1[4] = {15, 16, 17, 18};

uint8_t rec[4];
char c = 0;
uint8_t uidBuf[10];

ISR(TIMER3_COMPA_vect) {
    HAL_tick();
}
int main() {
    // Setup
    C4M_STDIO_init();
    HAL_time_init();
    init_timer();
    printf("Start slave write, mode 4\n");
    for (uint8_t i = 0; i < sizeof(uidBuf); i++)
        uidBuf[i] = 0;

    // Initailize Serial SPI remote register type
    SerialIsr_t SPIIsrStr = SERIAL_ISR_STR_SPI_INI;
    SerialIsr_net(&SPIIsrStr, ASA_SPIS4_step);

    uint8_t reg_1_Id = RemoRW_reg(&SPIIsrStr, reg_1, 4);
    printf("Create RemoRWreg [%u] with %u bytes \n", reg_1_Id,
           SPIIsrStr.remo_reg[reg_1_Id].sz_reg);

    // Write data to master

    printf("Write data to master\n");
    printf("\n");
    printf("\n=================\n");
    ASASPISerialIsrStr->rw_mode = 1;
    while (true) {
        _delay_ms(3000);
        for (uint8_t i = 0; i < 4; i++) {
            SPIIsrStr.remo_reg[reg_1_Id].data_p[i] += 4;
        }
    }
}

void init_timer(void) {
    ICR3 = 11058 * 2;
    TCCR3A = 0b00000000;
    TCCR3B = 0b00011001;
    TCNT3 = 0x00;
    ETIMSK |= 1 << OCIE3A;
}
