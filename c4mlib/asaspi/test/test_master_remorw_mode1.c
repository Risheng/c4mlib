/**
 * @file test_master_remorw_mode1.c
 * @author Deng Xiang-Guan
 * @date 2019.10.03
 * @brief 測試ASA_SPIM_trm mode 1函式。
 * 
 * 需搭配test_slave_remorw_mode1.c，測試SPI Master(主)mode 1純傳輸資料，
 * 由高到低，和Slave端交換資料，注意此部分的RegAdd為control flag，測試方式如下條列表示：
 *   1. 使用ASA_SPIM_trm函式，對Slave暫存器編號3寫入2 byte測試資料，
 *      包含control flag和資料。
 *   2. 更新測試程式，回到流程1。
 */

#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asaspi/src/asaspi_master.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/hal_spi.h"
#include "c4mlib/time/src/hal_time.h"

#define SPI_MODE 1
#define ASAID 4
#define DELAY 10

// If control flag  is four bits and data data is twelve bits. 
#define CF_reg 0x02
#define trm_data 0x87

uint16_t test_trm1 = (CF_reg) | (trm_data<<4);

int main() {
    // Setup
    C4M_STDIO_init();
    printf("Start master mode 1\n");
    SPIM_Inst.init();
    sei();
    while (true) {
        char chk;

        /* Test write remote register SPI mode 2 */
        printf("==== remo one ====\n");
        chk = ASA_SPIM_trm(SPI_MODE, ASAID, CF_reg, sizeof(test_trm1),
                           &test_trm1, DELAY);
        printf("chk===%d\n", chk);

        // 12 bits data is overflow
        if((test_trm1&0xFFF0) == 0xFFF0) {
            test_trm1 = CF_reg;
        }
        else {
            test_trm1 += 1<<4;
        }
        _delay_ms(3000);
    }
}
