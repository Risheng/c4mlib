/**
 * @file rtupcount.c
 * @author Yi-Mou
 * @brief reatimeupcount定義
 * @date 2019-08-26
 * 
 * 只做觸發計數
 */

#ifndef C4MLIB_RTPIO_RTUPCOUNT_H
#define C4MLIB_RTPIO_RTUPCOUNT_H

#include <stdint.h>

/**
 * @defgroup rtupcount_struct
 * @defgroup rtupcount_func
 */

/* Public Section Start */
/**
 * @brief RealTimeUpCount結構原型
 * 
 * @ingroup rtupcount_struct
 * @param Fb_Id 中斷中功能方塊名單編號。
 * @param TrigCount 觸發次數計數值。
 * 
 * 計數值紀錄執行step函式次數，用以追蹤次數。
 */
typedef struct {
    uint8_t Fb_Id;              ///<中斷中功能方塊名單編號。
    volatile uint8_t TrigCount; ///<觸發次數計數值。
} RealTimeUpCountStr_t;

/**
 * @brief 初始化結構鏈結
 * 
 * @ingroup rtupcount_func
 * @param RealTimeUpCountStr_p  RealTimeUpCountStr結構指標
 * @return uint8_t 錯誤代碼<br>
 *     0: 正常執行
 */
uint8_t RealTimeUpCount_net(RealTimeUpCountStr_t* RealTimeUpCountStr_p);

/**
 * @brief 將觸發計數+1。
 *
 * @ingroup rtupcount_func
 * @param RealTimeUpCountStr_p 要執行的結構指標。
 *
 * 執行觸發次數值+1，可登錄在中斷服務常式中執行。
 */
void RealTimeUpCount_step(RealTimeUpCountStr_t* RealTimeUpCountStr_p);
/* Public Section End */

#endif  // C4MLIB_RTPIO_RTUPCOUNT_H
