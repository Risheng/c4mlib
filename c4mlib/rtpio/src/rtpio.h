/**
 * @file rtpio.h
 * @author Yi-Mou
 * @brief RealTimePort函式原型
 * @date 2019-08-16
 *
 *
 */

#ifndef C4MLIB_RTPIO_RTPIO_H
#define C4MLIB_RTPIO_RTPIO_H

#include <stdint.h>
/* Public Section Start */

/**
 * @defgroup rtpio_macro
 * @defgroup rtpio_func
 * @defgroup rtpio_struct
 */

/* Public Section Start */
#define RTPIO_MAX_DATA_LENGTH \
    2  ///<硬體暫存器位元最大大小  @ingroup rtpio_macro

/**
 * @brief RealTimePort結構原型
 *
 * @ingroup rtpio_struct
 *
 * 存取硬體暫存器位址和其大小後續讀寫用。
 * 資料緩衝暫存區則供資料存放，當執行step函式時將資料讀寫暫存區。
 * 計數值則紀錄執行step函式次數，用以追蹤次數。
 */
typedef struct {
    volatile uint8_t* Reg_p;  ///< 存放硬體暫存器指標。
    uint8_t Bytes;            ///< 硬體暫存器大小 (最多2 Bytes) 。
    uint8_t Fb_Id;            ///< 中斷中功能方塊名單編號。
    volatile uint8_t Buff[RTPIO_MAX_DATA_LENGTH];  ///< 資料暫存緩衝區
    volatile uint8_t TrigCount;                    ///< 觸發次數計數值
} RealTimePortStr_t;

/**
 * @brief RealTimeFlag結構原型
 *
 * @ingroup rtpio_struct
 *
 * 存取硬體暫存器位址和其大小後續讀寫用。
 * 資料緩衝暫存區則供資料存放，當執行step函式時將旗標資料讀寫暫存區。
 * 計數值則紀錄執行step函式次數，用以追蹤次數。
 */
typedef struct {
    volatile uint8_t* Reg_p;      ///< 存放硬體暫存器指標。
    uint8_t Fb_Id;                ///< 中斷中功能方塊名單編號。
    uint8_t Mask;                 ///< 旗標讀寫遮罩。
    uint8_t Shift;                ///< 旗標讀寫平移位元。
    volatile uint8_t FlagsValue;  ///< 資料暫存緩衝區。
    volatile uint8_t TrigCount;   ///< 觸發次數計數值。
} RealTimeFlagStr_t;

/**
 * @brief 鏈結結構實體與硬體暫存器
 *
 * @ingroup rtpio_func
 * @param Str_p RealTimePort結構指標。
 * @param Reg_p 硬體暫存器指標。
 * @param Bytes 暫存器位元大小(最多2bytes)。
 * @return uint8_t 錯誤代碼<br>
 *          0: 正常執行。<br>
 *          1: Byte錯誤(不是1~2)。
 *
 * 輸出入埠暫存器鏈結驅動函式會執行實體輸出入埠暫存指標鏈結到資料結構的工作，以備中斷時讀寫。
 */
uint8_t RealTimePort_net(RealTimePortStr_t* Str_p, uint8_t* Reg_p,
                         uint8_t Bytes);

/**
 * @brief 執行一次暫存器讀取，並將觸發計數+1。
 *
 * @ingroup rtpio_func
 * @param RealTimePortStr_p 要執行的結構指標。
 *
 * 執行硬體暫存器輸入埠之讀取並轉存至資料結構內的資料暫存區，並將觸發次數值加1。配合其資料結構實體，可登錄在中斷服務常式中執行。
 */
void RealTimePortIn_step(RealTimePortStr_t* RealTimePortStr_p);

/**
 * @brief 執行一次暫存器寫入，並將觸發計數+1。
 *
 * @ingroup rtpio_func
 * @param RealTimePortStr_p 要執行的結構指標。
 *
 * 執行資料結構內的資料暫存區之讀取並轉存至硬體暫存器輸出埠，並將觸發次數值加1。配合其資料結構實體，可登錄在中斷服務常式中執行。
 */
void RealTimePortOut_step(RealTimePortStr_t* RealTimePortStr_p);

/**
 * @brief 鏈結結構實體與硬體暫存器
 *
 * @ingroup rtpio_func
 * @param Str_p RealTimePort結構指標。
 * @param Reg_p 硬體暫存器指標。
 * @param Mask  旗標資料讀寫遮罩(0x00~0xFF)。
 * @param Shift 旗標資料讀寫最低位元(0x00~0x08)。
 * @return uint8_t 錯誤代碼<br>
 *          0: 正常執行。<br>
 *
 * 旗標輸出入埠暫存器鏈結驅動函式會執行硬體輸出入埠暫存指標鏈結到資料結構的工作，以備中斷時讀寫。
 */
uint8_t RealTimeFlag_net(RealTimeFlagStr_t* Str_p, uint8_t* Reg_p, uint8_t Mask,
                         uint8_t Shift);

/**
 * @brief 執行一次旗標讀取，並將觸發計數+1。
 *
 * @ingroup rtpio_func
 * @param RealTimeFlagStr_p 要執行的結構指標。
 *
 * 執行硬體暫存器輸入埠之讀取並轉存至資料結構內的資料暫存區，並將觸發次數值加1。配合其資料結構實體，可登錄在中斷服務常式中執行。
 */
void RealTimeFlagIn_step(RealTimeFlagStr_t* RealTimeFlagStr_p);

/**
 * @brief 執行一次旗標寫入，並將觸發計數+1。
 *
 * @ingroup rtpio_func
 * @param RealTimeFlagStr_p 要執行的結構指標。
 *
 * 執行資料結構內的資料暫存區之讀取並轉存至硬體暫存器輸出埠，
 * 並將觸發次數值加1。配合其資料結構實體，可登錄在中斷服務常式中執行。
 */
void RealTimeFlagOut_step(RealTimeFlagStr_t* RealTimeFlagStr_p);
/* Public Section End */

#endif  // C4MLIB_RTPIO_RTPIO_H
