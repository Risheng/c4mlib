/**
 * @file test_extint.c
 * @author Deng Xiang-Guan
 * @date 2019.08.08
 * @brief 提供ASA函式庫標準中斷介面測試程式，請測試者測試EXT4~EXT7的上緣觸發腳為正常。
 */
#define USE_C4MLIB_DEBUG
#define F_CPU 11059200UL
#include <stddef.h>
#include <stdint.h>

#include <avr/interrupt.h>
#include <avr/io.h>

#include "c4mlib/device/src/device.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/hardware2/src/extint.h"

ExtIntStr_t ExtInt4 = EXTINT_4_STR_INI;
ExtIntStr_t ExtInt5 = EXTINT_5_STR_INI;
ExtIntStr_t ExtInt6 = EXTINT_6_STR_INI;
ExtIntStr_t ExtInt7 = EXTINT_7_STR_INI;

typedef struct context {
    volatile uint16_t ext_id;
    volatile uint32_t counters;
} UserContext_t;

/**
 * @note EXT0~EXT1 are TWI IO, EXT2~EXT3 are UART1 IO.
 * @External interrupts correspond to IO.
 * EXT4 : PE4
 * EXT5 : PE5
 * EXT6 : PE6
 * EXT7 : PE7
 */

/* Setting External interrupt hardware function start. */
void init_exti();
/* Setting External interrupt hardware function end. */

void USER_FUNCTION(void* pvData_p);

int main() {
    C4M_STDIO_init();
    ASABUS_ID_init();

    /***** Test the  ExtInt component *****/
    printf("======= Test ExtInt component =======\n");

    printf("Setup PORTA[0:7] output\n");
    DDRA = 0xFF;

    /* Netting standart interrupt structure to INTERNAL_ISR. */
    ExtInt_net(&ExtInt4, 4);
    ExtInt_net(&ExtInt5, 5);
    ExtInt_net(&ExtInt6, 6);
    ExtInt_net(&ExtInt7, 7);

    // Define user context.
    UserContext_t EXT4_context = {4, 0};
    UserContext_t EXT5_context = {5, 0};
    UserContext_t EXT6_context = {6, 0};
    UserContext_t EXT7_context = {7, 0};

    /* Register the USER_FUNCTION to EXT interrupt, and ADC_context is the
     * parameters that user feed. */
    uint8_t ext4_fb_id = ExtInt_reg(&ExtInt4, USER_FUNCTION, &EXT4_context);
    ExtInt_en(&ExtInt4, ext4_fb_id, true);
    printf("Added USER_FUNCTION with EXT4_context to ExtInt4 Component\n");
    uint8_t ext5_fb_id = ExtInt_reg(&ExtInt5, USER_FUNCTION, &EXT5_context);
    ExtInt_en(&ExtInt5, ext5_fb_id, true);
    printf("Added USER_FUNCTION with EXT5_context to ExtInt5 Component\n");
    uint8_t ext6_fb_id = ExtInt_reg(&ExtInt6, USER_FUNCTION, &EXT6_context);
    ExtInt_en(&ExtInt6, ext6_fb_id, true);
    printf("Added USER_FUNCTION with ext4_counter to ExtInt6 Component\n");
    uint8_t ext7_fb_id = ExtInt_reg(&ExtInt7, USER_FUNCTION, &EXT7_context);
    ExtInt_en(&ExtInt7, ext7_fb_id, true);
    printf("Added USER_FUNCTION with ext4_counter to ExtInt7 Component\n");


    /* Hardware initialization. */
    printf("Setup external Interrupt INT4-INT7\n");
    init_exti();
    printf("Enable Global Interrupt\n");
    sei();
    DEBUG_INFO("Start main, please check the external interrupt IO work successful.");
    while (1) {
        DEBUG_INFO("cnt4:%lu, cnt5:%lu, cnt6:%lu, cnt7:%lu\n", EXT4_context.counters, EXT5_context.counters, EXT6_context.counters, EXT7_context.counters);
    }
}

// Initialize ext0~ext7
void init_exti() {
    /***** Pin mapping *****/
    // PD0  PD1  PD2  PD3
    // INT0 INT1 INT2 INT3
    // SCL  SDA  RXD1 TXD1
    /***********************/

    /***** Pin mapping *****/
    // PE4  PE5  PE6  PE7
    // INT4 INT5 INT6 INT7
    // OC3B OC3C T3   ICP3
    /***********************/
    // Setup INT7:4 as input pin
    DDRE = 0xF0;

    // Setup INT7:4 using rising edge
    EICRB |= 0xFF;

    // Enable INT7:4
    EIMSK = 0xF0;
}

void USER_FUNCTION(void* pvData_p) {
    UserContext_t* p = pvData_p;
    uint8_t ext_id = p->ext_id;
    DEBUG_INFO("Interrupt => %d\n", ext_id);
    p->counters++;
}
