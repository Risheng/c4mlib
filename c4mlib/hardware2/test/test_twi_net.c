#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware2/src/twi.h"

#include "c4mlib/config/twi.cfg"

int main(void) {
    C4M_DEVICE_set();
    printf("test_twi_net.c Start\n");
    TwiIntStr_t twi = TWI_INIT;

    TwiInt_net(&twi, 0);
    TwiInt_set(&twi);
    
    return 0;
}
