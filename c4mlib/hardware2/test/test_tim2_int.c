/**
 * @file test_tim2_int.c
 * @author Yee-Mo
 * @brief 提供timer interrupt測試程式
 * @date 2019-09-05
 */
#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware2/src/tim.h"
#include "c4mlib/time/src/hal_time.h"

#include "c4mlib/config/tim.cfg"

TimIntStr_t TimInt2 = TIM2_INIT;

void USER_FUNCTION(void* pvData_p);

int main(void) {
    C4M_DEVICE_set();

    TimInt_net(&TimInt2, 2);  // 連結變數 TimInt2 到 timer2
    TimInt_set(&TimInt2);

    uint8_t counter2 = 0;
    uint8_t Timint_id = TimInt_reg(&TimInt2, USER_FUNCTION, &counter2); //登入函式到TimInt

    TimInt_en(&TimInt2, Timint_id, true);

    printf("Enable Global Interrupt\n");
    /** Enable interrupts */
    sei();
    while(1){
        printf("OCR2 = %x\t,TCCR2 = %x\t,TCNT2 = %x\t,TIMSK = %x\t,counter=%d\n",OCR2,TCCR2,TCNT2,TIMSK,counter2);
    }
}

void USER_FUNCTION(void* pvData_p) {
    *(uint8_t*)pvData_p=*(uint8_t*)pvData_p+1;
}
