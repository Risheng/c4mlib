/**
 * @file extint.c
 * @author Ye cheng-Wei
 * @author LiYu87
 * @author Deng Xiang-Guan
 * @date 2019.08.08
 * @brief 提供ASA函式庫標準中斷介面，將原生硬體Interrupt呼叫函式占用，並提供登陸函式介面。
 */
/* Open below comment, it will show the debug printf */
// #define USE_C4MLIB_DEBUG
/**
 * NOTE
 * ExtInt_step is called by hardware interrupt
 * and its code is in hardware/std_isr.
 */

#include "c4mlib/hardware2/src/extint.h"

#include "c4mlib/debug/src/debug.h"
#include "c4mlib/hardware2/src/ext_imp.h"
#include "c4mlib/macro/src/std_res.h"

uint8_t ExtInt_net(ExtIntStr_t* IntStr_p, uint8_t Num) {
    if(IntStr_p != NULL) {
        if (Num < EXT_HW_NUM) {
            ExtIntStrList_p[Num] = IntStr_p;
            DEBUG_INFO("Succeed to net SPI number %d\n", Num);
            return INT_OK;
        }
        else {
            // warning Num is excessive, IntStr_p can not net.
            DEBUG_INFO("Warning Num is excessive, IntStr_p can not net.");
            return INT_ERROR_NUM_EXCEED;
        }
    }
    return INT_ERROR_POINTER_IS_NULL;
}

uint8_t ExtInt_reg(ExtIntStr_t* IntStr_p, Func_t FbFunc_p, void* FbPara_p) {
    if (IntStr_p != NULL) {
        uint8_t new_Id = IntStr_p->IntTotal;
        IntStr_p->IntFb[new_Id].enable = 0;  // Default Disable
        IntStr_p->IntFb[new_Id].func_p = FbFunc_p;
        IntStr_p->IntFb[new_Id].funcPara_p = FbPara_p;
        IntStr_p->IntTotal++;
        return new_Id;
    }
    else {
        DEBUG_INFO("Warning !!! IntStr_p is NULL\n");
        return INT_ERROR_POINTER_IS_NULL;
    }
}

void ExtInt_en(ExtIntStr_t* IntStr_p, uint8_t Fb_Id, uint8_t enable) {
    if (IntStr_p != NULL) {
        if (Fb_Id < IntStr_p->IntTotal) {
            IntStr_p->IntFb[Fb_Id].enable = enable;
        }
    }
    else {
        DEBUG_INFO("Warning !!! IntStr_p is NULL\n");
    }
}
