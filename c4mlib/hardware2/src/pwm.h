/**
 * @file pwm.h
 * @author
 * @date
 * @brief
 *
 */

#ifndef C4MLIB_HARDWARE2_PWM_H
#define C4MLIB_HARDWARE2_PWM_H

#include "c4mlib/interrupt/src/isr_func.h"
#include "c4mlib/macro/src/std_def.h"
#include "c4mlib/macro/src/std_type.h"

#include <stdint.h>

#include "c4mlib/config/interrupt.cfg"

/* Public Section Start */
typedef struct __pwm_set_str {
    uint8_t ClockSource0 ;
    uint8_t ClockSource1 ;
    uint8_t WaveMode0 ;
    uint8_t WaveOut0 ;
    uint8_t WaveOut1 ;
    uint8_t WaveOut2 ;
    uint8_t PWMIntEn ;
    uint8_t TimIntEn ;
    uint8_t WaveoutPin0 ;
    uint8_t WaveoutPin1 ;
    uint8_t WaveoutPin2 ;
    uint16_t PWMMaxWidth ;
    uint16_t PulseWidth0 ;
    uint16_t PulseWidth1 ;
    uint16_t PulseWidth2 ;
} PwmSetStr_t;

typedef struct __pwm_int_str {
    PwmSetStr_t PwmSet;

    uint8_t (*SetFunc_p)(struct __pwm_int_str*);

    uint8_t IntTotal;                                   ///< 紀錄已有多少中斷已註冊
    volatile FuncBlockStr_t IntFb[MAX_PWMINT_FUNCNUM];  ///< 紀錄所有已註冊的中斷函式
} PwmIntStr_t;

uint8_t PwmInt_net(PwmIntStr_t* IntStr_p, uint8_t Num);

uint8_t PwmInt_set(PwmIntStr_t* IntStr_p);

uint8_t PwmInt_reg(PwmIntStr_t* IntStr_p, Func_t FbFunc_p, void* FbPara_p);

void PwmInt_en(PwmIntStr_t* IntStr_p, uint8_t Fb_Id, uint8_t enable);

void PwmInt_step(PwmIntStr_t* IntStr_p);
/* Public Section End */

/* Public Section Start */
/*----- Pwm Sets Macros -----------------------------------------------------*/

#define DISABLE 0
#define ENABLE  1
#define OUTPUT  1
#define INPUT   0

/* ClockSource0 */
#define PWM_CLOCKSOURCE_CLK_IO  0 
#define PWM_CLOCKSOURCE_CLK_OSC 1
/* ClockSource1 */ 
#define PWM0_CLOCKPRESCALEDBY_1         1
#define PWM0_CLOCKPRESCALEDBY_8         2
#define PWM0_CLOCKPRESCALEDBY_32        3
#define PWM0_CLOCKPRESCALEDBY_64        4
#define PWM0_CLOCKPRESCALEDBY_128       5
#define PWM0_CLOCKPRESCALEDBY_256       6
#define PWM0_CLOCKPRESCALEDBY_1024      7
#define PWM123_CLOCKPRESCALEDBY_1       1
#define PWM123_CLOCKPRESCALEDBY_8       2
#define PWM123_CLOCKPRESCALEDBY_64      3
#define PWM123_CLOCKPRESCALEDBY_256     4
#define PWM123_CLOCKPRESCALEDBY_1024    5
/* WaveMode0 */ 
#define WAVEMODE_CENTRAL_ALIGN  8
#define WAVEMODE_EDGE_ALIGN     9
/* Waveout0 */ 
#define WAVEOUT_NPULSE  2
#define WAVEOUT_PPULSE  3
/* Waveout1 */ 
/* Waveout2 */ 
/* PWMIntEn */ // DISABLE, ENABLE */
/* TimIntEn */ // DISABLE, ENABLE */
/* WaveoutPin0 */ // OUTPUT, INPUT */
/* WaveoutPin1 */ // OUTPUT, INPUT */
/* WaveoutPin2 */ // Output, Input */
/* PWMMaxWidth  tim0、tim2 N=255 fixed ; tim1、tim3 N<65536 fixed  */
/* PulseWidth0  0 <= DUTY <= N */
/* PulseWidth1  0 <= DUTY <= N */
/* PulseWidth2  0 <= DUTY <= N */
/*---------------------------------------------------------------------------*/
/* Public Section End */

#endif  // C4MLIB_HARDWARE2_PWM_H
