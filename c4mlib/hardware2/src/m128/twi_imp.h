/**
 * @file twi_imp.h
 * @author
 * @brief 
 * @date 2019.09.04
 * 
 */

#ifndef C4MLIB_INIT_M128_TWI_IMP_H
#define C4MLIB_INIT_M128_TWI_IMP_H

#include "c4mlib/hardware2/src/twi.h"

/* Public Section Start */
#define TWI_HW_NUM 1
extern TwiIntStr_t TwiImp[TWI_HW_NUM];
extern TwiIntStr_t *TwiIntStrList_p[TWI_HW_NUM];
/* Public Section End */

#endif  // C4MLIB_INIT_M128_TWI_IMP_H
