
/**
 * @file tim_imp.c
 * @author
 * @brief
 * @date 2019.09.04
 *
 */

#include "c4mlib/hardware2/src/m128/tim_imp.h"

#include "c4mlib/macro/src/bits_op.h"
#include "c4mlib/macro/src/std_def.h"
#include "c4mlib/macro/src/std_res.h"

#include <avr/io.h>

#include "c4mlib/config/tim.cfg"

static uint8_t tim0_hw_init(TimIntStr_t *str_p);
static uint8_t tim1_hw_init(TimIntStr_t *str_p);
static uint8_t tim2_hw_init(TimIntStr_t *str_p);
static uint8_t tim3_hw_init(TimIntStr_t *str_p);

TimIntStr_t TimImp[TIM_HW_NUM] = {
    {
        .TimSet = TIM0_HW_SET_CFG,
        .SetFunc_p = tim0_hw_init
    },
    {
        .TimSet = TIM1_HW_SET_CFG,
        .SetFunc_p = tim1_hw_init
    },
    {
        .TimSet = TIM2_HW_SET_CFG,
        .SetFunc_p = tim2_hw_init
    },
    {
        .TimSet = TIM3_HW_SET_CFG,
        .SetFunc_p = tim3_hw_init
    }
};

TimIntStr_t *TimIntStrList_p[TIM_HW_NUM];

uint8_t tim0_hw_init(TimIntStr_t *str_p) {
    switch (str_p->TimSet.ClockSource0){
        case TIM_CLOCKSOURCE_CLK_IO:
            ASSR &= ~(1 << AS0);
            ASSR |= (0 << AS0);
            break;
        case TIM_CLOCKSOURCE_CLK_OSC:
            ASSR &= ~(1 << AS0);
            ASSR |= (1 << AS0);
            break;
    }
    switch (str_p->TimSet.ClockSource1){
        case TIM0_CLOCKPRESCALEDBY_1:
            TCCR0 &= ~((1 << CS02) | (1 << CS01)| (1 << CS00));
            TCCR0 |= ((0 << CS02) | (0 << CS01)| (1 << CS00));
            break;
        case TIM0_CLOCKPRESCALEDBY_8:
            TCCR0 &= ~((1 << CS02) | (1 << CS01)| (1 << CS00));
            TCCR0 |= ((0 << CS02) | (1 << CS01)| (0 << CS00));
            break;
        case TIM0_CLOCKPRESCALEDBY_32:
            TCCR0 &= ~((1 << CS02) | (1 << CS01)| (1 << CS00));
            TCCR0 |= ((0 << CS02) | (1 << CS01)| (1 << CS00));
            break;
        case TIM0_CLOCKPRESCALEDBY_64:
            TCCR0 &= ~((1 << CS02) | (1 << CS01)| (1 << CS00));
            TCCR0 |= ((1 << CS02) | (0 << CS01)| (0 << CS00));
            break;
        case TIM0_CLOCKPRESCALEDBY_128:
            TCCR0 &= ~((1 << CS02) | (1 << CS01)| (1 << CS00));
            TCCR0 |= ((1 << CS02) | (0 << CS01)| (1  << CS00));
            break;
        case TIM0_CLOCKPRESCALEDBY_256:
            TCCR0 &= ~((1 << CS02) | (1 << CS01)| (1 << CS00));
            TCCR0 |= ((1 << CS02) | (1 << CS01)| (0 << CS00));
            break;
        case TIM0_CLOCKPRESCALEDBY_1024:
            TCCR0 &= ~((1 << CS02) | (1 << CS01)| (1 << CS00));
            TCCR0 |= ((1 << CS02) | (1 << CS01)| (1 << CS00));
            break;
    }
    switch (str_p->TimSet.WaveMode0) {
        case WAVEMODE_SQUARE_FIXED:
            TCCR0 &= ~((1 << WGM01) | (1 << WGM00));
            TCCR0 |= ((0 << WGM01) | (0 << WGM00));
            break;
        case WAVEMODE_SQUARE_ADJUSTABLE:
            TCCR0 &= ~((1 << WGM01) | (1 << WGM00));
            TCCR0 |= ((1 << WGM01) | (0 << WGM00));
            break;
    }
    switch (str_p->TimSet.Waveout0){
        case DISABLE:
            TCCR0 &= ~((1 << COM01) | (1 << COM00));
            TCCR0 |= ((0 << COM01) | (0 << COM00));
            break;
        case ENABLE:
            TCCR0 &= ~((1 << COM01) | (1 << COM00));
            TCCR0 |= ((0 << COM01) | (1 << COM00));
            break;
    }
    switch (str_p->TimSet.TimIntEn){
        case DISABLE:
            // 禁能
            TIMSK &= ~(1 << OCIE0);
            TIMSK |= (0 << OCIE0);
            break;
        case ENABLE:
            // 致能
            TIMSK &= ~(1 << OCIE0);
            TIMSK |= (1 << OCIE0);
            break;
    }
    switch (str_p->TimSet.WaveoutPin0) {
        case INPUT:
            DDRB &= ~(1 << PB4);
            DDRB |= (0 << PB4);
            break;
        case OUTPUT:
            DDRB &= ~(1 << PB4);
            DDRB |= (1 << PB4);
            break;
    }
    
    OCR0 = str_p->TimSet.TimerPeriod;

    TCNT0 = 0;
    return 0 ;
}

uint8_t tim1_hw_init(TimIntStr_t *str_p) {

    OCR1A = str_p->TimSet.TimerPeriod;
    OCR1B = str_p->TimSet.TimerPeriod;
    OCR1C = str_p->TimSet.TimerPeriod;
    
    switch (str_p->TimSet.ClockSource1){
        case TIM123_CLOCKPRESCALEDBY_1:
            TCCR1B &= ~((1 << CS12) | (1 << CS11)| (1 << CS10));
            TCCR1B |= ((0 << CS12) | (0 << CS11)| (1 << CS10));
            break;
        case TIM123_CLOCKPRESCALEDBY_8:
            TCCR1B &= ~((1 << CS12) | (1 << CS11)| (1 << CS10));
            TCCR1B |= ((0 << CS12) | (1 << CS11)| (0 << CS10));
            break;
        case TIM123_CLOCKPRESCALEDBY_64:
            TCCR1B &= ~((1 << CS12) | (1 << CS11)| (1 << CS10));
            TCCR1B |= ((0 << CS12) | (1 << CS11)| (1 << CS10));
            break;
        case TIM123_CLOCKPRESCALEDBY_256:
            TCCR1B &= ~((1 << CS12) | (1 << CS11)| (1 << CS10));
            TCCR1B |= ((1 << CS12) | (0 << CS11)| (0 << CS10));
            break;
        case TIM123_CLOCKPRESCALEDBY_1024:
            TCCR1B &= ~((1 << CS12) | (1 << CS11)| (1 << CS10));
            TCCR1B |= ((1 << CS12) | (0 << CS11)| (1 << CS10));
            break;
    }
    switch (str_p->TimSet.WaveMode0) {
        case WAVEMODE_SQUARE_FIXED:
            TCCR1A &= ~((1 << WGM11) | (1 << WGM10));
            TCCR1A |= ((0 << WGM11) | (0 << WGM10));
            TCCR1B &= ~((1 << WGM11) | (1 << WGM10));
            TCCR1B |= ((0 << WGM13) | (0 << WGM12));
            break;
        case WAVEMODE_SQUARE_ADJUSTABLE:
            TCCR1A &= ~((1 << WGM11) | (1 << WGM10));
            TCCR1A |= ((0 << WGM11) | (0 << WGM10));
            TCCR1B &= ~((1 << WGM11) | (1 << WGM10));
            TCCR1B |= ((0 << WGM13) | (1 << WGM12));
            break;
    }
    switch(str_p->TimSet.Waveout0){
        case DISABLE:
            TCCR1A &= ~((1 << COM1A1) | (1 << COM1A0));
            TCCR1A |= ((0 << COM1A1) | (0 << COM1A0));
            break;
        case ENABLE:
            TCCR1A &= ~((1 << COM1A1) | (1 << COM1A0));
            TCCR1A |= ((0 << COM1A1) | (1 << COM1A0));
            break;
    }
    switch(str_p->TimSet.Waveout1){
        case DISABLE:
            TCCR1A &= ~((1 << COM1B1) | (1 << COM1B0));
            TCCR1A |= ((0 << COM1B1) | (0 << COM1B0));
            break;
        case ENABLE:
            TCCR1A &= ~((1 << COM1B1) | (1 << COM1B0));
            TCCR1A |= ((0 << COM1B1) | (1 << COM1B0));
            break;
    }
    switch(str_p->TimSet.Waveout2){
        case DISABLE:
            TCCR1A &= ~((1 << COM1C1) | (1 << COM1C0));
            TCCR1A |= ((0 << COM1C1) | (0 << COM1C0));
            break;
        case ENABLE:
            TCCR1A &= ~((1 << COM1C1) | (1 << COM1C0));
            TCCR1A |= ((0 << COM1C1) | (1 << COM1C0));
            break;
    }
    switch (str_p->TimSet.TimIntEn){
        case DISABLE:
            // 禁能
            TIMSK &= ~(1 << OCIE1A);
            TIMSK |= (0 << OCIE1A);
            break;
        case ENABLE:
            // 致能
            TIMSK &= ~(1 << OCIE1A);
            TIMSK |= (1 << OCIE1A);
            break;
    }
    
    switch (str_p->TimSet.WaveoutPin0) {
        case INPUT:
            DDRB &= ~(1 << PB5);
            DDRB |= (0 << PB5);
            break;
        case OUTPUT:
            DDRB &= ~(1 << PB5);
            DDRB |= (1 << PB5);
            break;
    }
    
    switch (str_p->TimSet.WaveoutPin1) {
        case INPUT:
            DDRB &= ~(1 << PB6);
            DDRB |= (0 << PB6);
            break;
        case OUTPUT:
            DDRB &= ~(1 << PB6);
            DDRB |= (1 << PB6);
            break;
    }
    
    switch (str_p->TimSet.WaveoutPin2) {
        case INPUT:
            DDRB &= ~(1 << PB7);
            DDRB |= (0 << PB7);
            break;
        case OUTPUT:
            DDRB &= ~(1 << PB7);
            DDRB |= (1 << PB7);
            break;
    }
    TCNT1 = 0;

    return 0 ;
}

uint8_t tim3_hw_init(TimIntStr_t *str_p) {

    OCR3A = str_p->TimSet.TimerPeriod;
    OCR3B = str_p->TimSet.TimerPeriod;
    OCR3C = str_p->TimSet.TimerPeriod;
    switch (str_p->TimSet.ClockSource1){
        case TIM123_CLOCKPRESCALEDBY_1:
            TCCR3B &= ~((1 << CS32) | (1 << CS31)| (1 << CS30));
            TCCR3B |= ((0 << CS32) | (0 << CS31)| (1 << CS30));
            break;
        case TIM123_CLOCKPRESCALEDBY_8:
            TCCR3B &= ~((1 << CS32) | (1 << CS31)| (1 << CS30));
            TCCR3B |= ((0 << CS32) | (1 << CS31)| (0 << CS30));
            break;
        case TIM123_CLOCKPRESCALEDBY_64:
            TCCR3B &= ~((1 << CS32) | (1 << CS31)| (1 << CS30));
            TCCR3B |= ((0 << CS32) | (1 << CS31)| (1 << CS30));
            break;
        case TIM123_CLOCKPRESCALEDBY_256:
            TCCR3B &= ~((1 << CS32) | (1 << CS31)| (1 << CS30));
            TCCR3B |= ((1 << CS32) | (0 << CS31)| (0 << CS30));
            break;
        case TIM123_CLOCKPRESCALEDBY_1024:
            TCCR3B &= ~((1 << CS32) | (1 << CS31)| (1 << CS30));
            TCCR3B |= ((1 << CS32) | (0 << CS31)| (1 << CS30));
            break;
    }
    switch (str_p->TimSet.WaveMode0) {
        case WAVEMODE_SQUARE_FIXED:
            TCCR3A &= ~((1 << WGM31) | (1 << WGM30));
            TCCR3A |= ((0 << WGM31) | (0 << WGM30));
            TCCR3B &= ~((1 << WGM31) | (1 << WGM30));
            TCCR3B |= ((0 << WGM33) | (0 << WGM32));
            break;
        case WAVEMODE_SQUARE_ADJUSTABLE:
            TCCR3A &= ~((1 << WGM31) | (1 << WGM30));
            TCCR3A |= ((0 << WGM31) | (0 << WGM30));
            TCCR3B &= ~((1 << WGM31) | (1 << WGM30));
            TCCR3B |= ((0 << WGM33) | (1 << WGM32));
            break;
    }
    
    switch(str_p->TimSet.Waveout0){
        case DISABLE:
            TCCR3A &= ~((1 << COM3A1) | (1 << COM3A0));
            TCCR3A |= ((0 << COM3A1) | (0 << COM3A0));
            break;
        case ENABLE:
            TCCR3A &= ~((1 << COM3A1) | (1 << COM3A0));
            TCCR3A |= ((0 << COM3A1) | (1 << COM3A0));
            break;
    }
    switch(str_p->TimSet.Waveout1){
        case DISABLE:
            TCCR3A &= ~((1 << COM3B1) | (1 << COM3B0));
            TCCR3A |= ((0 << COM3B1) | (0 << COM3B0));
            break;
        case ENABLE:
            TCCR3A &= ~((1 << COM3B1) | (1 << COM3B0));
            TCCR3A |= ((0 << COM3B1) | (1 << COM3B0));
            break;
    }
    switch(str_p->TimSet.Waveout2){
        case DISABLE:
            TCCR3A &= ~((1 << COM3C1) | (1 << COM3C0));
            TCCR3A |= ((0 << COM3C1) | (0 << COM3C0));
            break;
        case ENABLE:
            TCCR3A &= ~((1 << COM3C1) | (1 << COM3C0));
            TCCR3A |= ((0 << COM3C1) | (1 << COM3C0));
            break;
    }
    switch (str_p->TimSet.TimIntEn){
        case DISABLE:
            // 禁能
            ETIMSK &= ~(1 << OCIE3A);
            ETIMSK |= (0 << OCIE3A);
            break;
        case ENABLE:
            // 致能
            ETIMSK &= ~(1 << OCIE3A);
            ETIMSK |= (1 << OCIE3A);
            break;
    }
    
    switch (str_p->TimSet.WaveoutPin0) {
        case INPUT:
            DDRE &= ~(1 << PE3);
            DDRE |= (0 << PE3);
            break;
        case OUTPUT:
            DDRE &= ~(1 << PE3);
            DDRE |= (1 << PE3);
            break;
    }
    
    switch (str_p->TimSet.WaveoutPin1) {
        case INPUT:
            DDRE &= ~(1 << PE4);
            DDRE |= (0 << PE4);
            break;
        case OUTPUT:
            DDRE &= ~(1 << PE4);
            DDRE |= (1 << PE4);
            break;
    }
    
    switch (str_p->TimSet.WaveoutPin2) {
        case INPUT:
            DDRE &= ~(1 << PE5);
            DDRE |= (0 << PE5);
            break;
        case OUTPUT:
            DDRE &= ~(1 << PE5);
            DDRE |= (1 << PE5);
            break;
    }
    TCNT3 = 0;

    return 0 ;
}


uint8_t tim2_hw_init(TimIntStr_t *str_p) {

    OCR2 = str_p->TimSet.TimerPeriod;
    
    switch(str_p->TimSet.ClockSource1){
        case TIM123_CLOCKPRESCALEDBY_1:
            TCCR2 &= ~((1 << CS22) | (1 << CS21)| (1 << CS20));
            TCCR2 |= ((0 << CS22) | (0 << CS21)| (1 << CS20));
            break;
        case TIM123_CLOCKPRESCALEDBY_8:
            TCCR2 &= ~((1 << CS22) | (1 << CS21)| (1 << CS20));
            TCCR2 |= ((0 << CS22) | (1 << CS21)| (0 << CS20));
            break;
        case TIM123_CLOCKPRESCALEDBY_64:
            TCCR2 &= ~((1 << CS22) | (1 << CS21)| (1 << CS20));
            TCCR2 |= ((0 << CS22) | (1 << CS21)| (1 << CS20));
            break;
        case TIM123_CLOCKPRESCALEDBY_256:
            TCCR2 &= ~((1 << CS22) | (1 << CS21)| (1 << CS20));
            TCCR2 |= ((1 << CS22) | (0 << CS21)| (0 << CS20));
            break;
        case TIM123_CLOCKPRESCALEDBY_1024:
            TCCR2 &= ~((1 << CS22) | (1 << CS21)| (1 << CS20));
            TCCR2 |= ((1 << CS22) | (0 << CS21)| (1 << CS20));
            break;
    }
    switch(str_p->TimSet.WaveMode0){
        case WAVEMODE_SQUARE_FIXED:
            TCCR2 &= ~((1 << WGM21) | (1 << WGM20));
            TCCR2 |= ((0 << WGM21) | (0 << WGM20));
            break;
        case WAVEMODE_SQUARE_ADJUSTABLE:
            TCCR2 &= ~((1 << WGM21) | (1 << WGM20));
            TCCR2 |= ((1 << WGM21) | (0 << WGM20));
            break;
    }
    
    switch (str_p->TimSet.Waveout0){
        case DISABLE:
            TCCR2 &= ~((1 << COM21) | (1 << COM20));
            TCCR2 |= ((0 << COM21) | (0 << COM20));
            break;
        case ENABLE:
            TCCR2 &= ~((1 << COM21) | (1 << COM20));
            TCCR2 |= ((0 << COM21) | (1 << COM20));
            break;
    }
    switch(str_p->TimSet.TimIntEn){
        case DISABLE:
            // 禁能
            TIMSK &= ~(1 << OCIE2);
            TIMSK |= (0 << OCIE2);
            break;
        case ENABLE:
            // 致能
            TIMSK &= ~(1 << OCIE2);
            TIMSK |= (1 << OCIE2);
            break;
    }
    switch (str_p->TimSet.WaveoutPin0) {
        case INPUT:
            DDRB &= ~(1 << PB7);
            DDRB |= (0 << PB7);
            break;
        case OUTPUT:
            DDRB &= ~(1 << PB7);
            DDRB |= (1 << PB7);
            break;
    }
    TCNT2 = 0;

    return 0 ;
}

