/**
 * @file uart_imp.c
 * @author 
 * @brief 
 * @date 2019.09.04
 * 
 */

#include "c4mlib/hardware2/src/m128/uart_imp.h"

#include "c4mlib/macro/src/bits_op.h"
#include "c4mlib/macro/src/std_def.h"
#include "c4mlib/macro/src/std_res.h"

#include <avr/io.h>

#include "c4mlib/config/uart.cfg"

static uint8_t uart0_hw_init(UartIntStr_t *IntStr_p);
static uint8_t uart1_hw_init(UartIntStr_t *IntStr_p);

UartIntStr_t UartImp[UART_HW_NUM] = {
    {
        .UartSet = UART_HW_SET_CFG,
        .SetFunc_p = uart0_hw_init
    },
    {
        .UartSet = UART_HW_SET_CFG,
        .SetFunc_p = uart1_hw_init
    }
};

UartIntStr_t *UartTxIntStrList_p[UART_HW_NUM];
UartIntStr_t *UartRxIntStrList_p[UART_HW_NUM];

uint8_t uart0_hw_init(UartIntStr_t *IntStr_p) {

    uint16_t n = F_CPU / 16 / IntStr_p->UartSet.BitRateRx - 1;
    UBRR0H = (uint8_t)(n >> 8);
    UBRR0L = (uint8_t)n;

    switch (IntStr_p->UartSet.Parity) {
        case UART_PARITY_NONE:
            UCSR0C &= ~((1 << UPM00) | (1 << UPM01));
            UCSR0C |= ((0 << UPM00) | (0 << UPM01));
            break;
        case UART_PARITY_EVEN:
            UCSR0C &= ~((1 << UPM00) | (1 << UPM01));
            UCSR0C |= ((0 << UPM00) | (1 << UPM01));
            break;
        case UART_PARITY_ODD:
            UCSR0C &= ~((1 << UPM00) | (1 << UPM01));
            UCSR0C |= ((1 << UPM00) | (1 << UPM01));
            break;
    }

    switch (IntStr_p->UartSet.StopBits) {
        case UART_STOPBITS_1:
            UCSR0C &= ~(1 << USBS0);
            UCSR0C |= (0 << USBS0);
            break;
        case UART_STOPBITS_2:
            UCSR0C &= ~(1 << USBS0);
            UCSR0C |= (1 << USBS0);
            break;
    }

    switch (IntStr_p->UartSet.WordBits) {
        case UART_WORDBITS_5:
            UCSR0B &= ~(1 << UCSZ02);
            UCSR0B |= (0 << UCSZ02);
            UCSR0C &= ~((1 << UCSZ00) | (1 << UCSZ01));
            UCSR0C |= ((0 << UCSZ00) | (0 << UCSZ01));
            break;
        case UART_WORDBITS_6:
            UCSR0B &= ~(1 << UCSZ02);
            UCSR0B |= (0 << UCSZ02);
            UCSR0C &= ~((1 << UCSZ00) | (1 << UCSZ01));
            UCSR0C |= ((0 << UCSZ00) | (1 << UCSZ01));
            break;
        case UART_WORDBITS_7:
            UCSR0B &= ~(1 << UCSZ02);
            UCSR0B |= (0 << UCSZ02);
            UCSR0C &= ~((1 << UCSZ00) | (1 << UCSZ01));
            UCSR0C |= ((1 << UCSZ00) | (0 << UCSZ01));
            break;
        case UART_WORDBITS_8:
            UCSR0B &= ~(1 << UCSZ02);
            UCSR0B |= (0 << UCSZ02);
            UCSR0C &= ~((1 << UCSZ00) & (1 << UCSZ01));
            UCSR0C |= ((1 << UCSZ00) | (1 << UCSZ01));
            break;
        
        // case UART_WORDBITS_9: // 老師將bit0 用Bit8En去控制
        //     UCSR0B &= ~(1 << UCSZ02);
        //     UCSR0B |= (1 << UCSZ02);
        //     UCSR0C &= ~((1 << UCSZ00) & (1 << UCSZ01));
        //     UCSR0C |= ((1 << UCSZ00) | (1 << UCSZ01));
        //     break;
    }

    switch (IntStr_p->UartSet.Bit8En){
        case ENABLE:
            UCSR0B &= ~(1 << UCSZ02);
            UCSR0B |= (1 << UCSZ02);
            break;
        case DISABLE:
            UCSR0B &= ~(1 << UCSZ02);
            UCSR0B |= (0 << UCSZ02);
            break;
    }
    switch (IntStr_p->UartSet.TxEn){
        case ENABLE:
            UCSR0B &= ~(1 << TXEN0);
            UCSR0B |= (1 << TXEN0);
            break;
        case DISABLE:
            UCSR0B &= ~(1 << TXEN0);
            UCSR0B |= (0 << TXEN0);
            break;
    }
    switch (IntStr_p->UartSet.RxEn){
        case ENABLE:
            UCSR0B &= ~(1 << RXEN0);
            UCSR0B |= (1 << RXEN0);
            break;
        case DISABLE:
            UCSR0B &= ~(1 << RXEN0);
            UCSR0B |= (0 << RXEN0);
            break;
    }
    switch (IntStr_p->UartSet.TxIntEn){
        case ENABLE:
            UCSR0B &= ~(1 << TXCIE0);
            UCSR0B |= (1 << TXCIE0);
            break;
        case DISABLE:
            UCSR0B &= ~(1 << TXCIE0);
            UCSR0B |= (1 << TXCIE0);
            break;
    }
    switch (IntStr_p->UartSet.RxIntEn){
        case ENABLE:
            UCSR0B &= ~(1 << RXCIE0);
            UCSR0B |= (1 << RXCIE0);
            break;
        case DISABLE:
            UCSR0B &= ~(1 << RXCIE0);
            UCSR0B |= (0 << RXCIE0);
            break;
    }
    switch (IntStr_p->UartSet.MultyMCU){
        case ENABLE:
            UCSR0A &= ~(1 << MPCM0);
            UCSR0A |= (1 << MPCM0);
            break;
        case DISABLE:
            UCSR0A &= ~(1 << MPCM0);
            UCSR0A |= (0 << MPCM0);
            break;
    }
    switch (IntStr_p->UartSet.DoubleBitRate){
        case ENABLE:
            UCSR0A &= ~(1 << U2X0);
            UCSR0A |= (1 << U2X0);
            break;
        case DISABLE:
            UCSR0A &= ~(1 << U2X0);
            UCSR0A |= (0 << U2X0);
            break;
    }
    return 0 ;
}

uint8_t uart1_hw_init(UartIntStr_t *IntStr_p) {
    uint16_t n = F_CPU / 16 / IntStr_p->UartSet.BitRateRx - 1;
    UBRR1H = (uint8_t)(n >> 8);
    UBRR1L = (uint8_t)n;

    switch (IntStr_p->UartSet.Parity) {
        case UART_PARITY_NONE:
            UCSR1C &= ~((1 << UPM10) | (1 << UPM11));
            UCSR1C |= ((0 << UPM10) | (0 << UPM11));
            break;
        case UART_PARITY_EVEN:
            UCSR1C &= ~((1 << UPM10) | (1 << UPM11));
            UCSR1C |= ((0 << UPM10) | (1 << UPM11));
            break;
        case UART_PARITY_ODD:
            UCSR1C &= ~((1 << UPM10) | (1 << UPM11));
            UCSR1C |= ((1 << UPM10) | (1 << UPM11));
            break;
    }

    switch (IntStr_p->UartSet.StopBits) {
        case UART_STOPBITS_1:
            UCSR1C &= ~(1 << USBS1);
            UCSR1C |= (0 << USBS1);
            break;
        case UART_STOPBITS_2:
            UCSR1C &= ~(1 << USBS1);
            UCSR1C |= (1 << USBS1);
            break;
    }

    switch (IntStr_p->UartSet.WordBits) {
        case UART_WORDBITS_5:
            UCSR1B &= ~(1 << UCSZ12);
            UCSR1B |= (0 << UCSZ12);
            UCSR1C &= ~((1 << UCSZ10) | (1 << UCSZ11));
            UCSR1C |= ((0 << UCSZ10) | (0 << UCSZ11));
            break;
        case UART_WORDBITS_6:
            UCSR1B &= ~(1 << UCSZ12);
            UCSR1B |= (0 << UCSZ12);
            UCSR1C &= ~((1 << UCSZ10) | (1 << UCSZ11));
            UCSR1C |= ((0 << UCSZ10) | (1 << UCSZ11));
            break;
        case UART_WORDBITS_7:
            UCSR1B &= ~(1 << UCSZ12);
            UCSR1B |= (0 << UCSZ12);
            UCSR1C &= ~((1 << UCSZ10) | (1 << UCSZ11));
            UCSR1C |= ((1 << UCSZ10) | (0 << UCSZ11));
            break;
        case UART_WORDBITS_8:
            UCSR1B &= ~(1 << UCSZ12);
            UCSR1B |= (0 << UCSZ12);
            UCSR1C &= ~((1 << UCSZ10) | (1 << UCSZ11));
            UCSR1C |= ((1 << UCSZ10) | (1 << UCSZ11));
            break;
        
        // case UART_WORDBITS_9: // 老師將bit0 用Bit8En去控制
        //     UCSR1B &= ~(1 << UCSZ12);
        //     UCSR1B |= (1 << UCSZ12);
        //     UCSR1C &= ~((1 << UCSZ10) | (1 << UCSZ11));
        //     UCSR1C |= ((1 << UCSZ10) | (1 << UCSZ11));
        //     break;
    }

    switch (IntStr_p->UartSet.Bit8En){
        case ENABLE:
            UCSR1B &= ~(1 << UCSZ12);
            UCSR1B |= (1 << UCSZ12);
            break;
        case DISABLE:
            UCSR1B &= ~(1 << UCSZ12);
            UCSR1B |= (0 << UCSZ12);
            break;
    }
    switch (IntStr_p->UartSet.TxEn){
        case ENABLE:
            UCSR1B &= ~(1 << TXEN1);
            UCSR1B |= (1 << TXEN1);
            break;
        case DISABLE:
            UCSR1B &= ~(1 << TXEN1);
            UCSR1B |= (0 << TXEN1);
            break;
    }
    switch (IntStr_p->UartSet.RxEn){
        case ENABLE:
            UCSR1B &= ~(1 << RXEN1);
            UCSR1B |= (1 << RXEN1);
            break;
        case DISABLE:
            UCSR1B &= ~(1 << RXEN1);
            UCSR1B |= (0 << RXEN1);
            break;
    }
    switch (IntStr_p->UartSet.TxIntEn){
        case ENABLE:
            UCSR1B &= ~(1 << TXCIE1);
            UCSR1B |= (1 << TXCIE1);
            break;
        case DISABLE:
            UCSR1B &= ~(1 << TXCIE1);
            UCSR1B |= (1 << TXCIE1);
            break;
    }
    switch (IntStr_p->UartSet.RxIntEn){
        case ENABLE:
            UCSR1B &= ~(1 << RXCIE1);
            UCSR1B |= (1 << RXCIE1);
            break;
        case DISABLE:
            UCSR1B &= ~(1 << RXCIE1);
            UCSR1B |= (0 << RXCIE1);
            break;
    }
    switch (IntStr_p->UartSet.MultyMCU){
        case ENABLE:
            UCSR1A &= ~(1 << MPCM1);
            UCSR1A |= (1 << MPCM1);
            break;
        case DISABLE:
            UCSR1A &= ~(1 << MPCM1);
            UCSR1A |= (0 << MPCM1);
            break;
    }
    switch (IntStr_p->UartSet.DoubleBitRate){
        case ENABLE:
            UCSR1A &= ~(1 << U2X1);
            UCSR1A |= (1 << U2X1);
            break;
        case DISABLE:
            UCSR1A &= ~(1 << U2X1);
            UCSR1A |= (0 << U2X1);
            break;
    }
    return 0;
}
