/**
 * @file ext_imp.h
 * @author LiYu87
 * @date 2019.09.04
 * @brief 
 * 
 */

#ifndef C4MLIB_HARDWARE2_M128_EXT_IMP_H
#define C4MLIB_HARDWARE2_M128_EXT_IMP_H

#include "c4mlib/hardware2/src/extint.h"

/* Public Section Start */
#define EXT_HW_NUM 8
extern ExtIntStr_t *ExtIntStrList_p[EXT_HW_NUM];
/* Public Section End */

#endif  // C4MLIB_HARDWARE2_M128_EXT_IMP_H
