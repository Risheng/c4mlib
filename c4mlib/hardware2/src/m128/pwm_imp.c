/**
 * @file pwm_imp.c
 * @author maxwu84
 * @brief
 * @date 2019.09.04
 *
 */

#include "c4mlib/hardware2/src/m128/pwm_imp.h"

#include "c4mlib/macro/src/bits_op.h"
#include "c4mlib/macro/src/std_def.h"
#include "c4mlib/macro/src/std_res.h"

#include <avr/io.h>

#include "c4mlib/config/pwm.cfg"

static uint8_t pwm0_hw_init(PwmIntStr_t *str_p);
static uint8_t pwm1_hw_init(PwmIntStr_t *str_p);
static uint8_t pwm2_hw_init(PwmIntStr_t *str_p);
static uint8_t pwm3_hw_init(PwmIntStr_t *str_p);

PwmIntStr_t PwmImp[PWM_HW_NUM] = {
    {
        .PwmSet = PWM0_HW_SET_CFG,
        .SetFunc_p = pwm0_hw_init
    },
    {
        .PwmSet = PWM1_HW_SET_CFG,
        .SetFunc_p = pwm1_hw_init
    },
    {
        .PwmSet = PWM2_HW_SET_CFG,
        .SetFunc_p = pwm2_hw_init
    },
    {
        .PwmSet = PWM3_HW_SET_CFG,
        .SetFunc_p = pwm3_hw_init
    }
};

// PwmIntStrList_p is used in std_isr
PwmIntStr_t *PwmIntStrList_p[PWM_HW_NUM];

uint8_t pwm0_hw_init(PwmIntStr_t *str_p) {
    
    switch (str_p->PwmSet.ClockSource0) {
        case PWM_CLOCKSOURCE_CLK_IO:
            ASSR &= ~(1 << AS0);
            ASSR |= (0 << AS0);
            break;
        case PWM_CLOCKSOURCE_CLK_OSC:
            ASSR &= ~(1 << AS0);
            ASSR |= (1 << AS0);
            break;
    }
    switch (str_p->PwmSet.ClockSource1) {
        
        case PWM0_CLOCKPRESCALEDBY_1:
            TCCR0 &= ~((1 << CS02) | (1 << CS01)| (1 << CS00));
            TCCR0 |= ((0 << CS02) | (0 << CS01)| (1 << CS00));
            break;
        case PWM0_CLOCKPRESCALEDBY_8:
            TCCR0 &= ~((1 << CS02) | (1 << CS01)| (1 << CS00));
            TCCR0 |= ((0 << CS02) | (1 << CS01)| (0 << CS00));
            break;
        case PWM0_CLOCKPRESCALEDBY_32:
            TCCR0 &= ~((1 << CS02) | (1 << CS01)| (1 << CS00));
            TCCR0 |= ((0 << CS02) | (1 << CS01)| (1 << CS00));
            break;
        case PWM0_CLOCKPRESCALEDBY_64:
            TCCR0 &= ~((1 << CS02) | (1 << CS01)| (1 << CS00));
            TCCR0 |= ((1 << CS02) | (0 << CS01)| (0 << CS00));
            break;
        case PWM0_CLOCKPRESCALEDBY_128:
            TCCR0 &= ~((1 << CS02) | (1 << CS01)| (1 << CS00));
            TCCR0 |= ((1 << CS02) | (0 << CS01)| (1 << CS00));
            break;
        case PWM0_CLOCKPRESCALEDBY_256:
            TCCR0 &= ~((1 << CS02) | (1 << CS01)| (1 << CS00));
            TCCR0 |= ((1 << CS02) | (1 << CS01)| (0 << CS00));
            break;
        case PWM0_CLOCKPRESCALEDBY_1024:
            TCCR0 &= ~((1 << CS02) | (1 << CS01)| (1 << CS00));
            TCCR0 |= ((1 << CS02) | (1 << CS01)| (1 << CS00));
            break;
    }
    switch (str_p->PwmSet.WaveMode0) {
        case WAVEMODE_CENTRAL_ALIGN:
            TCCR0 &= ~((1 << WGM01) | (1 << WGM00));
            TCCR0 |= ((0 << WGM01) | (1 << WGM00));
            break;
        case WAVEMODE_EDGE_ALIGN:
            TCCR0 &= ~((1 << WGM01) | (1 << WGM00));
            TCCR0 |= ((1 << WGM01) | (1 << WGM00));
            break;
    }
    switch (str_p->PwmSet.WaveOut0) {
        case WAVEOUT_NPULSE:
            TCCR0 &= ~((1 << COM01) | (1 << COM00));
            TCCR0 |= ((1 << COM01) | (0 << COM00));
            break;
        case WAVEOUT_PPULSE:
            TCCR0 &= ~((1 << COM01) | (1 << COM00));
            TCCR0 |= ((1 << COM01) | (1 << COM00));
            break;

    }
    switch (str_p->PwmSet.PWMIntEn) {
        case DISABLE:
        
            TIMSK &= ~(1 << TOIE0);
            TIMSK |= (0 << TOIE0);
            break;
        case ENABLE:
            TIMSK &= ~(1 << TOIE0);
            TIMSK |= (1 << TOIE0);
            break;
    }
    switch (str_p->PwmSet.WaveoutPin0) {
        case INPUT:
            DDRB &= ~(1 << PB4);
            DDRB |= (0 << PB4);
            break;
        case OUTPUT:
            DDRB &= ~(1 << PB4);
            DDRB |= (1 << PB4);
            break;
    }
    if( str_p->PwmSet.PulseWidth0 <  str_p->PwmSet.PWMMaxWidth){
        OCR0 = str_p->PwmSet.PulseWidth0 ;
    }
    
    return 0 ;
}

uint8_t pwm1_hw_init(PwmIntStr_t *str_p) {
    
    switch (str_p->PwmSet.ClockSource1) {

        case PWM123_CLOCKPRESCALEDBY_1:
            TCCR1B &= ~((1 << CS12) | (1 << CS11)| (1 << CS10));
            TCCR1B |= ((0 << CS12) | (0 << CS11)| (1 << CS10));
            break;
        case PWM123_CLOCKPRESCALEDBY_8:
            TCCR1B &= ~((1 << CS12) | (1 << CS11)| (1 << CS10));
            TCCR1B |= ((0 << CS12) | (1 << CS11)| (0 << CS10));
            break;
        case PWM123_CLOCKPRESCALEDBY_64:
            TCCR1B &= ~((1 << CS12) | (1 << CS11)| (1 << CS10));
            TCCR1B |= ((0 << CS12) | (1 << CS11)| (1 << CS10));
            break;
        case PWM123_CLOCKPRESCALEDBY_256:
            TCCR1B &= ~((1 << CS12) | (1 << CS11)| (1 << CS10));
            TCCR1B |= ((1 << CS12) | (0 << CS11)| (0 << CS10));
            break;
        case PWM123_CLOCKPRESCALEDBY_1024:
            TCCR1B &= ~((1 << CS12) | (1 << CS11)| (1 << CS10));
            TCCR1B |= ((1 << CS12) | (0 << CS11)| (1 << CS10));
            break;
    }
    switch (str_p->PwmSet.WaveMode0) {
        case WAVEMODE_CENTRAL_ALIGN:
            /* 
             * mode10：
             * WGM13    WGM12   WGM11   WGM10
             * 1        0       1       0
            */
            TCCR1A &= ~((1 << WGM11) | (1 << WGM10));
            TCCR1A |= ((1 << WGM11) | (0 << WGM10));
    
            TCCR1B &= ~((1 << WGM13) | (1 << WGM12));
            TCCR1B |= ((1 << WGM13) | (0 << WGM12));
            break;
        case WAVEMODE_EDGE_ALIGN:
            /* 
             * mode14：
             * WGM13    WGM12   WGM11   WGM10
             * 1        1       1       0
            */
            TCCR1A &= ~((1 << WGM11) | (1 << WGM10));
            TCCR1A |= ((1 << WGM11) | (0 << WGM10));
    
            TCCR1B &= ~((1 << WGM13) | (1 << WGM12));
            TCCR1B |= ((1 << WGM13) | (1 << WGM12));
            break;
    }
    switch (str_p->PwmSet.WaveOut0) {
        case WAVEOUT_NPULSE:
            // Compare Output Mode for Channel A
            TCCR1A &= ~((1 << COM1A1) | (1 << COM1A0));
            TCCR1A |= ((1 << COM1A1) | (0 << COM1A0));
            break;
        case WAVEOUT_PPULSE:
            // Compare Output Mode for Channel A
            TCCR1A &= ~((1 << COM1A1) | (1 << COM1A0));
            TCCR1A |= ((1 << COM1A1) | (1 << COM1A0));
            break;

    }
    switch (str_p->PwmSet.WaveOut1) {
        case WAVEOUT_NPULSE:
            // Compare Output Mode for Channel B
            TCCR1A &= ~((1 << COM1B1) | (1 << COM1B0));
            TCCR1A |= ((1 << COM1B1) | (0 << COM1B0));
            break;
        case WAVEOUT_PPULSE:
            // Compare Output Mode for Channel B
            TCCR1A &= ~((1 << COM1B1) | (1 << COM1B0));
            TCCR1A |= ((1 << COM1B1) | (1 << COM1B0));
            break;

    }
    switch (str_p->PwmSet.WaveOut2) {
        case WAVEOUT_NPULSE:
            // Compare Output Mode for Channel C
            TCCR1A &= ~((1 << COM1C1) | (1 << COM1C0));
            TCCR1A |= ((1 << COM1C1) | (0 << COM1C0));
            break;
        case WAVEOUT_PPULSE:
            // Compare Output Mode for Channel C
            TCCR1A &= ~((1 << COM1C1) | (1 << COM1C0));
            TCCR1A |= ((1 << COM1C1) | (1 << COM1C0));
            break;

    }
    switch (str_p->PwmSet.PWMIntEn) {
        case DISABLE:
            TIMSK &= ~(1 << TOIE1);
            TIMSK |= (0 << TOIE1);
            break;
        case ENABLE:
            TIMSK &= ~(1 << TOIE1);
            TIMSK |= (1 << TOIE1);
            break;
    }
    switch (str_p->PwmSet.WaveoutPin0) {
        case INPUT:
            DDRB &= ~(1 << PB5);
            DDRB |= (0 << PB5);
            break;
        case OUTPUT:
            DDRB &= ~(1 << PB5);
            DDRB |= (1 << PB5);
            break;
    }    
    switch (str_p->PwmSet.WaveoutPin1) {
        case INPUT:
            DDRB &= ~(1 << PB6);
            DDRB |= (0 << PB6);
            break;
        case OUTPUT:
            DDRB &= ~(1 << PB6);
            DDRB |= (1 << PB6);
            break;
    }    
    switch (str_p->PwmSet.WaveoutPin2) {
        case INPUT:
            DDRB &= ~(1 << PB7);
            DDRB |= (0 << PB7);
            break;
        case OUTPUT:
            DDRB &= ~(1 << PB7);
            DDRB |= (1 << PB7);
            break;
    }
    if( str_p->PwmSet.PulseWidth0 <  str_p->PwmSet.PWMMaxWidth){
        OCR1A = str_p->PwmSet.PulseWidth0 ;
    }
    if( str_p->PwmSet.PulseWidth1 <  str_p->PwmSet.PWMMaxWidth){
        OCR1B = str_p->PwmSet.PulseWidth1 ;
    }
    if( str_p->PwmSet.PulseWidth2 <  str_p->PwmSet.PWMMaxWidth){
        OCR1C = str_p->PwmSet.PulseWidth2 ;
    }

    return 0;
}

uint8_t pwm2_hw_init(PwmIntStr_t *str_p) {

    switch (str_p->PwmSet.ClockSource1) {
        case PWM123_CLOCKPRESCALEDBY_1:
            TCCR2 &= ~((1 << CS22) | (1 << CS21)| (1 << CS20));
            TCCR2 |= ((0 << CS22) | (0 << CS21)| (1 << CS20));
            break;
        case PWM123_CLOCKPRESCALEDBY_8:
            TCCR2 &= ~((1 << CS22) | (1 << CS21)| (1 << CS20));
            TCCR2 |= ((0 << CS22) | (1 << CS21)| (0 << CS20));
            break;
        case PWM123_CLOCKPRESCALEDBY_64:
            TCCR2 &= ~((1 << CS22) | (1 << CS21)| (1 << CS20));
            TCCR2 |= ((0 << CS22) | (1 << CS21)| (1 << CS20));
            break;
        case PWM123_CLOCKPRESCALEDBY_256:
            TCCR2 &= ~((1 << CS22) | (1 << CS21)| (1 << CS20));
            TCCR2 |= ((1 << CS22) | (0 << CS21)| (0 << CS20));
            break;
        case PWM123_CLOCKPRESCALEDBY_1024:
            TCCR2 &= ~((1 << CS22) | (1 << CS21)| (1 << CS20));
            TCCR2 |= ((1 << CS22) | (0 << CS21)| (1 << CS20));
            break;  
        }
    switch (str_p->PwmSet.WaveMode0) {
        case WAVEMODE_CENTRAL_ALIGN:
            TCCR2 &= ~((1 << WGM21) | (1 << WGM20));
            TCCR2 |= ((0 << WGM21) | (1 << WGM20));
            break;
        case WAVEMODE_EDGE_ALIGN:
            TCCR2 &= ~((1 << WGM21) | (1 << WGM20));
            TCCR2 |= ((1 << WGM21) | (1 << WGM20));
            break;
    }
    switch (str_p->PwmSet.WaveOut0) {
        case WAVEOUT_NPULSE:
            TCCR2 &= ~((1 << COM21) | (1 << COM20));
            TCCR2 |= ((1 << COM21) | (0 << COM20));
            break;
        case WAVEOUT_PPULSE:
            TCCR2 &= ~((1 << COM21) | (1 << COM20));
            TCCR2 |= ((1 << COM21) | (1 << COM20));
            break;

    }
    switch (str_p->PwmSet.PWMIntEn) {
        case DISABLE:
            TIMSK &= ~(1 << TOIE2);
            TIMSK |= (0 << TOIE2);
            break;
        case ENABLE:
            TIMSK &= ~(1 << TOIE2);
            TIMSK |= (1 << TOIE2);
            break;
    }    
    switch (str_p->PwmSet.WaveoutPin0) {
        case INPUT:
            DDRB &= ~(1 << PB7);
            DDRB |= (0 << PB7);
            break;
        case OUTPUT:
            DDRB &= ~(1 << PB7);
            DDRB |= (1 << PB7);
            break;
    }
    if( str_p->PwmSet.PulseWidth0 <  str_p->PwmSet.PWMMaxWidth){
        OCR2 = str_p->PwmSet.PulseWidth0 ;
    }
    return 0;
}

uint8_t pwm3_hw_init(PwmIntStr_t *str_p){
    
    switch (str_p->PwmSet.ClockSource1) {
        case PWM123_CLOCKPRESCALEDBY_1:
            TCCR3B &= ~((1 << CS32) | (1 << CS31)| (1 << CS30));
            TCCR3B |= ((0 << CS32) | (0 << CS31)| (1 << CS30));
            break;
        case PWM123_CLOCKPRESCALEDBY_8:
            TCCR3B &= ~((1 << CS32) | (1 << CS31)| (1 << CS30));
            TCCR3B |= ((0 << CS32) | (1 << CS31)| (0 << CS30));
            break;
        case PWM123_CLOCKPRESCALEDBY_64:
            TCCR3B &= ~((1 << CS32) | (1 << CS31)| (1 << CS30));
            TCCR3B |= ((0 << CS32) | (1 << CS31)| (1 << CS30));
            break;
        case PWM123_CLOCKPRESCALEDBY_256:
            TCCR3B &= ~((1 << CS32) | (1 << CS31)| (1 << CS30));
            TCCR3B |= ((1 << CS32) | (0 << CS31)| (0 << CS30));
            break;
        case PWM123_CLOCKPRESCALEDBY_1024:
            TCCR3B &= ~((1 << CS32) | (1 << CS31)| (1 << CS30));
            TCCR3B |= ((1 << CS32) | (0 << CS31)| (1 << CS30));
            break;  
        }
    switch (str_p->PwmSet.WaveMode0) {
        case WAVEMODE_CENTRAL_ALIGN:
            /* 
             * mode10：
             * WGM33    WGM32   WGM31   WGM30
             * 1        0       1       0
            */
            TCCR3A &= ~((1 << WGM31) | (1 << WGM30));
            TCCR3A |= ((1 << WGM31) | (0 << WGM30));
    
            TCCR3B &= ~((1 << WGM33) | (1 << WGM32));
            TCCR3B |= ((1 << WGM33) | (0 << WGM32));
            break;
        case WAVEMODE_EDGE_ALIGN:
            /* 
             * mode14：
             * WGM33    WGM32   WGM31   WGM30
             * 1        1       1       0
            */
            TCCR3A &= ~((1 << WGM31) | (1 << WGM30));
            TCCR3A |= ((1 << WGM31) | (0 << WGM30));
    
            TCCR3B &= ~((1 << WGM33) | (1 << WGM32));
            TCCR3B |= ((1 << WGM33) | (1 << WGM32));
            break;
    }
    switch (str_p->PwmSet.WaveOut0) {
        case WAVEOUT_NPULSE:
            // Compare Output Mode for Channel A
            TCCR3A &= ~((1 << COM3A1) | (1 << COM3A0));
            TCCR3A |= ((1 << COM3A1) | (0 << COM3A0));
            break;
        case WAVEOUT_PPULSE:
            // Compare Output Mode for Channel A
            TCCR3A &= ~((1 << COM3A1) | (1 << COM3A0));
            TCCR3A |= ((1 << COM3A1) | (1 << COM3A0));
            break;
    }
    switch (str_p->PwmSet.WaveOut1) {
        case WAVEOUT_NPULSE:
            // Compare Output Mode for Channel B
            TCCR3A &= ~((1 << COM3B1) | (1 << COM3B0));
            TCCR3A |= ((1 << COM3B1) | (0 << COM3B0));
            break;
        case WAVEOUT_PPULSE:
            // Compare Output Mode for Channel B
            TCCR3A &= ~((1 << COM3B1) | (1 << COM3B0));
            TCCR3A |= ((1 << COM3B1) | (1 << COM3B0));
            break;
    }
    switch (str_p->PwmSet.WaveOut2) {
        case WAVEOUT_NPULSE:
            // Compare Output Mode for Channel C
            TCCR3A &= ~((1 << COM3C1) | (1 << COM3C0));
            TCCR3A |= ((1 << COM3C1) | (0 << COM3C0));
            break;
        case WAVEOUT_PPULSE:
            // Compare Output Mode for Channel C
            TCCR3A &= ~((1 << COM3C1) | (1 << COM3C0));
            TCCR3A |= ((1 << COM3C1) | (1 << COM3C0));
            break;
    }
    switch (str_p->PwmSet.PWMIntEn) {
        case DISABLE:
            ETIMSK &= ~(1 << TOIE3);
            ETIMSK |= (0 << TOIE3);
            break;
        case ENABLE:
            ETIMSK &= ~(1 << TOIE3);
            ETIMSK |= (1 << TOIE3);
            break;
    }
    
    switch (str_p->PwmSet.WaveoutPin0) {
        case INPUT:
            DDRE &= ~(1 << PE3);
            DDRE |= (0 << PE3);
            break;
        case OUTPUT:
            DDRE &= ~(1 << PE3);
            DDRE |= (1 << PE3);
            break;
    }
    switch (str_p->PwmSet.WaveoutPin1) {
        case INPUT:
            DDRE &= ~(1 << PE4);
            DDRE |= (0 << PE4);
            break;
        case OUTPUT:
            DDRE &= ~(1 << PE4);
            DDRE |= (1 << PE4);
            break;
    }
    switch (str_p->PwmSet.WaveoutPin2) {
        case INPUT:
            DDRE &= ~(1 << PE5);
            DDRE |= (0 << PE5);
            break;
        case OUTPUT:
            DDRE &= ~(1 << PE5);
            DDRE |= (1 << PE5);
            break;
    }
    if( str_p->PwmSet.PulseWidth0 <  str_p->PwmSet.PWMMaxWidth){
        OCR3A = str_p->PwmSet.PulseWidth0 ;
    }
    if( str_p->PwmSet.PulseWidth1 <  str_p->PwmSet.PWMMaxWidth){
        OCR3B = str_p->PwmSet.PulseWidth1 ;
    }
    if( str_p->PwmSet.PulseWidth2 <  str_p->PwmSet.PWMMaxWidth){
        OCR3C = str_p->PwmSet.PulseWidth2 ;
    }
    return 0;

}
