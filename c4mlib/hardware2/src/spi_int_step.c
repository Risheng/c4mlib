/**
 * @file spi_int_step.c
 * @author LiYu87
 * @date 2019.09.04
 * @brief
 *
 * NOTE
 * SpiInt_step is used by hardware interrupt
 * and its code is in hardware/std_isr.
 */

#include "c4mlib/hardware2/src/spi.h"

void SpiInt_step(SpiIntStr_t *IntStr_p) {
    if (IntStr_p != NULL) {
        for (uint8_t i = 0; i < IntStr_p->IntTotal; i++) {
            if (IntStr_p->IntFb[i].enable) {
                IntStr_p->IntFb[i].func_p(IntStr_p->IntFb[i].funcPara_p);
            }
        }
    }
}
