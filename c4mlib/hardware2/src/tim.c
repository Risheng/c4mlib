/**
 * @file timint.c
 * @author Deng Xiang-Guan
 * @date 2019.08.08
 * @brief 提供ASA函式庫標準中斷介面，將原生硬體Interrupt呼叫函式占用，並提供登陸函式介面。
 */

#include "c4mlib/hardware2/src/tim.h"

#include "c4mlib/debug/src/debug.h"
#include "c4mlib/hardware2/src/tim_imp.h"
#include "c4mlib/macro/src/std_res.h"

uint8_t TimInt_net(TimIntStr_t* IntStr_p, uint8_t Num) {
    if(IntStr_p != NULL) {
        if(Num < TIM_HW_NUM) {
            TimIntStrList_p[Num] = IntStr_p;

            IntStr_p->SetFunc_p = TimImp[Num].SetFunc_p;
            DEBUG_INFO("Succeed to net TIMER number %d\n", Num);
            return INT_OK;
        }
        else {
            // warning Num is excessive, IntStr_p can not net.
            DEBUG_INFO("Warning Num is excessive, IntStr_p can not net.");
            return INT_ERROR_NUM_EXCEED;
        }
    }
    return INT_ERROR_POINTER_IS_NULL;
}

uint8_t TimInt_set(TimIntStr_t* IntStr_p) {
    if (IntStr_p != NULL) {
        return IntStr_p->SetFunc_p(IntStr_p);
    }
    else {
        return INT_ERROR_POINTER_IS_NULL;
    }
}

uint8_t TimInt_reg(TimIntStr_t* IntStr_p, Func_t FbFunc_p, void* FbPara_p) {
    if (IntStr_p != NULL) {
        uint8_t new_Id = IntStr_p->IntTotal;
        IntStr_p->IntFb[new_Id].enable = 0;  // Default Disable
        IntStr_p->IntFb[new_Id].func_p = FbFunc_p;
        IntStr_p->IntFb[new_Id].funcPara_p = FbPara_p;
        IntStr_p->IntTotal++;

        return new_Id;
    }
    else {
        DEBUG_INFO("Warning !!! IntStr_p is NULL\n");
        return INT_ERROR_POINTER_IS_NULL;
    }
}

void TimInt_en(TimIntStr_t* IntStr_p, uint8_t Fb_Id, uint8_t enable) {
    if (IntStr_p != NULL) {
        if (Fb_Id < IntStr_p->IntTotal) {
            IntStr_p->IntFb[Fb_Id].enable = enable;
        }
    }
    else {
        DEBUG_INFO("Warning !!! IntStr_p is NULL\n");
    }
}
