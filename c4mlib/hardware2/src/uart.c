/**
 * @file uart.c
 * @author Deng Xiang-Guan
 * @date 2019.08.08
 * @brief
 * 提供ASA函式庫標準中斷介面，將原生硬體Interrupt呼叫函式占用，並提供登陸函式介面。
 */

#include "c4mlib/hardware2/src/uart.h"

#include "c4mlib/debug/src/debug.h"
#include "c4mlib/hardware2/src/uart_imp.h"
#include "c4mlib/macro/src/std_res.h"

uint8_t UartInt_net(UartIntStr_t* IntStr_p, uint8_t Num) {
    if (IntStr_p != NULL) {
        if (Num < UART_HW_NUM) {
            UartTxIntStrList_p[Num] = IntStr_p;
            UartRxIntStrList_p[Num] = IntStr_p;

            IntStr_p->SetFunc_p = UartImp[Num].SetFunc_p;

            DEBUG_INFO("Succeed to net UART number %d\n", Num);
            return INT_OK;
        }
        else {
            // warning Num is excessive, IntStr_p can not net.
            DEBUG_INFO("Warning Num is excessive, IntStr_p TX can not net.\n");
            return INT_ERROR_NUM_EXCEED;
        }
    }
    return INT_ERROR_POINTER_IS_NULL;
}

uint8_t UartInt_set(UartIntStr_t* IntStr_p) {
    if (IntStr_p != NULL) {
        return IntStr_p->SetFunc_p(IntStr_p);
    }
    else {
        return INT_ERROR_POINTER_IS_NULL;
    }
}

uint8_t UartTxInt_reg(UartIntStr_t* IntStr_p, Func_t FbFunc_p, void* FbPara_p) {
    if (IntStr_p != NULL) {
        uint8_t new_Id = IntStr_p->TxIntTotal;
        IntStr_p->TxIntFb[new_Id].enable = 0;
        IntStr_p->TxIntFb[new_Id].func_p = FbFunc_p;
        IntStr_p->TxIntFb[new_Id].funcPara_p = FbPara_p;
        IntStr_p->TxIntTotal++;
        return new_Id;
    }
    else {
        DEBUG_INFO("Warning !!! IntStr_p is NULL\n");
        return INT_ERROR_POINTER_IS_NULL;
    }
}

uint8_t UartRxInt_reg(UartIntStr_t* IntStr_p, Func_t FbFunc_p, void* FbPara_p) {
    if (IntStr_p != NULL) {
        uint8_t new_Id = IntStr_p->RxIntTotal;
        IntStr_p->RxIntFb[new_Id].enable = 0;  // Default Disable
        IntStr_p->RxIntFb[new_Id].func_p = FbFunc_p;
        IntStr_p->RxIntFb[new_Id].funcPara_p = FbPara_p;
        IntStr_p->RxIntTotal++;
        return new_Id;
    }
    else {
        DEBUG_INFO("Warning !!! IntStr_p is NULL\n");
        return INT_ERROR_POINTER_IS_NULL;
    }
}

void UartTxInt_en(UartIntStr_t* IntStr_p, uint8_t Fb_Id, uint8_t enable) {
    if (IntStr_p != NULL) {
        if (Fb_Id < IntStr_p->TxIntTotal) {
            IntStr_p->TxIntFb[Fb_Id].enable = enable;
        }
    }
    else {
        DEBUG_INFO("Warning !!! IntStr_p is NULL\n");
    }
}

void UartRxInt_en(UartIntStr_t* IntStr_p, uint8_t Fb_Id, uint8_t enable) {
    if (IntStr_p != NULL) {
        if (Fb_Id < IntStr_p->RxIntTotal) {
            IntStr_p->RxIntFb[Fb_Id].enable = enable;
        }
    }
    else {
        DEBUG_INFO("Warning !!! IntStr_p is NULL\n");
    }
}
