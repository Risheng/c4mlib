/**
 * @file std_def.h
 * @author LiYu87
 * @date 2019.09.06
 * @brief Provide C4MLIB standard macros.
 *
 */

#ifndef C4MLIB_MACRO_STD_DEF_H
#define C4MLIB_MACRO_STD_DEF_H

#define DISABLE 0  ///< 0 啟用 @ingroup macro_macro
#define ENABLE  1  ///< 1 關閉 @ingroup macro_macro

/**
 * @brief 確認 EN 是否為 ENABLE 或 DISABLE
 * @ingroup macro_macro
 */
#define IS_ENABLE_OR_DISABLE(EN) ((EN) == ENABLE || (EN) == DISABLE)

#endif  // C4MLIB_MACRO_STD_DEF_H
