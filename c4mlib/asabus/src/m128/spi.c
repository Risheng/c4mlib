/**
 * @file spi.c
 * @author LiYu87
 * @brief ASA M128 之ASABUS SPI 之硬體初始化。
 * @date 2019.10.13
 */

#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/macro/src/bits_op.h"
#include "c4mlib/macro/src/std_res.h"

#include <avr/io.h>

void ASABUS_SPI_init(void) {
    // set mosi, sck as outputs
    // set miso as inputs
    REGFPT(BUS_SPI_DDR, BUS_SPI_MASK, BUS_SPI_SHIFT, BUS_SPI_OUT);
    // Enable SPI, set clock rate fosc/64, SPI Transfer format mode 0
    SPCR = (1 << SPE) | (1 << MSTR) | (1 << SPR1) | (1 << SPR0) | (1 << SPI2X);
}

char ASABUS_SPI_swap(char data) {
    SPDR = data;
    while (!(SPSR & (1 << SPIF)))
        ;
    return SPDR;
}
