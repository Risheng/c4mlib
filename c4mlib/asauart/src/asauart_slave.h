/**
 * @file asauart_slave.h
 * @author Ye cheng-Wei
 * @author Wu Cheng Han
 * @date 2019.10.07
 * @brief 放置 UART slave端 通訊封包通用函式及Macro
 */

#ifndef C4MLIB_ASAUART_ASAUART_SLAVE_H
#define C4MLIB_ASAUART_ASAUART_SLAVE_H

#define UART_BROADCAST_ID 0  ///< ASA UART 廣播用 ID

#define DEFAULTUARTBAUD 38400

#define ASAUART_CMD_HEADER 0xAA
#define ASAUART_RSP_HEADER 0xAB
#define DATAOK 0x00

#define UARTS_SM_HEADER 0  ///< UART解包器狀態機代號:  等待標頭
#define UARTS_SM_UID 1     ///< UART解包器狀態機代號:  等待DEVICE ID
#define UARTS_SM_ADDR 2  ///< UART解包器狀態機代號:  等待Register Address
#define UARTS_SM_BYTES 3  ///< UART解包器狀態機代號:  等待位元組數
#define UARTS_SM_DATA 4   ///< UART解包器狀態機代號:  等待資料
#define UARTS_SM_CHKSUM 5  ///< UART解包器狀態機代號:  等待檢查碼

/**
 * @brief Slave端 Mode 0 串列埠 Rx中斷 執行片段。
 * @ingroup asauart_func
 *
 * 進行UART封包解包，解包狀態機轉移與執行皆於呼叫階段執行。
 *
 * @warning 必須先將UART Interrupt致能，並注意是否已於 RemoReg_init()
 * 內實作將此函式連接至uart_hal.c內的UARTS_inst.rx_compelete_cb。
 */

void ASA_UARTS0_rx_step(void);

/**
 * @brief Slave端 Mode 1 串列埠 Rx中斷 執行片段。
 * @ingroup asauart_func
 *
 * 進行UART封包解包，解包狀態機轉移與執行皆於呼叫階段執行。
 *
 */
void ASA_UARTS1_rx_step(void);

/**
 * @brief Slave端 Mode 2 串列埠 Rx中斷 執行片段。
 * @ingroup asauart_func
 *
 * 進行UART封包解包，解包狀態機轉移與執行皆於呼叫階段執行。
 */

void ASA_UARTS2_rx_step(void);

/**
 * @brief Slave端 Mode 3 串列埠 Rx中斷 執行片段。
 * @ingroup asauart_func
 *
 * 進行UART封包解包，解包狀態機轉移與執行皆於呼叫階段執行。
 */
void ASA_UARTS3_rx_step(void);

/**
 * @brief Slave端 Mode 4 串列埠 Rx中斷 執行片段。
 *
 * 進行UART封包解包，解包狀態機轉移與執行皆於呼叫階段執行。
 */
void ASA_UARTS4_rx_step(void);

/**
 * @brief Slave端 Mode 5 串列埠 Rx中斷 執行片段。
 * @ingroup asauart_func
 *
 * 進行UART封包解包，解包狀態機轉移與執行皆於呼叫階段執行。
 */
void ASA_UARTS5_rx_step(void);

/**
 * @brief Slave端 Mode 6 串列埠 Rx中斷 執行片段。
 *
 * 進行UART封包解包，解包狀態機轉移與執行皆於呼叫階段執行。
 */
void ASA_UARTS6_rx_step(void);

/**
 * @brief Slave端 Mode 7 串列埠 Rx中斷 執行片段。
 * @ingroup asauart_func
 *
 * 進行UART封包解包，解包狀態機轉移與執行皆於呼叫階段執行。
 */
void ASA_UARTS7_rx_step(void);

/**
 * @brief Slave端 Mode 8 串列埠 Rx中斷 執行片段。
 *
 * 進行UART封包解包，解包狀態機轉移與執行皆於呼叫階段執行。
 */
void ASA_UARTS8_rx_step(void);

/**
 * @brief Slave端 Mode 9 串列埠 Rx中斷 執行片段。
 * @ingroup asauart_func
 *
 * 進行UART封包解包，解包狀態機轉移與執行皆於呼叫階段執行。
 */
void ASA_UARTS9_rx(void);

/**
 * @brief Slave端 Mode 10 串列埠 Rx中斷 執行片段。
 * @ingroup asauart_func
 *
 * 進行UART封包解包，解包狀態機轉移與執行皆於呼叫階段執行。
 */
void ASA_UARTS10_rx_step(void);

/**
 * @brief Slave端 Mode 0 串列埠 Tx中斷 執行片段。
 * @ingroup asauart_func
 *
 * 進行UART封包解包，解包狀態機轉移與執行皆於呼叫階段執行。
 */
void ASA_UARTS0_tx_step(void);

/**
 * @brief Slave端 Mode 1 串列埠 Tx中斷 執行片段。
 * @ingroup asauart_func
 *
 * 進行UART封包解包，解包狀態機轉移與執行皆於呼叫階段執行。
 */
void ASA_UARTS1_tx_step(void);

/**
 * @brief Slave端 Mode 2 串列埠 Tx中斷 執行片段。
 * @ingroup asauart_func
 *
 * 進行UART封包解包，解包狀態機轉移與執行皆於呼叫階段執行。
 */
void ASA_UARTS2_tx_step(void);

/**
 * @brief Slave端 Mode 3 串列埠 Tx中斷 執行片段。
 * @ingroup asauart_func
 *
 * 進行UART封包解包，解包狀態機轉移與執行皆於呼叫階段執行。
 */
void ASA_UARTS3_tx_step(void);

/**
 * @brief Slave端 Mode 4 串列埠 Tx中斷 執行片段。
 * @ingroup asauart_func
 *
 * 進行UART封包解包，解包狀態機轉移與執行皆於呼叫階段執行。
 */
void ASA_UARTS4_tx_step(void);

/**
 * @brief Slave端 Mode 5 串列埠 Tx中斷 執行片段。
 * @ingroup asauart_func
 *
 * 進行UART封包解包，解包狀態機轉移與執行皆於呼叫階段執行。
 */
void ASA_UARTS5_tx_step(void);

/**
 * @brief Slave端 Mode 6 串列埠 Tx中斷 執行片段。
 * @ingroup asauart_func
 *
 * 進行UART封包解包，解包狀態機轉移與執行皆於呼叫階段執行。
 */
void ASA_UARTS6_tx_step(void);

/**
 * @brief Slave端 Mode 7 串列埠 Tx中斷 執行片段。
 * @ingroup asauart_func
 *
 * 進行UART封包解包，解包狀態機轉移與執行皆於呼叫階段執行。
 */
void ASA_UARTS7_tx_step(void);

/**
 * @brief Slave端 Mode 8 串列埠 Tx中斷 執行片段。。
 * @ingroup asauart_func
 *
 * 進行UART封包解包，解包狀態機轉移與執行皆於呼叫階段執行。。
 */
void ASA_UARTS8_tx_step(void);

/**
 * @brief Slave端 Mode 9 串列埠 Tx中斷 執行片段。
 *
 * 進行UART封包解包，解包狀態機轉移與執行皆於呼叫階段執行。
 */
void ASA_UARTS9_tx_step(void);

/**
 * @brief Slave端 Mode 10 串列埠 Tx中斷 執行片段。
 * @ingroup asauart_func
 *
 * 進行UART封包解包，解包狀態機轉移與執行皆於呼叫階段執行。
 */
void ASA_UARTS10_tx_step(void);

#endif  // C4MLIB_ASAUART_ASAUART_SLAVE_H
