/**
 * @file asauart_master.h
 * @author Ye cheng-Wei
 * @author Wu Cheng Han
 * @date 2019.10.07
 * @brief 放置 ASA UART Master 通訊函式及Marco
 */

#ifndef C4MLIB_ASAUART_MASTER_UART_H
#define C4MLIB_ASAUART_MASTER_UART_H

#include <stdint.h>

/**
 * @defgroup asauart_func asauart functions
 * @defgroup asauart_macro asauart macros
 */

#define ASAUART_CMD_HEADER 0xAA  ///< @ingroup asauart_macro
#define ASAUART_RSP_HEADER 0xAB  ///< @ingroup asauart_macro


/* Public Section Start */
/**
 * @brief ASA UART Master 多位元組傳送函式。
 *
 * @ingroup asauart_func
 *
 * @param Mode      UART 通訊模式，目前支援0, 1, 2, 3。
 * @param UartID    目標裝置的UART ID。
 * @param RegAdd    遠端讀寫暫存器(Register)的位址或控制旗標(control flag)。
 * @param Bytes     待送資料位元組數。
 * @param Data_p    待送資料指標。
 * @param WaitTick  位元組間延遲時間，單位為 1us。
 * @return  char    錯誤代碼：
 *                   - 0： 通訊成功。
 *                   - 1： Timeout。
 *                   - 4： SLA錯誤。
 *                   - 5： mode選擇錯誤。
 *
 * ASA 作為 Master端 透過UART通訊傳送資料。
 * Uart Master端 傳送給 Slave端，依照封包不同，分作11種 Mode ，如以下所示：
 *  - Mode 0：
 *      封包組成 為 [Header]、[UID]、[RegAdd]、[Data]、[Rec(Header)]。
 *      先後傳送資料 [ASAUART_CMD_HEADER (0xAA)]、[UID]、[RegAdd]，隨後再根據
 *      資料筆數[Bytes]由低到高丟出資料，傳完資料後會傳送最後一筆接收的資料
 *      [checksum] 給Slave端驗證，Slave會回傳當作 [ASAUART_RSP_HEADER(0xAB)]
 *      成功資訊或錯誤資訊(0x06)。
 *  - Mode 1：
 *      封包組成 為 [ Data+CF(Control Flag) ]。
 *      傳送第一筆資料為 [RegAdd | Data_p[0]]，剩餘的傳輸資料由低到高傳送。
 *      * 此 mode 中 RegAdd 為控制旗標(control flag)。
 *  - Mode 2：
 *      封包組成 為 [ CF(Control Flag)+Data ]。
 *      傳送第一筆資料為 [RegAdd |Data_p[Bytes-1]]，剩餘的傳輸資料由高到低傳送。
 *      * 此 mode 中 RegAdd 為控制旗標(control flag)。
 *  - Mode 3：
 *      封包組成 為 [Data]，單純送收資料。
 *      注意：RegAdd 2 專門用來傳送資料，RegAdd 3 專門用來接收資料。
 *      由低到高傳送資料。
 *  - Mode 4：
 *      封包組成 為 [Data]，單純送收資料。
 *      注意：RegAdd 2 專門用來傳送資料，RegAdd 3 專門用來接收資料。
 *      由高到低傳送資料。
 *  - Mode 5：
 *      封包組成 為 [RegAdd(8bit)]、[Data]。
 *      注意：RegAdd 2、3 專門用來傳送資料，RegAdd 4、5 專門用來接收資料。
 *      先傳送一筆資料為 [RegAdd]，接著由低到高傳送資料 [Data]。
 *  - Mode 6：
 *      封包組成 為 [RegAdd(8bit)]、[Data]。
 *      注意：RegAdd 2、3 專門用來傳送資料，RegAdd 4、5 專門用來接收資料。
 *      先傳送一筆資料 [RegAdd]，接著由高到低傳送資料 [Data]。
 *  - Mode 7：
 *      封包組成 為 [ W (1bit，0)+RegAdd(7bit) ]、[Data]。
 *      先傳送一筆資料 [RegAdd]，接著由低到高傳送資料 [Data]。
 *  - Mode 8：
 *      封包組成 為 [ W (1bit，0)+RegAdd(7bit) ] 、[Data]。
 *      先傳送一筆資料 [RegAdd]，接著由高到低傳送資料 [Data]。
 *  - Mode 9：
 *      封包組成 為 [ RegAdd(7bit)+W (1bit，0) ]、[Data]。
 *      先傳送一筆資料 [RegAdd<<1]，接著由低到高傳送資料 [Data]。
 *  - Mode 10：
 *      封包組成 為 [ RegAdd(7bit)+W (1bit，0) ]、[Data]。
 *      先傳送一筆資料 [RegAdd<<1]，接著由高到低傳送資料 [Data]。
 */
char UARTM_trm(char Mode, char UartID, char RegAdd, char Bytes, void *Data_p,uint16_t WaitTick);

/**
 * @brief ASA UART Master 多位元組接收函式。
 *
 * @ingroup asauart_func
 *
 * @param Mode      UART 通訊模式，目前支援0, 1, 2, 3。
 * @param UartID    UART僕ID：   目標裝置的UART ID
 * @param RegAdd    遠端讀寫暫存器(Register)的位址或控制旗標(control flag)。
 * @param Bytes     待收資料位元組數。
 * @param Data_p    待收資料指標。
 * @param WaitTick  位元組間延遲時間，單位為 1us。
 * @return  char    錯誤代碼：
 *                   - 0： 通訊成功。
 *                   - 1： Timeout。
 *                   - 4： SLA錯誤。
 *                   - 5： mode選擇錯誤。
 *
 * Uart Master端 傳送給 Slave端，依照封包不同，分作11種 Mode ，如以下所示：
 *  - Mode 0：
 *      封包組成 為 [Header]、[UID]、[RegAdd]、[Data]、[Rec(Header)]。
 *      先後傳送資料 [ASAUART_CMD_HEADER (0xAA)]、[UID]、[RegAdd]、[checksum]
 *      後，讀取Slave端回傳的[ASAUART_RSP_HEADER]，確認無誤後 再根據資料筆數
 *      [Bytes] 由低到高接收資料，接收資料完後會接收Slave傳送的最後一筆資料
 *      [checksum] 進行比對，無誤後將資料存取起來。
 *  - Mode 1：
 *      封包組成 為 [ Data+CF(Control Flag) ]。
 *      注意：限定搭配 UARTM_trm()後 使用。
 *      由低到高接收資料。
 *      * 此 mode 中 RegAdd 為控制旗標(control flag)。
 *  - Mode 2：
 *      封包組成 為 [ CF(Control Flag)+Data ]。
 *      注意：限定搭配 UARTM_trm()後 使用。
 *      由高到低接收資料。
 *      * 此 mode 中 RegAdd 為控制旗標(control flag)。
 *  - Mode 3：
 *      封包組成 為 [Data]，單純送收資料。
 *      注意：RegAdd 2 專門用來傳送資料，RegAdd 3 專門用來接收資料。
 *      由低到高接收資料。
 *  - Mode 4：
 *      封包組成 為 [Data]，單純送收資料。
 *      注意：RegAdd 2 專門用來傳送資料，RegAdd 3 專門用來接收資料。
 *      由高到低接收資料。
 *  - Mode 5：
 *      封包組成 為 [RegAdd(8bit)]、[Data]。
 *      注意：RegAdd 2、3 專門用來傳送資料，RegAdd 4、5
 *      專門用來接收資料。先傳送一筆資料為 [RegAdd] ，
 *      接著由低到高接收資料 [Data]。
 *  - Mode 6：
 *      封包組成 為 [RegAdd(8bit)]、[Data]。
 *      注意：RegAdd 2、3 專門用來傳送資料，RegAdd 4、5
 *      專門用來接收資料。先傳送一筆資料 [RegAdd]，
 *      接著由高到低接收資料 [Data]。
 *  - Mode 7：
 *      封包組成 為 [ R (1bit，1)+RegAdd(7bit) ]、[Data]。
 *      先傳送一筆資料 RegAdd佔最低位元、R位元佔最高位元
 *      [0x80 | RegAdd]，接著由低到高接收資料 [Data]。
 *  - Mode 8：
 *      封包組成 為 [ R (1bit，1)+RegAdd(7bit) ]、[Data]。
 *      先傳送一筆資料 RegAdd佔最低位元、R位元佔最高位元
 *      [0x80 | RegAdd]，接著由高到低接收資料 [Data]。
 *  - Mode 9：
 *      封包組成 為 [ RegAdd(7bit)+R (1bit，1) ]、[Data]。
 *      先傳送一筆資料 RegAdd佔最高位元、R位元佔最低位元
 *      [RegAdd<<1 | 0x01]，接著由低到高接收資料 [Data]。
 *  - Mode 10：
 *      封包組成 為 [ RegAdd(7bit)+R (1bit，1) ]、[Data]。
 *      先傳送一筆資料 RegAdd佔最高位元、R位元佔最低位元
 *      [RegAdd<<1 | 0x01]，接著由高到低接收資料 [Data]。
 */
char UARTM_rec(char Mode, char UartID, char RegAdd, char Bytes, void *Data_p,uint16_t WaitTick);

/**
 * @brief ASA UART Master 旗標式傳送函式。
 *
 * @ingroup asauart_func
 *
 * @param Mode      UART 通訊模式，目前支援0, 3, 5, 7, 9。
 * @param UartID    UART僕ID：      目標的裝置UART ID
 * @param RegAdd    遠端讀寫暫存器(Register)的位址或控制旗標(control flag)。
 * @param Mask      位元組遮罩。
 * @param Shift     待送旗標向左位移。
 * @param Data_p    待送資料指標。
 * @param WaitTick  位元組間延遲時間，單位為 1us。
 * @return  char    錯誤代碼：
 *                   - 0： 通訊成功。
 *                   - 1： Timeout。
 *                   - 4： SLA錯誤。
 *                   - 5： mode選擇錯誤。
 *
 * Uart Master 旗標式資料傳輸是由UARTM_trm()與UARTM_rec()實現。
 * 依照功能將只支援指定Mode，通訊方式如下：
 *  - Mode 0： 使用UARTM_rec() Mode 0 讀取指定UID、RegAdd中的資料，
 *    將資料左移後，用遮罩取資料，最後使用UARTM_trm() Mode 0
 *    傳輸資料到指定RegAdd。
 *  - Mode 3： 使用UARTM_rec() Mode 3 讀取資料，將資料左移後，
 *    用遮罩取資料，最後使用UARTM_trm() Mode 3 傳輸資料。
 *  - Mode 5： 使用UARTM_rec() Mode 5 讀取指定RegAdd中的資料，
 *    將資料左移後，用遮罩取資料，最後使用UARTM_trm() Mode 5
 *    傳輸資料到指定RegAdd。
 *  - Mode 7： 使用UARTM_rec() Mode 7 讀取指定RegAdd中的資料，
 *    將資料左移後，用遮罩取資料，最後使用UARTM_trm() Mode 7
 *    傳輸資料到指定RegAdd。
 *  - Mode 9： 使用UARTM_rec() Mode 9 讀取指定RegAdd中的資料，
 *    將資料左移後，用遮罩取資料，最後使用UARTM_trm() Mode 9
 *    傳輸資料到指定RegAdd。
 *
 */
char UARTM_ftm(char Mode, char UartID, char RegAdd, char Mask, char Shift,
               char *Data_p, uint16_t WaitTick);

/**
 * @brief ASA UART Master 旗標式接收函式。
 *
 * @ingroup asauart_func
 *
 * @param Mode      UART 通訊模式，目前支援0, 3, 5 ,7 ,9。
 * @param UartID    UART僕ID：      目標的裝置UART ID
 * @param RegAdd    遠端讀寫暫存器(Register)的位址或控制旗標(control flag)。
 * @param Mask      位元組遮罩。
 * @param Shift     接收旗標向右位移。
 * @param Data_p    待收資料指標。
 * @param WaitTick  位元組間延遲時間，單位為 1us。
 * @return  char    錯誤代碼：
 *                   - 0： 通訊成功。
 *                   - 1： Timeout。
 *                   - 4： SLA錯誤。
 *                   - 5： mode選擇錯誤。
 *
 * Uart Master 旗標式資料接收是由TWIM_rec()實現。
 * 依照功能將只支援指定Mode，通訊方式如下：
 *  - Mode 0：
 *      使用UARTM_rec() Mode 0 讀取指定UID、RegAdd中的資料，
 *      將資料左移後，用遮罩取資料。
 *  - Mode 3：
 *      使用UARTM_rec() Mode 3 讀取資料，將資料左移後，
 *      用遮罩取資料。
 *  - Mode 5：
 *      使用UARTM_rec() Mode 5 讀取指定RegAdd中的資料，
 *      將資料左移後，用遮罩取資料。
 *  - Mode 7：
 *      使用UARTM_rec() Mode 7 讀取指定RegAdd中的資料，
 *      將資料左移後，用遮罩取資料。
 *  - Mode 9：
 *      使用UARTM_rec() Mode 9 讀取指定RegAdd中的資料，
 *      將資料左移後，用遮罩取資料。
 */
char UARTM_frc(char Mode, char UartID, char RegAdd, char Mask, char Shift,
               char *Data_p, uint16_t WaitTick);
/* Public Section End */

#endif  // C4MLIB_ASAUART_MASTER_UART_H
