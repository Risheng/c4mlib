/**
 * @file test_uart1.c
 * @author Wu Cheng Han
 * @date 2019.10.07
 * @brief 測試 ASA單板電腦 作為Master，透過Uart Mode1
 * 跟遠端Register 通訊，進行封包傳送接收。
 *
 * 需與 test_slave_uart1 一起做測試
 * 預測結果；
 * 原來 data1_buffer[5] = {5, 6, 7, 8, 9};
 * 原來 data2_buffer[5] = {0, 0, 0, 0, 0};
 *
 * 原來 data1_buffer[5] = {5, 6, 7, 8, 9};
 * 原來 data2_buffer[5] = {86, 87, 88, 89, 90};
 *
 * >>  【test 1】傳送 4 筆資料
 * >>  【test 1】UARTM_trm : data[0]=80
 * >>  【test 1】UARTM_trm : data[1]=6
 * >>  【test 1】UARTM_trm : data[2]=7
 * >>  【test 1】UARTM_trm : data[3]=8
 * >>  【test 1】UARTM_trm : data[4]=9
 * >>  【test 2】UARTM_rec : data[0]=86
 * >>  【test 2】UARTM_rec : data[1]=87
 * >>  【test 2】UARTM_rec : data[2]=88
 * >>  【test 2】UARTM_rec : data[3]=89
 * >>  【test 2】UARTM_rec : data[4]=90
 *
 */

#define CFbit 4  // 假設 CF 占了 4 bits

#include "c4mlib/asauart/src/asauart_master.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/isr.h"
#include "c4mlib/time/src/hal_time.h"

#include <util/delay.h>
#define DELAY 0
ISR(TIMER3_COMPA_vect) {
    HAL_tick();
}

// Initialize the TIME3 with CTC mode, interrupt at 1000 Hz
void init_timer();

int main() {
    C4M_DEVICE_set();
    HAL_time_init();

    init_timer();
    UARTM_Inst.init();

    sei();

    uint8_t data1_buffer[5] = {5, 6, 7, 8, 9};
    uint8_t data2_buffer[5] = {0, 0, 0, 0, 0};
    uint8_t CF = 0x01;
    uint8_t result = 0;

    /*
     * Mode1 先低後高
     * Data+CF (CF向右對齊 Data向左對齊)
     */

    /***** UARTM Mode1 Transmit test *****/
    /*** 【test 1】 ***/
    printf("【test 1】傳送 4 筆資料\n");
    CF = CF >> 0;
    data1_buffer[0] = data1_buffer[0] << CFbit;
    result = UARTM_trm(1, 0, CF, 4, data1_buffer, DELAY);
    if (result)
        printf("<1>UARTM_trm Fail [%u]\n", result);
    for (int i = 0; i < 5; i++) {
        printf("【test 1】UARTM_trm : data[%u]=%u\n", i, data1_buffer[i]);
    }
    HAL_delay(100);

    /***** UARTM Mode1  Receive test *****/
    result = UARTM_rec(1, 0, CF, 5, data2_buffer, DELAY);
    if (result)
        printf("<1>UARTM_rec Fail [%u]\n", result);
    for (int i = 0; i < 5; i++) {
        printf("【test 2】UARTM_rec : data[%u]=%u\n", i, data2_buffer[i]);
    }
    while (1) {
    }  // Block here
}

void init_timer() {
    // Pre-scale 1
    // COMA COMB non-inverting mode
    // 1    0
    // CTC with TOP ICRn
    // WGM3 WGM2 WGM1 WGM0
    // 1    1    0    0
    // TCCR3A  [COM3A1 COM3A0 COM3B1 COM3B0 COM3C1 COM3C0 WGM31 WGM30]
    // TCCR3B  [ICNC3 ICES3 �V WGM33 WGM32 CS32 CS31 CS30]

    ICR3 = 11058;
    TCCR3A = 0b00000000;
    TCCR3B = 0b00011001;
    TCNT3 = 0x00;
    ETIMSK |= 1 << OCIE3A;
}
