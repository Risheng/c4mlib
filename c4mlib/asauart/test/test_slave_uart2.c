/**
 * @file test_slave_uart2.c
 * @author Wu Cheng Han
 * @date 2019.10.07
 * @brief 測試 ASA單板電腦 作為Slave，建立遠端Register，當Master端
 * 透過Uart Mode2 跟遠端Register 通訊，進行封包接收，並拆出CF進行比對，
 * 再根據CF進行下一步動作、或傳送資料。
 *
 * 需與 test_uart2 一起做測試
 * 預測結果；
 * 原來 reg_1[4] = {1, 2, 3, 4};
 * 原來 reg_2[5] = {86, 87, 88, 89, 90};
 *
 * 後來 reg_1[4] = {24, 7, 6, 5};
 * 後來 reg_2[5] = {86, 87, 88, 89, 90};
 */

#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asauart/src/asauart_slave.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/hal_uart.h"
#include "c4mlib/hardware/src/isr.h"
#include "c4mlib/time/src/hal_time.h"
#include "c4mlib/time/src/timeout.h"

#include "c4mlib/config/remo_reg.cfg"

#define CFbit 4

ISR(TIMER1_COMPA_vect) {
    Timeout_tick();
    HAL_tick();
}

ISR(USART1_RX_vect) {
    ASA_UARTS2_rx_step();
}

ISR(USART1_TX_vect) {
    ASA_UARTS2_tx_step();
}

// Initialize the TIME1 with CTC mode, interrupt at 1000 Hz
void init_timer();
void CF_rec(void *);  // Control Flag Recognize

int main() {
    // The configure program is same.
    C4M_DEVICE_set();
    init_timer();
    HAL_time_init();

    sei();

    SerialIsr_t UARTIsrStr = SERIAL_ISR_STR_UART_INI;
    // FIX  ME: 使用新的初始化方式
    SerialIsr_net(&UARTIsrStr, 0);

    uint8_t reg_1[4] = {1, 2, 3, 4};
    uint8_t reg_2[5] = {86, 87, 88, 89, 90};

    uint8_t reg_1_Id = RemoRW_reg(&UARTIsrStr, reg_1, 4);
    printf("Create RemoRWreg [%u] with %u bytes \n", reg_1_Id,
           UARTIsrStr.remo_reg[reg_1_Id].sz_reg);
    UARTIsrStr.remo_reg[reg_1_Id].func_p = CF_rec;
    UARTIsrStr.remo_reg[reg_1_Id].funcPara_p = reg_1;
    uint8_t reg_2_Id = RemoRW_reg(&UARTIsrStr, reg_2, 5);
    printf("Create RemoRWreg [%u] with %u bytes \n", reg_2_Id,
           UARTIsrStr.remo_reg[reg_2_Id].sz_reg);

    while (1) {
        // ---- Show the register data ---- //
        for (int i = 0; i < 4; i++) {
            printf("reg_1[%d] = %3u  ", i, reg_1[i]);
        }
        printf("\n");

        for (int i = 0; i < 5; i++) {
            printf("reg_2[%d] = %3u  ", i, reg_2[i]);
        }
        printf("\n");

        HAL_delay(100UL);
    }
}

void init_timer() {
    // Pre-scale 1
    // COMA COMB non-inverting mode
    // 1    0
    // CTC with TOP ICRn
    // WGM3 WGM2 WGM1 WGM0
    // 1    1    0    0
    // TCCR1A  [COM1A1 COM1A0 COM1B1 COM1B0 COM1C1 COM1C0 WGM11 WGM10]
    // TCCR1B  [ICNC1 ICES1 -- WGM13 WGM12 CS12 CS11 CS10]

    ICR1 = 11058;
    TCCR1A = 0b00000000;
    TCCR1B = 0b00011001;
    TCNT1 = 0x00;
    TIMSK |= 1 << OCIE1A;
}

void CF_rec(void *Data_p) {
    uint8_t CF = 255;
    CF = *((uint8_t *)Data_p) & CF;
    for (int i = 0; i < 8 - CFbit; i++) {
        CF = CF >> 1;
    }
    switch (CF) {
        case 1:
            ASA_UARTS2_tx_step();
            break;
        default:
            break;
    }
    // 參數初始
    CF = 255;  // CF 重製
}
