/**
 * @file test_uart3.c
 * @author Wu Cheng Han
 * @date 2019.10.07
 * @brief 測試 ASA單板電腦 作為Master，透過Uart Mode3
 * 跟遠端Register 通訊，進行封包傳送接收。
 *
 * 需與 test_slave_uart3 一起做測試
 * 預測結果；
 * 原來 data1_buffer[5] = {9, 8, 7, 6, 5};
 * 原來 data2_buffer[10] = {1, 2, 3, 4, 5, 5, 4, 3, 2, 1};

 *
 * 原來 data1_buffer[5] = {9, 8, 7, 6, 5};
 * 原來 data2_buffer[5] = {11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
 *
 * >>  【test 1】傳送 5 筆資料
 * >>  【test 1】UARTM_trm : data[0]=9
 * >>  【test 1】UARTM_trm : data[1]=8
 * >>  【test 1】UARTM_trm : data[2]=7
 * >>  【test 1】UARTM_trm : data[3]=6
 * >>  【test 1】UARTM_trm : data[4]=5
 * >>  【test 2】接收 10 筆資料
 * >>  【test 2】UARTM_rec : data[0]=11
 * >>  【test 2】UARTM_rec : data[1]=12
 * >>  【test 2】UARTM_rec : data[2]=13
 * >>  【test 2】UARTM_rec : data[3]=14
 * >>  【test 2】UARTM_rec : data[4]=15
 * >>  【test 2】UARTM_rec : data[5]=16
 * >>  【test 2】UARTM_rec : data[6]=17
 * >>  【test 2】UARTM_rec : data[7]=18
 * >>  【test 2】UARTM_rec : data[8]=19
 */

#include "c4mlib/asauart/src/asauart_master.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/isr.h"
#include "c4mlib/time/src/hal_time.h"

#include <util/delay.h>
#define DELAY 0
ISR(TIMER3_COMPA_vect) {
    HAL_tick();
}

// Initialize the TIME3 with CTC mode, interrupt at 1000 Hz
void init_timer();

int main() {
    C4M_DEVICE_set();
    HAL_time_init();

    init_timer();
    UARTM_Inst.init();

    sei();

    uint8_t data1_buffer[5] = {9, 8, 7, 6, 5};
    uint8_t data2_buffer[10] = {1, 2, 3, 4, 5, 5, 4, 3, 2, 1};

    uint8_t result = 0;

    /***** UARTM Mode3 Transmit test *****/
    /*** 【test 1】 ***/
    printf("【test 1】傳送 5 筆資料\n");
    result = UARTM_trm(3, 0, 0, 5, data1_buffer, DELAY);
    if (result)
        printf("<1>UARTM_trm Fail [%d]\n", result);
    for (int i = 0; i < 5; i++) {
        printf("【test 1】UARTM_trm : data[%u]=%u\n", i, data1_buffer[i]);
    }
    HAL_delay(100);

    /***** UARTM Mode3  Receive test *****/
    /*** 【test 2】 ***/
    printf("【test 2】接收 10 筆資料\n");
    result = UARTM_rec(3, 0, 0, 10, data2_buffer, DELAY);
    if (result)
        printf("<2>UARTM_rec Fail [%d]\n", result);
    for (int i = 0; i < 10; i++) {
        printf("【test 2】UARTM_rec : data[%u]=%u\n", i, data2_buffer[i]);
    }
    HAL_delay(100);

    while (1) {
    }  // Block here
}

void init_timer() {
    // Pre-scale 1
    // COMA COMB non-inverting mode
    // 1    0
    // CTC with TOP ICRn
    // WGM3 WGM2 WGM1 WGM0
    // 1    1    0    0
    // TCCR3A  [COM3A1 COM3A0 COM3B1 COM3B0 COM3C1 COM3C0 WGM31 WGM30]
    // TCCR3B  [ICNC3 ICES3 �V WGM33 WGM32 CS32 CS31 CS30]

    ICR3 = 11058;
    TCCR3A = 0b00000000;
    TCCR3B = 0b00011001;
    TCNT3 = 0x00;
    ETIMSK |= 1 << OCIE3A;
}
