/**
 * @file test_sync_array.c
 * @author LiYu87
 * @brief 測試傳送陣列的同步機制
 * @date 2019.08.21
 *
 * 建議配合人機進行測試。
 */

#include "c4mlib/asahmi/src/asa_hmi.h"
#include "c4mlib/device/src/device.h"

int main() {
    C4M_STDIO_init();

    float data[5] = {1.1, -1, 0, 1, -2.1};
    char num = 5;

    HMI_snput_array(HMI_TYPE_F32, num, data);
    HMI_snget_array(HMI_TYPE_F32, num, data);
    HMI_snput_array(HMI_TYPE_F32, num, data);

    return 0;
}
