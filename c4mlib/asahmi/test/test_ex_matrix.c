/**
 * @file test_ex_matrix.c
 * @author LiYu87
 * @brief 測試函式 HMI_get_matrix
 * @date 2019.08.21
 *
 * 建立在 HMI_put_matrix 可以成功發送資料的前提下，進行測試。
 * 建議配合人機進行測試。
 */

#include "c4mlib/asahmi/src/asa_hmi.h"
#include "c4mlib/device/src/device.h"

int main() {
    C4M_STDIO_init();

    uint8_t data[2][5] = {
        {0, 1, 2, 3, 4},
        {5, 6, 7, 8, 9}
    };

    HMI_put_matrix(HMI_TYPE_UI8, 2, 5, data);
    HMI_get_matrix(HMI_TYPE_UI8, 2, 5, data);
    HMI_put_matrix(HMI_TYPE_UI8, 2, 5, data);

    return 0;
}
