/**
 * @file 7s00.h
 * @author smallplayplay
 * @author LiYu87
 * @brief 宣告7s00相關函式原型。
 * @date 2019.10.12
 */

#ifndef C4MLIB_ASA7S00_7S00_H
#define C4MLIB_ASA7S00_7S00_H

#include <stdint.h>

/**
 * @defgroup asa7s00_macros asa7s00 macros
 * @defgroup asa7s00_struct asa7s00 structs
 * @defgroup asa7s00_func asa7s00 functions
 */

/* Public Section Start */
/**
 * @brief ASA 7S00 參數結構。
 *
 * @ingroup asa7s00_struct
 *
 * ASA 7S00 有四個七節管，由左到右分別為編號0、1、2、3七節管。
 *
 * 其中 FFFlags 每個bit分別代表以下意思，其中閃爍旗標0、1代表不閃爍、閃爍
 * 浮點數旗標0、1代表該七節管小點不顯示、顯示：
 *  - bit 0 : 編號 3 七節管的閃爍旗標
 *  - bit 1 : 編號 2 七節管的閃爍旗標
 *  - bit 2 : 編號 1 七節管的閃爍旗標
 *  - bit 3 : 編號 0 七節管的閃爍旗標
 *  - bit 4 : 編號 3 七節管的浮點數旗標
 *  - bit 5 : 編號 2 七節管的浮點數旗標
 *  - bit 6 : 編號 1 七節管的浮點數旗標
 *  - bit 7 : 編號 0 七節管的浮點數旗標
 *
 * _7S00_put_reg 則代表編號0、1、2、3七節管要顯示的ascii。
 * 支援的ascii有數字0~9、英文A~F。
 */
typedef struct {
    uint8_t FFFlags;           ///< 浮點數旗標，閃爍旗標 (float and flash flag)
    uint8_t ASCII_list[4];     ///< not used.
    uint8_t _7S00_put_reg[4];  ///< 編號0、1、2、3七節管的ASCII碼。
} Asa7s00Para_t;

/**
 * @brief 經由ASA Bus設定7s00的小數點及位元的顯示。
 *
 * @ingroup asa7s00_func
 * @param ASA_ID ASA介面卡的ID編號。
 * @param LSByte 控制旗標(control flag)或遠端讀寫暫存器的ID。
 * @param Mask  遮罩。
 * @param Shift 向左位移。
 * @param Data  寫入資料。
 * @param Str_p ASA 7S00 參數結構。
 * @return char 錯誤代碼：
 *   - 0：成功無誤。
 *   - 4：ID錯誤，無法找到該裝置。
 *   - 5：欲顯示之數值無法表示。
 *   - 7：LSByte錯誤。
 *   - 9：Shift錯誤。
 *
 * 透過ASA Bus的傳輸，將7s00的ASA ID、LSByte=200、Mask、Shift、Data以及7s00的參
 * 數結構輸進此函數中，即可完成7s00四個位數七節管的小數點和閃爍位元的顯示設定。
 * 7s00的LSByte為固定的200，若輸入非200的值則會回傳7的error code。
 */
char ASA_7S00_set(char ASA_ID, char LSByte, char Mask, char Shift, char Data,
                  Asa7s00Para_t* Str_p);

/**
 * @brief 經由ASA Bus輸出要顯示在7s00的數值。
 *
 * @ingroup asa7s00_func
 * @param ASA_ID ASA介面卡的ID編號。
 * @param LSByte 控制旗標(control flag)或遠端讀寫暫存器的ID。
 * @param Bytes 資料大小，即為*Data_p的大小。
 * @param Data_p  指標指向資料。
 * @param Str_p ASA 7S00 參數結構。
 * @return char 錯誤代碼： 
 *   - 0：成功無誤。
 *   - 4：ID錯誤，無法找到該裝置。
 *   - 5：欲顯示之數值無法表示。
 *   - 7：LSByte錯誤。
 *   - 8：輸入Byte錯誤。
 * 
 * 透過ASA Bus的傳輸，將7s00的ASA ID、LSByte=200、Byte、*Data_p以及7s00的參
 * 數結構輸進此函數中，7s00及會按照使用者的輸入*Data_p和LSByte、Byte決定輸出的形式。
 * 假如Byte為1且LSByte為0~3，此函式會指定7s00某一位元輸出*Data_p之ASCII碼。
 * 假如Byte為4且LSByte為0，此函式會使7s00輸出至四位元的七節管，且為*Data_p之ASCII碼。
 */                 
char ASA_7S00_put(char ASA_ID, char LSByte, char Bytes, void* Data_p,
                  Asa7s00Para_t* Str_p);

/**
 * @brief 經由ASA Bus取得顯示在7s00的數值。
 *
 * @ingroup asa7s00_func
 * @param ASA_ID ASA介面卡的ID編號。
 * @param LSByte 控制旗標(control flag)或遠端讀寫暫存器的ID。
 * @param Bytes 資料大小，即為*Data_p的大小。
 * @param Data_p  指標指向取得的資料。
 * @param Str_p ASA 7S00 參數結構。
 * @return char 錯誤代碼： 
 *   - 0：成功無誤。
 *   - 2：ID錯誤，無法找到該裝置。
 *   - 3：輸入Byte大小超過1。
 *   - 4：ID錯誤，無法找到該裝置。
 *   - 5：欲顯示之數值無法表示。
 *   - 7：LSByte錯誤。
 *   - 8：輸入Byte錯誤。
 *
 * 透過ASA Bus的傳輸，將7s00的ASA ID、LSByte=200、Byte、*Data_p以及7s00的參
 * 數結構輸進此函數中，函式就會將7s00上一時刻的輸出讀回，並存進*Data_p之中。
 */                  
char ASA_7S00_get(char ASA_ID, char LSByte, char Bytes, void* Data_p,
                  Asa7s00Para_t* Str_p);
/* Public Section End */

/**
 * @brief 經由ASA Bus輸出要顯示在7s00的數值。
 *
 * @ingroup asa7s00_func
 * @param ASA_ID ASA介面卡的ID編號。
 * @param RegAdd 控制旗標(control flag)或遠端讀寫暫存器的ID，即為LSByte.
 * @param Bytes 資料大小，即為*Data_p的大小。
 * @param Data_p  指標指向資料。
 * @param Str_p ASA 7S00 參數結構。
 * @return char 錯誤代碼：
 *   - 0：成功無誤。
 *   - 4：ID錯誤，無法找到該裝置。
 *   - 5：欲顯示之數值無法表示。
 *   - 7：LSByte錯誤。
 *   - 8：輸入Byte錯誤。
 *
 * 此函式為c4mlib函式庫內部使用，不開放給使用者使用。
 *
 * 透過ASA Bus的傳輸，將7s00的ASA ID、LSByte=200、Byte、*Data_p以及7s00的參
 * 數結構輸進此函數中，7s00及會按照使用者的輸入*Data_p和LSByte、Byte決定輸出的形式。
 * 假如Byte為1且LSByte為0~3，此函式會指定7s00某一位元輸出*Data_p之ASCII碼。
 * 假如Byte為4且LSByte為0，此函式會使7s00輸出至四位元的七節管，且為*Data_p之ASCII碼。
 * 此函式會調用ASA_7S00_put做相關使用。
 */
char ASA_7s00_trm(char ASA_ID, char RegAdd, char Bytes, void *Data_p,
                  void *Str_p);

/**
 * @brief 功能待補。
 *
 * @ingroup asa7s00_func
 * @param ASA_ID ASA介面卡的ID編號。
 * @param RegAdd 控制旗標(control flag)或遠端讀寫暫存器的ID，即為LSByte.
 * @param Bytes 資料大小，即為*Data_p的大小。
 * @param Data_p  指標指向資料。
 * @param Str_p ASA 7S00 參數結構。
 * @return char 錯誤代碼：
 *   - 0：成功無誤。
 *
 * 此函式為c4mlib函式庫內部使用，不開放給使用者使用。
 *
 * 此函式為規格書中所開出的函式，但尚未開發為成完成。
 */
char ASA_7s00_rec(char ASA_ID, char RegAdd, char Bytes, void *Data_p,
                  void *Str_p);

/**
 * @brief 功能待補。
 *
 * @ingroup asa7s00_func
 * @param ASA_ID ASA介面卡的ID編號。
 * @param RegAdd 控制旗標(control flag)或遠端讀寫暫存器的ID，即為LSByte.
 * @param Mask 遮罩。
 * @param Shift 資料向右位移。
 * @param Data_p  指標指向資料。
 * @param Str_p ASA 7S00 參數結構。
 * @return char 錯誤代碼：
 *   - 0：成功無誤。
 *
 * 此函式為c4mlib函式庫內部使用，不開放給使用者使用。
 *
 * 此函式為規格書中所開出的函式，但尚未開發為成完成。
 */
char ASA_7S00_frc(char ASA_ID, char RegAdd, char Mask, char Shift, void *Data_p,
                  void *Str_p);

/**
 * @brief 經由ASA Bus設定7s00的小數點及位元顯示。
 *
 * @ingroup asa7s00_func
 * @param ASA_ID ASA介面卡的ID編號。
 * @param RegAdd 控制旗標(control flag)或遠端讀寫暫存器的ID，即為LSByte.
 * @param Mask 遮罩。
 * @param Shift 資料向左位移。
 * @param Data_p  指標指向資料。
 * @param Str_p ASA 7S00 參數結構。
 * @return char 錯誤代碼：
 *   - 0：成功無誤。
 *   - 4：ID錯誤，無法找到該裝置。
 *   - 5：欲顯示之數值無法表示。
 *   - 7：LSByte錯誤。
 *   - 9：Shift錯誤。
 *
 * 此函式為c4mlib函式庫內部使用，不開放給使用者使用。
 *
 * 透過ASA Bus的傳輸，將7s00的ASA ID、LSByte=200、Mask、Shift、Data以及7s00的參
 * 數結構輸進此函數中，即可完成7s00四個位數七節管的小數點和閃爍位元的顯示設定。
 * 7s00的LSByte為固定的200，若輸入非200的值會回傳7的error code。
 * 此函式會調用ASA_7S00_set做相關使用。
 */
char ASA_7S00_ftm(char ASA_ID, char RegAdd, char Mask, char Shift, void *Data_p,
                  void *Str_p);

#endif  // C4MLIB_ASA7S00_7S00_H
