/**
 * @file asa_twi_slave_mode2.c
 * @author Yuchen
 * @date 2019.03.29
 * @brief Slave TWI mode 2 通訊封包函式實現
 */

#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asatwi/src/asa_twi.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/device/src/device.h"

void ASA_TWIS2_step(void) {
    switch (TWI_STATUS) {
        case TWI_SR_SLA_ACK: {
            DEBUG_INFO("Own SLA+W has been received---%x\n", TWI_STATUS);
            TWICom_ACKCom(USE_ACK);
            //初始化byte_counter為ID = 2的暫存器資料長度
            ASATWISerialIsrStr->byte_counter =
                ((ASATWISerialIsrStr->remo_reg[2].sz_reg) - 1);
            break;
        }
        case TWI_SR_DATA_ACK: {
            DEBUG_INFO(
                "Previously addressed with own SLA+W data has been "
                "received---%x\n",
                TWI_STATUS);
            TWICom_ACKCom(USE_ACK);
            //固定將接收的資料存入ID = 2的暫存器
            ASATWISerialIsrStr->remo_reg[2]
                .data_p[ASATWISerialIsrStr->byte_counter] = TWDR;
            ASATWISerialIsrStr->byte_counter--;
            break;
        }
        case TWI_SR_DATA_STO: {
            DEBUG_INFO(
                "A STOP condition or repeated START condition has been "
                "received---%x\n",
                TWI_STATUS);
            TWICom_ACKCom(USE_ACK);
            ASATWISerialIsrStr->byte_counter = 0;
            break;
        }
        case TWI_ST_SLA_ACK: {
            DEBUG_INFO("Own SLA+R has been received---%x\n", TWI_STATUS);
            ASATWISerialIsrStr->byte_counter =
                ((ASATWISerialIsrStr->remo_reg[2].sz_reg) - 1);
            //第一筆資料傳送
            //固定傳送ID = 2的暫存器資料
            TWDR = ASATWISerialIsrStr->remo_reg[2]
                       .data_p[ASATWISerialIsrStr->byte_counter];
            //若只傳送一筆資料，傳送完畢後直送NACK
            if (ASATWISerialIsrStr->remo_reg[2].sz_reg == 1) {
                TWICom_ACKCom(USE_NACK);
            }
            else {
                //若傳送資料不只有一筆，直送ACK
                ASATWISerialIsrStr->byte_counter--;
                TWCR |= (1 << TWEN) | (1 << TWEA) | (1 << TWIE);
            }
            TWCR |= (1 << TWEN) | (1 << TWEA) | (1 << TWIE);
            break;
        }
        case TWI_ST_DATA_ACK: {
            DEBUG_INFO("Data byte in TWDR has been transmitted---%x\n",
                       TWI_STATUS);
            //最後一筆資料
            //傳送最後一筆資料 + NACK
            if (ASATWISerialIsrStr->byte_counter == 0) {
                TWDR = ASATWISerialIsrStr->remo_reg[2]
                           .data_p[ASATWISerialIsrStr->byte_counter];
                ASATWISerialIsrStr->byte_counter =
                    ASATWISerialIsrStr->remo_reg[2].sz_reg;
                TWICom_ACKCom(USE_NACK);
            }
            else {
                //資料尚未傳送完畢
                //傳送下一筆資料 + ACK
                TWDR = ASATWISerialIsrStr->remo_reg[2]
                           .data_p[ASATWISerialIsrStr->byte_counter];
                ASATWISerialIsrStr->byte_counter--;
                TWICom_ACKCom(USE_ACK);
            }
            break;
        }
        case TWI_ST_Data_last: {
            DEBUG_INFO("Last data byte in TWDR has been transmitted---%x\n",
                       TWI_STATUS);
            //最後一筆資料以傳遞完畢 + ACK
            TWICom_ACKCom(USE_ACK);
            break;
        }
        case TWI_ST_DATA_NACK: {
            DEBUG_INFO("Data byte in TWDR has been transmitted---%x\n",
                       TWI_STATUS);
            TWICom_ACKCom(USE_ACK);
            break;
        }
    }
}
