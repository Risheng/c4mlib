/**
 * @file test_twi_master_mode6.c
 * @author YuChen
 * @brief TWI Master mode6測試程式
 * @date 2019.10.04
 *
 * 測試方式：<br>
 *      軟體：本測試程式需搭配test_twi_Slave_mode6.c同步測試。<br>
 *      硬體：須將兩塊ASA_M128單板電腦的SCL與SDA串接。<br>
 * 測試時須先將已燒錄test_twi_slave_mode6.c的ASA_M128啟動，再將燒錄test_twi_master_mode6.c的ASA_M128啟動。<br>
 *
 * 測試程式執行動作：<br>
 * 將欲設資料傳入Slave板中，並讀回經Slave運算的值。<br>
 * 傳輸資料：已矩陣存取數值10~19。<br>
 * 接收資料：矩陣各值加5。
 *
 * 測試結果：<br>
 * >>  [Master] Transmit mode6 data1[0] = 10<br>
 * >>  [Master] Transmit mode6 data1[1] = 11<br>
 * >>  [Master] Transmit mode6 data1[2] = 12<br>
 * >>  [Master] Transmit mode6 data1[3] = 13<br>
 * >>  [Master] Transmit mode6 data1[4] = 14<br>
 * >>  [Master] Transmit mode6 data1[5] = 15<br>
 * >>  [Master] Transmit mode6 data1[6] = 16<br>
 * >>  [Master] Transmit mode6 data1[7] = 17<br>
 * >>  [Master] Transmit mode6 data1[8] = 18<br>
 * >>  [Master] Transmit mode6 data1[9] = 19<br>
 * >>  [Master] Transmit mode6 data2 = 9<br>
 * >>  [Master] Device ID Configer ok!!<br>
 * >>  [Master] Error code : 0<br>
 * >>  [Master] Error code : 0<br>
 * >>  [Master] Error code : 0<br>
 * >>  [Master] Error code : 0<br>
 * >>  [Master] Receive mode6 data1[0] = 15<br>
 * >>  [Master] Receive mode6 data1[1] = 16<br>
 * >>  [Master] Receive mode6 data1[2] = 17<br>
 * >>  [Master] Receive mode6 data1[3] = 18<br>
 * >>  [Master] Receive mode6 data1[4] = 19<br>
 * >>  [Master] Receive mode6 data1[5] = 20<br>
 * >>  [Master] Receive mode6 data1[6] = 21<br>
 * >>  [Master] Receive mode6 data1[7] = 22<br>
 * >>  [Master] Receive mode6 data1[8] = 23<br>
 * >>  [Master] Receive mode6 data1[9] = 24<br>
 * >>  [Master] Receive mode4 data2 = 14
 */

#include "c4mlib/asatwi/src/asa_twi.h"
#include "c4mlib/device/src/device.h"

#define ARRARY_SIZE 10
#define SLA 0x39
#define WAITTICK 50

void TWI_Master_set();
uint8_t check = 0;
int main(void) {
    C4M_STDIO_init();
    TWI_Master_set();
    uint8_t Mode = 6;
    uint8_t default_SLA = 0b00000000;   /*TWI 廣播SLA*/
    uint8_t regadd2 = 0x02;
    uint8_t regadd3 = 0x03;
    uint8_t set_SLA = SLA;
    uint8_t conf_add = 0b00000000; /*Remo_reg第0暫存器將針對EEPROM寫入*/
    uint8_t bytes = ARRARY_SIZE;
    uint8_t data2 = 9;
    uint8_t data1[ARRARY_SIZE];
    //  設定data初值
    for (int i = 0; i < ARRARY_SIZE; i++) {
        data1[i] = i + 10;
        printf("[Master] Transmit mode6 data1[%d] = %d\t\n", i, data1[i]);
    }
    printf("[Master] Transmit mode6 data2 = %d\n", data2);
    check = TWIM_trm(Mode, default_SLA, conf_add, 1, &set_SLA,
                     WAITTICK); /*使用廣播頻道與Slave通訊，並將欲設定之Slave
                                   ID儲存至Remo_reg第0暫存器*/
    if (check == 0) {
        printf("[Master] Device ID Configer ok!!\n");
    }
    check = TWIM_trm(Mode, SLA, regadd2, bytes, data1, WAITTICK);
    printf("[Master] Error code : %d\n", check);
    _delay_ms(50);
    check = TWIM_trm(Mode, SLA, regadd3, 1, &data2, WAITTICK);
    printf("[Master] Error code : %d\n", check);
    _delay_ms(50);
    check = TWIM_rec(Mode, SLA, regadd2, bytes, data1, WAITTICK);
    printf("[Master] Error code : %d\n", check);
    _delay_ms(50);
    check = TWIM_rec(Mode, SLA, regadd3, 1, &data2, WAITTICK);
    printf("[Master] Error code : %d\n", check);
    for (int i = 0; i < bytes; i++) {
        printf("[Master] Receive mode6 data1[%d] = %d\t\n", i, data1[i]);
    }
    printf("[Master] Receive mode4 data2 = %d\n", data2);
}
void TWI_Master_set() {
    // Set TWI speed // _CPU Clock frequency_/16+2*(TWBR)*4^(prescaler bits) ;
    // prescaler bits = 1
    TWBR = 12;
}
