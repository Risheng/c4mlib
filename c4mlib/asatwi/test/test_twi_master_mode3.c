/**
 * @file test_twi_master_mode3.c
 * @author Yuchen
 * @brief TWI Master mode3 測試程式
 * @date 2019.10.04
 *
 * 測試方式：<br>
 *      軟體：本測試程式需搭配test_twi_Slave_mode3.c同步測試。<br>
 *      硬體：須將兩塊ASA_M128單板電腦的SCL與SDA串接。<br>
 * 測試時須先將已燒錄test_twi_slave_mode3.c的ASA_M128啟動，再將燒錄test_twi_master_mode3.c的ASA_M128啟動。<br>
 *
 * 測試程式執行動作：<br>
 * 將欲設資料傳入Slave板中，並讀回經Slave運算的值。<br>
 * 傳輸資料：已矩陣存取數值10~19。<br>
 * 接收資料：矩陣各值加10。
 *
 * 測試結果：<br>
 * >>  [MASTER] Transmit mode3 data1[0] = 10<br>
 * >>  [MASTER] Transmit mode3 data1[1] = 11<br>
 * >>  [MASTER] Transmit mode3 data1[2] = 12<br>
 * >>  [MASTER] Transmit mode3 data1[3] = 13<br>
 * >>  [MASTER] Transmit mode3 data1[4] = 14<br>
 * >>  [MASTER] Transmit mode3 data1[5] = 15<br>
 * >>  [MASTER] Transmit mode3 data1[6] = 16<br>
 * >>  [MASTER] Transmit mode3 data1[7] = 17<br>
 * >>  [MASTER] Transmit mode3 data1[8] = 18<br>
 * >>  [MASTER] Transmit mode3 data1[9] = 19<br>
 * >>  [Master] Error code : 0<br>
 * >>  [Master] Error code : 0<br>
 * >>  [Master] Receive mode3 data1[0] = 20<br>
 * >>  [Master] Receive mode3 data1[1] = 21<br>
 * >>  [Master] Receive mode3 data1[2] = 22<br>
 * >>  [Master] Receive mode3 data1[3] = 23<br>
 * >>  [Master] Receive mode3 data1[4] = 24<br>
 * >>  [Master] Receive mode3 data1[5] = 25<br>
 * >>  [Master] Receive mode3 data1[6] = 26<br>
 * >>  [Master] Receive mode3 data1[7] = 27<br>
 * >>  [Master] Receive mode3 data1[8] = 28<br>
 * >>  [Master] Receive mode3 data1[9] = 29
 */

#include "c4mlib/asatwi/src/asa_twi.h"
#include "c4mlib/device/src/device.h"

#define ARRARY_SIZE 10
#define SLA 0x39
#define WAITTICK 50

void TWI_Master_set();
uint8_t check = 0;

int main(void) {
    C4M_STDIO_init();
    TWI_Master_set();
    uint8_t data1[ARRARY_SIZE];
    //  設定data初值
    for (int i = 0; i < ARRARY_SIZE; i++) {
        data1[i] = i + 10;
        printf("[MASTER] Transmit mode3 data1[%d] = %d\t\n", i, data1[i]);
    }
    uint8_t Mode = 3;
    uint8_t regadd1 = 0x02;
    uint8_t bytes = ARRARY_SIZE;
    check = TWIM_trm(Mode, SLA, regadd1, bytes, data1, WAITTICK);
    printf("[Master] Error code : %d\n", check);
    _delay_ms(150);
    check = TWIM_rec(Mode, SLA, regadd1, bytes, data1, WAITTICK);
    printf("[Master] Error code : %d\n", check);
    _delay_ms(150);
    for (int i = 0; i < bytes; i++) {
        printf("[Master] Receive mode3 data1[%d] = %d\t\n", i, data1[i]);
    }
}
void TWI_Master_set() {
    // Set TWI speed // _CPU Clock frequency_/16+2*(TWBR)*4^(prescaler bits) ;
    // prescaler bits = 1
    TWBR = 12;
}
