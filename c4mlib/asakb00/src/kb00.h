/**
 * @file kb00.h
 * @author s915888
 * @author s108327005
 * @brief 宣告kb00相關函式原型。
 * @date 2019.10.08
 */

#ifndef C4MLIB_ASAKB00_KB00_H
#define C4MLIB_ASAKB00_KB00_H

#include <stdint.h>

/**
 * @defgroup asakb00_macros asakb00 macros
 * @defgroup asakb00_struct asakb00 structs
 * @defgroup asakb00_func asakb00 functions
 */

/* Public Section Start */
/**
 * @brief ASA KB00 參數結構。
 * @ingroup asakb00_struct
 */
typedef struct {
    uint8_t mode;        ///< 控制鍵號模式、ASCII模式
    uint8_t keymap[16];  ///< 控制ASCII模式下每個按鍵的回傳值
} AsaKb00Para_t;

/**
 * @brief 呼叫本函式可以執行 ASA_KB00 介面卡工作模式的設定。
 *
 * @ingroup asakb00_func
 * @param ASA_ID ASA介面卡的ID編號。
 * @param LSByte 欲操作暫存器編號。
 * @param Mask  遮罩。
 * @param Shift 向左位移。
 * @param Data  寫入資料。
 * @param Str_p ASA KB00 參數結構。
 * @return char 錯誤代碼：
 *   - 0 : 成功無誤。
 *   - 4 : ASA_ID 錯誤，應如旋鈕所指向編號，小於7。
 *   - 7 : LSByte 錯誤，應介於 200 ~ 216。
 *   - 9 : Shift 錯誤，應小於 7。
 *
 * 呼叫本函式可以執行 ASA_KB00 介面卡工作模式的設定，主要有以下兩個功能：
 * 1. 選擇盤輸出鍵號或者對應的 ASCII 碼。
 *    - LSByte 應為 200。
 *    - Mask 應為 0x01。
 *    - Shift 應為 0。
 *    - Data 應為 0、1 ，代表鍵號模式、ASCII模式。
 * 2. 更新盤各按鍵對應的 ASCII 碼。
 *    - LSByte 應為 201 ~ 216，分別代表鍵號1~16的 ASCII 碼。
 *    - Mask 應為 0xff。
 *    - Shift 應為 0。
 *    - Data 要更新的 ASCII 碼。
 */
char ASA_KB00_set(char ASA_ID, char LSByte, char Mask, char Shift, char Data,
                  AsaKb00Para_t *Str_p);

/**
 * @brief 呼叫本函式可以 讀取盤輸入 或 查詢盤 ASCII 碼。
 *
 * @ingroup asakb00_func
 * @param ASA_ID  ASA介面卡的ID編號。
 * @param LSByte 欲操作暫存器編號。
 * @param Bytes 資料大小，即為*Data_p的大小。
 * @param *Data_p  指標指向資料。
 * @param Str_p ASA KB00 參數結構。
 * @return char
 *   - 0 : 成功無誤。
 *   - 4 : ASA_ID 錯誤，應如旋鈕所指向編號，小於7。
 *   - 7 : LSByte 錯誤，應介於 200 ~ 216。
 *   - 8 : Bytes 錯誤。
 *
 * 此函式為c4mlib函式庫內部使用，不開放給使用者使用。
 *
 * 呼叫本函式可以主要有以下兩個功能：
 * 1. 讀取盤輸入：由 ASA_KB00 介面卡讀取鍵入的數字，若無輸入，則會回傳 NULL 的
 * ASCII 碼(0)。
 *    - LSByte 應為 100。
 *    - Bytes 應為 1。
 *    - Data_p 應為 1 byte 大小記憶體位置(char、uint8_t)。
 * 2. 查詢盤 ASCII 碼：讀取目前設定的盤對應之 ASCII 碼。
 *    - LSByte 應為 201 ~ 216，分別代表鍵號1~16。
 *    - Bytes 應為 1。
 *    - Data_p 應為 1 byte 大小記憶體位置(char、uint8_t)。
 */
char ASA_KB00_get(char ASA_ID, char LSByte, char Bytes, void *Data_p,
                  AsaKb00Para_t *Str_p);
/* Public Section End */

/**
 * @brief 旗標傳輸
 *
 * @ingroup asakb00_func
 * @param ASA_ID 如旋鈕選擇，應小於7。
 * @param RegAdd 欲蓋寫暫存器編號。
 * @param Mask 遮罩值。
 * @param Shift 向左位移。
 * @param Data_p 資料來源變數。
 * @param Str_p ASA KB00 參數結構。
 * @return char 錯誤代碼：
 *   - 0 : 成功無誤。
 *   - 4 : 設定旗標群錯誤，應介於1~7。
 *   - 7 : 設定欲蓋寫暫存器編號錯誤，應介於 200 ~ 216。
 *   - 9 : 設定位移錯誤，應小於7。
 *
 * 此函式為c4mlib函式庫內部使用，不開放給使用者使用。
 *
 * 主控旗標蓋寫 Master Flag overwrite Transmitter 函式可利用通訊方式
 * 送旗標群去蓋寫遠端的指定暫存器，能夠配合伺服端通訊方式暫存器監控模式。
 */
char ASA_KB00_ftm(char ASA_ID, char RegAdd, char Mask, char Shift, void *Data_p,
                  void *Str_p);

/**
 * @brief 旗標接收
 *
 * @ingroup asakb00_func
 * @param ASA_ID 如旋鈕選擇，應小於7。
 * @param RegAdd 欲蓋寫暫存器編號。
 * @param Mask 遮罩值。
 * @param Shift 向右位移。
 * @param Data_p 資料來源變數。
 * @param Str_p ASA KB00 參數結構。
 * @return char 錯誤代碼：
 *   - 0 : 成功無誤。
 *   - 4 : 設定旗標群錯誤，應介於1~7。
 *   - 7 : 設定欲蓋寫暫存器編號錯誤，應介於 200 ~ 216。
 *   - 9 : 設定位移錯誤，應小於7。
 *
 * 此函式為c4mlib函式庫內部使用，不開放給使用者使用。
 *
 * 主控旗標讀取 Master Flag overwrite ReCeive 函式可利用通訊方式
 * 接收指定暫存器旗標群，能夠配合伺服端暫存器監控模式。
 */
char ASA_KB00_frc(char ASA_ID, char RegAdd, char Mask, char Shift, void *Data_p,
                  void *Str_p);

/**
 * @brief 接收資料
 *
 * @ingroup asakb00_func
 * @param ASA_ID 如旋鈕選擇，應小於7。
 * @param RegAdd 欲讀回介面卡中變數的暫存器編號。
 * @param Bytes 欲讀回介面卡中變數的 BYTES 數。
 * @param Data_p 待存放收到資料變數起始位址。
 * @param Str_p ASA KB00 參數結構。
 * @return char 錯誤代碼：
 *   - 0 : 成功無誤。
 *   - 4 : 設定旗標群錯誤，應介於1~7。
 *   - 7 : 設定欲蓋寫暫存器編號錯誤，應介於 200 ~ 216。
 *   - 9 : 設定位移錯誤，應小於7。
 *
 * 此函式為c4mlib函式庫內部使用，不開放給使用者使用。
 *
 * 依據旋鈕選擇設定，以指定編號之封包格式，向遠端介面卡通訊接收資料。
 */
char ASA_KB00_rec(char ASA_ID, char RegAdd, char Bytes, void *Data_p,
                  void *Str_p);

/**
 * @brief 傳輸資料
 *
 * @ingroup asakb00_func
 * @param ASA_ID 如旋鈕選擇，應小於7。
 * @param RegAdd 欲寫入介面卡中變數的暫存器編號。
 * @param Bytes 欲寫入介面卡中變數的 BYTES 數。
 * @param Data_p 待送出資料。
 * @param Str_p ASA KB00 參數結構。
 * @return char 錯誤代碼：
 *   - 0 : 成功無誤。
 *   - 4 : 設定旗標群錯誤，應介於1~7。
 *   - 7 : 設定欲蓋寫暫存器編號錯誤，應介於 200 ~ 216。
 *   - 9 : 設定位移錯誤，應小於7。
 *
 * 此函式為c4mlib函式庫內部使用，不開放給使用者使用。
 *
 * 依據旋鈕選擇設定，以指定編號之封包格式，向遠端介面卡通訊送出資料。
 */
char ASA_KB00_trm(char ASA_ID, char RegAdd, char Bytes, void *Data_p,
                  void *Str_p);

#endif  // C4MLIB_ASAKB00_KB00_H
