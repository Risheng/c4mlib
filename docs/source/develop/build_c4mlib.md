# C4MLIB 專案建置流程

主寫者： 未定 <br>
編輯者： <br>
日期：   <br>

## 產生c4mlib.h及libc4m.a

``` bash
cd c4mlib
py setup.py
```

執行後，
- 會生成 `.\c4mlib\dist\libc4m.a`
- 會生成 `.\c4mlib\dist\c4mlib.h`

## 產生libc4m.a

``` bash
cd c4mlib
make
```
執行後，
- 會生成 `.\c4mlib\dist\libc4m.a`
